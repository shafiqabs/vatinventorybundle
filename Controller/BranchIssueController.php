<?php
namespace Terminalbd\InventoryBundle\Controller;

use App\Entity\Application\Accounting;
use App\Entity\Application\Inventory;
use App\Entity\Core\Customer;
use App\Entity\Core\Setting;
use App\Service\ConfigureManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\BranchIssue;
use Terminalbd\InventoryBundle\Entity\BranchIssueItem;
use Terminalbd\InventoryBundle\Entity\InvoiceKeyValue;
use Terminalbd\InventoryBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\Sales;
use Terminalbd\InventoryBundle\Entity\SalesItem;
use Terminalbd\InventoryBundle\Form\BranchIssueFormType;
use Terminalbd\InventoryBundle\Form\SalesExportFormType;
use Terminalbd\InventoryBundle\Repository\ItemRepository;


/**
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY')")
 * @Route("/inventory/branch-issue")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class BranchIssueController extends AbstractController
{


    /**
     * @Route("/", methods={"GET"}, name="inventory_branch_issue")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_BRANCH_ISSUE')")
     */

    public function index(Request $request): Response
    {
        return $this->render('@TerminalbdInventory/branch-issue/index.html.twig');
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_BRANCH_ISSUE')")
     * @Route("/new", methods={"GET", "POST"}, name="inventory_branch_issue_create")
     */

    public function create(Request $request): Response
    {
        $entity = new BranchIssue();
        $terminal = $this->getUser()->getTerminal();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $em = $this->getDoctrine()->getManager();
        $entity->setConfig($config);
        $receiveDate = new \DateTime('now');
        $entity->setProcess('created');
        $entity->setCreatedBy($this->getUser());
        $entity->setIssuePerson($this->getUser()->getName());
        $entity->setIssueDesignation($this->getUser()->getDesignation()->getName());
        $entity->setMode('local');
        $em->persist($entity);
        $em->flush();
        return $this->redirectToRoute('inventory_branch_issue_edit',['id'=> $entity->getId()]);

    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit",methods={"GET", "POST"}, name="inventory_branch_issue_edit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_BRANCH_ISSUE')")
     */
    public function edit(Request $request, BranchIssue $entity): Response
    {

        if(in_array($entity->getProcess(),array("checked","approved","done"))){
            return $this->redirectToRoute('inventory_branch_issue_show',['id'=> $entity->getId()]);
        }
        $terminal = $this->getUser()->getTerminal();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $form = $this->createForm(BranchIssueFormType::class, $entity,array('terminal' => $terminal))->add('SaveAndCreate', SubmitType::class);
        $data = $request->request->all();
        $form->handleRequest($request);
        $modes = array("production-item");
        $products = $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:Item")->modeWiseStockItem($config,$modes);
        if ($form->isSubmitted() && $form->isValid()) {
            $entity->upload();
            if(empty($entity->getBranch())){
                $entity->setProcess('in-progress');
            }
            $this->getDoctrine()->getManager()->flush();
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:InvoiceKeyValue')->insertBatchIssueKeyValue($entity,$data);
            if($entity->getProcess() == "done"){
                return $this->redirectToRoute('inventory_branch_issue_show',['id'=> $entity->getId()]);
            }
            return $this->redirectToRoute('inventory_branch_issue_edit',['id'=> $entity->getId()]);
        }
        $mode = "local";
        return $this->render("@TerminalbdInventory/branch-issue/{$mode}/new.html.twig", [
            'actionUrl' => $this->generateUrl('inventory_branch_issue_edit',array('id'=> $entity->getId())),
            'dataAction' => $this->generateUrl('inventory_branch_issue_ajax',array('id'=> $entity->getId())),
            'entity' => $entity,
            'products' => $products,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-submit",methods={"GET", "POST"}, name="inventory_branch_issue_ajax")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_BRANCH_ISSUE')")
     */

    public function ajaxSubmit(Request $request, BranchIssue $entity): Response
    {
        $data = $_REQUEST;
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $form = $this->createForm(BranchIssueFormType::class, $entity,array('terminal' => $terminal));
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if(empty($entity->getCustomer())){
                $entity->setProcess('in-progress');
            }
            $em->persist($entity);
            $em->flush();
        }
        $invoice = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:BranchIssue')->updatesalesTotalPrice($entity);
        $result = $this->returnResultData($invoice);
        return new Response(json_encode($result));
    }

    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }


    /**
     * Check if mobile available for registering
     * @Route("/customer-info", methods={"GET"}, name="inventory_customer_info")
     * @param   string
     * @return  bool
     */
    function showCustomerAjax(Request $request) : Response
    {
        $post = $_REQUEST['customer'];
        $customer = $this->getDoctrine()->getRepository(Customer::class)->find($post);
        if($customer){
            return new Response(json_encode(array('binNo'=> $customer->getBinNo() ,'customerMobile'=> $customer->getMobile(), 'customerAddress'=> $customer->getAddress())));
        }
        return new Response(json_encode(array('binNo'=> '' ,'customerMobile'=> '', 'customerAddress'=>'')));
    }

    /**
     * Check if mobile available for registering
     * @Route("/{sales}/create-sales-customer", methods={"POST"}, name="inventory_customer_create_ajax")
     * @param   string
     * @return  bool
     */
    function createCustomerAjax(Request $request,BranchIssue $sales) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $mode = "creatable";
        $status = $this->getDoctrine()->getRepository(Customer::class)->checkAvailable($terminal,$mode,$data);
        if($status == 'true') {
            $post = new Customer();
            $post->setTerminal($terminal);
            $post->setName($data['name']);
            $post->setCompanyName($data['company']);
            $confManager = new ConfigureManager();
            $mobile = $confManager->specialExpClean($data['mobile']);
            $post->setMobile($mobile);
            $businessType = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('name' => $data['businessType']));
            if ($businessType) {
                $post->setBusinessType($businessType);
            }
            if($data['address']){
                $post->setAddress($data['address']);
            }
            if($data['binNo']){
                $post->setBinNo($data['binNo']);
            }
            $this->getDoctrine()->getManager()->persist($post);
            $this->getDoctrine()->getManager()->flush();
            $sales->setCustomer($post);
            $this->getDoctrine()->getManager()->persist($sales);
            $this->getDoctrine()->getManager()->flush();
            return new Response('valid');
        }else{
            return new Response('invalid');
        }
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="inventory_branch_issue_show")
     * @Security("is_granted('ROLE_INVENTORY_BRANCH_ISSUE') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(BranchIssue::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $mode = "local";
        return $this->render("@TerminalbdInventory/branch-issue/local/show.html.twig", [
            'entity' => $entity,
        ]);
    }

     /**
     * Show a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="inventory_branch_issue_preview")
     * @Security("is_granted('ROLE_INVENTORY_BRANCH_ISSUE') or is_granted('ROLE_DOMAIN')")
     */
    public function preview($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(BranchIssue::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $mode = "local";
        return $this->render("@TerminalbdInventory/branch-issue/local/show.html.twig", [
            'entity' => $entity,
        ]);
    }



    /**
     * Approved a Setting entity.
     *
     * @Route("/{id}/{process}/process", methods={"GET"}, name="inventory_branch_issue_process")
     * @Security("is_granted('ROLE_INVENTORY_BRANCH_ISSUE') or is_granted('ROLE_DOMAIN')")
     */
    public function process($id,$process): Response
    {
        $entity = $this->getDoctrine()->getRepository(BranchIssue::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        if ($entity->getProcess() == 'done'){
            $entity->setCheckedBy($this->getUser());
            $entity->setProcess('checked');
        }elseif ($entity->getProcess() == 'checked'){
            $entity->setApprovedBy($this->getUser());
            $entity->setProcess('approved');
        }
        $em->persist($entity);
        $em->flush();
        return new Response('success');

    }

    private function returnResultData(BranchIssue $invoice,$msg=''){

        $invoiceItems = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:BranchIssueItem')->getBranchIssueLocalItems($invoice);

        $subTotal = $invoice->getSubTotal() > 0 ? $invoice->getSubTotal() : 0;
        $netTotal = $invoice->getNetTotal() > 0 ? $invoice->getNetTotal() : 0;
        $totalVat = $invoice->getValueAddedTax() > 0 ? $invoice->getValueAddedTax() : 0;
        $totalSd = $invoice->getSupplementaryDuty() > 0 ? $invoice->getSupplementaryDuty() : 0;
        $totalTti = $invoice->getTotalTaxIncidence() > 0 ? $invoice->getTotalTaxIncidence() : 0;
        $totalQuantity = $invoice->getQuantity() > 0 ? $invoice->getQuantity() : 0;
        $total = $invoice->getTotal() > 0 ? $invoice->getTotal() : 0;
        $due = $invoice->getDue() > 0 ? $invoice->getDue() : 0;
        $amount = $invoice->getAmount() > 0 ? $invoice->getAmount() : 0;
        $discount = $invoice->getDiscount() > 0 ? $invoice->getDiscount() : 0;
        $data = array(
            'subTotal' => number_format($subTotal,2),
            'totalQuantity' => number_format($totalQuantity,2),
            'totalVat' => number_format($totalVat,2),
            'totalSd' => number_format($totalSd,2),
            'totalTti' => number_format($totalTti,2),
            'netTotal' => number_format($netTotal,2),
            'amountTotal' => $netTotal,
            'total' => number_format($total,2),
            'due' => number_format($due,2),
            'amount' => $amount,
            'discount' => number_format($discount,2),
            'discountMode' => $invoice->getDiscountMode(),
            'discountPercent' => $invoice->getDiscountCalculation(),
            'invoiceItems' => $invoiceItems ,
            'msg' => $msg ,
            'success' => 'success'
        );

        return $data;

    }

    /**
     * Search a Setting entity.
     *
     * @Route("/{id}/product-search", methods={"GET"}, name="inventory_branch_issue_item_search")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_BRANCH_ISSUE')")
     */

    public function particularSearch(Item $particular)
    {
        $unit = !empty($particular->getUnit() && !empty($particular->getUnit()->getName())) ? $particular->getUnit()->getName():'Unit';

        $vatOptions = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:MasterItem')->getVatMinMax($particular->getMasterItem());

        $productGroup = $particular->getMasterItem()->getProductGroup()->getSlug();
        if($productGroup == "production-item"){
            $url = $this->generateUrl('inventory_production_batch_item');
            $batchItems = $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionBatchItem')->getCurrentBatchItems($particular,$url);
        }elseif($productGroup == "finish-goods"){
            $url = $this->generateUrl('inventory_purchase_batch_item');
            $batchItems = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:PurchaseItem')->getCurrentBatchItems($particular,$url);
        }
        return new Response(json_encode(array('productId'=> $particular->getId() , 'salesPrice'=> $particular->getsalesPrice(), 'remainingQnt'=> $particular->getRemainingQuantity() , 'quantity'=> 1 , 'unit'=> $unit,'batchItems' => $batchItems,'productGroup' => $productGroup)));

    }



    /**
     * Check if mobile available for registering
     * @Route("/{id}/sales-item-insert", methods={"POST"}, name="inventory_create_branch_issue_item")
     * @param   string
     * @return  bool
     */
    function itemInsertAjax(Request $request, BranchIssue $sales) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $em = $this->getDoctrine()->getManager();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:BranchIssueItem")->insertUpdateItem($config,$sales,$data);
        $invoice = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:BranchIssue')->updateSalesTotalPrice($sales);           $result = $this->returnResultData($invoice);
        return new Response(json_encode($result));

    }

    /**
     * Check if mobile available for registering
     * @Route("/item-update", methods={"POST"}, name="inventory_barnch_issue_item_update")
     * @param   string
     * @return  bool
     */

    function itemUpdateAjax(Request $request) : Response
    {
        $data = $_REQUEST;
        $item = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:BranchIssueItem')->find($data['item']);
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:BranchIssueItem")->updateItem($item,$data);
        $invoice = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:BranchIssue')->updatesalesTotalPrice($item->getBarnchIssue());
        $result = $this->returnResultData($invoice);
        return new Response(json_encode($result));

    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{invoice}/{item}/item-delete", methods={"GET"}, name="inventory_branch_issue_item_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_BRANCH_ISSUE')")
     */
    public function deleteItem(BranchIssue $invoice,$item): Response
    {
        $post = $this->getDoctrine()->getRepository(BranchIssueItem::class)->find($item);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $sales = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:BranchIssue')->updatesalesTotalPrice($invoice);
        $result = $this->returnResultData($sales);
        return new Response(json_encode($result));
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{meta}/delete-invoice-meta", methods={"GET"}, name="inventory_branch_issue_delete_invoice_meta")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_BRANCH_ISSUE')")
     */
    public function deleteInvoiceMeta(InvoiceKeyValue $meta): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($meta);
        $em->flush();
        return new Response('success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="inventory_branch_issue_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_BRANCH_ISSUE')")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(BranchIssue::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }


    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="inventory_branch_issue_data_table")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_BRANCH_ISSUE')")
     */

    public function dataTable(Request $request)
    {

        $query = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $iTotalRecords = $this->getDoctrine()->getRepository(BranchIssue::class)->count(array('config'=> $config));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:BranchIssue')->findWithSearch($config,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post Item */

        foreach ($result as $post):

            $editUrl = $this->generateUrl('inventory_branch_issue_edit',array('id' => $post['id']));
            $deleteUrl = $this->generateUrl('inventory_branch_issue_delete',array('id'=> $post['id']));
            $viewUrl = $this->generateUrl('inventory_branch_issue_show',array('id'=> $post['id']));
            $created = $post['created']->format('d-m-Y');
            if($post['discountMode'] == "percent"){
                $calculation = $post['calculation']."%";
            }else{
                $calculation = $post['calculation'];
            }
            $records["data"][] = array(
                $id                 = $i,
                $created            = $created,
                $invoice            = $post['invoice'],
                $branch             = $post['branchName'],
                $subTotal           = $post['subTotal'],
                $taxTotal           = $post['taxTotal'],
                $netTotal           = $post['netTotal'],
                $process            = ucfirst($post['process']),
                $action             ="<div class='btn-group card-option'><button type='button' class='btn btn-notify' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fas fa-ellipsis-h'></i></button>
<ul class='list-unstyled card-option dropdown-info dropdown-menu dropdown-menu-right'>
 <li class='dropdown-item'> <a href='{$viewUrl}' >View</a></li>
<li class='dropdown-item'> <a href='{$editUrl}'>Edit</a></li>
<li class='dropdown-item'> <a  data-action='{$deleteUrl}' class='remove' data-id='{$post['id']}' href='javascript:'>Remove</a></li>
</ul></div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }

}
