<?php
/**
 * Created by PhpStorm.
 * User: shafiq
 * Date: 9/29/19
 * Time: 9:09 PM
 */

namespace Terminalbd\InventoryBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
/**
 * Controller used to manage blog contents in the backend.
 *
 * Please note that the application backend is developed manually for learning
 * purposes. However, in your real Symfony application you should use any of the
 * existing bundles that let you generate ready-to-use backends without effort.
 *
 * @Route("/inventory")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_USER')or is_granted('ROLE_DOMAIN')")
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class InventoryController extends AbstractController
{


    /**
     * @Route("/bundle", name="inventory_index_bundle")
     * @return \Symfony\Component\HttpFoundation\Response
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY')")
     */
    function index() {
        return $this->render('@TerminalbdInventory/post/index.html.twig');
    }

}