<?php

namespace Terminalbd\InventoryBundle\Controller;

use App\Entity\Application\Inventory;
use App\Entity\Application\Production;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\Category;
use Terminalbd\InventoryBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\Setting;
use Terminalbd\InventoryBundle\Form\ItemFormType;
use Terminalbd\InventoryBundle\Repository\ItemRepository;
use Terminalbd\InventoryBundle\Repository\TaxTariffRepository;

/**
 * @Route("/inventory/item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ItemController extends AbstractController
{


    /**
     * @Route("/", methods={"GET"}, name="inventory_item")
     */
    public function index(Request $request): Response
    {
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $productTypes = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Setting')->getChildRecords("product-type");
        $productGroups = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Setting')->getChildRecords("product-group");
        $brands = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Setting')->getChildRecords("brand");
        $masterItems = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:MasterItem')->findBy(array('config' => $config,'status'=>1),array('name'=>"ASC"));
      //  $categories = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Category')->getChildRecords("brand");
        return $this->render('@TerminalbdInventory/item/index.html.twig', [
            'productTypes' => $productTypes,
            'productGroups' => $productGroups,
            'brands' => $brands,
            'masterItems' => $masterItems,
            'categories' => '',
        ]);
    }

    /**
     * @Route("/finish-goods", methods={"GET"}, name="inventory_item_finish_goods")
     */
    public function finishGoods(Request $request): Response
    {
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $modes = array("production-item","finish-goods");
        $products = $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:Item")->finishGoods($config,$modes);
        return $this->render('@TerminalbdInventory/item/finishGooods.html.twig', [
            'products' => $products,
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_ITEM')")
     * @Route("/new", methods={"GET", "POST"}, name="inventory_item_create")
     */
    public function create(Request $request, TranslatorInterface $translator): Response
    {
        $entity = new Item();
        $data = $request->request->all();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $form = $this->createForm(ItemFormType::class, $entity,array('inventory' => $config))
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {
            $entity->setConfig($config);
            $entity->setVatPercent($entity->getMasterItem()->getValueAddedTax());
            $entity->setSdPercent($entity->getMasterItem()->getSupplementaryDuty());
            $em->persist($entity);
            $em->flush();
            $this->addFlash('success',$translator->trans('post.insert_successfully'));
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:ItemKeyValue')->insertKeyValue($entity,$data);
            return $this->redirectToRoute('inventory_item_create');

        }
        return $this->render('@TerminalbdInventory/item/new.html.twig', [
            'id' => 'postForm',
            'entity' => $entity,
            'form' => $form->createView(),
            'actionUrl' => $this->generateUrl('inventory_item_create'),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit",methods={"GET", "POST"}, name="inventory_item_edit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_ITEM')")
     */
    public function edit(Request $request, Item $entity, TranslatorInterface $translator): Response
    {

        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $form = $this->createForm(ItemFormType::class, $entity,array('inventory' => $config))
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        $data = $request->request->all();
        if ($form->isSubmitted() && $form->isValid()) {
            if($entity->isTaxOverride() != true ){
                $entity->setVatPercent($entity->getMasterItem()->getValueAddedTax());
                $entity->setSdPercent($entity->getMasterItem()->getSupplementaryDuty());
            }
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success',$translator->trans('post.updated_successfully'));
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:ItemKeyValue')->insertKeyValue($entity,$data);
            return $this->redirectToRoute('inventory_item_edit',['id'=>$entity->getId()]);
        }
        return $this->render('@TerminalbdInventory/item/new.html.twig', [
            'actionUrl' => $this->generateUrl('inventory_item_edit',array('id'=> $entity->getId())),
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="inventory_item_show")
     * @Security("is_granted('ROLE_IINVENTORY_ITEM') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(Item::class)->find($id);
        return $this->render('@TerminalbdInventory/item/show.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="inventory_item_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_ITEM')")
     */

    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(Item::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Production a Setting entity.
     *
     * @Route("/{id}/production", methods={"GET"}, name="inventory_item_production")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_ITEM')")
     */
    public function production($id): Response
    {
        $post = $this->getDoctrine()->getRepository(Item::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $config = $this->getDoctrine()->getRepository(Production::class)->findConfig($this->getUser());
        $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionItem')->insertUpdate($config,$this->getUser(),$post);
        return $this->redirectToRoute('inventory_item_finish_goods');
    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="inventory_item_data_table")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY')")
     */

    public function dataTable(Request $request)
    {

        $query = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $iTotalRecords = $this->getDoctrine()->getRepository(Item::class)->count(array('config'=> $config));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Item')->findWithSearch($config,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post Item */

        foreach ($result as $post):

            $editUrl = $this->generateUrl('inventory_item_edit',array('id' => $post['id']));
            $deleteUrl = $this->generateUrl('inventory_item_delete',array('id'=> $post['id']));
            $viewUrl = $this->generateUrl('inventory_item_show',array('id'=> $post['id']));
            $productionUrl = $this->generateUrl('inventory_item_production',array('id'=> $post['id']));
            $records["data"][] = array(
                $id                     = $i,
                $hsCode                 = $post['hsCode'],
                $groupName              = $post['groupName'],
                $masterName             = $post['masterName'],
                $brandName              = $post['brandName'],
                $name                   = $post['name'],
                $unitName               = $post['unitName'],
                $sku                    = $post['sku'],
                $purchasePrice                      = $post['purchasePrice'],
                $salesPrice                         = $post['salesPrice'],
                $openingQuantity                    = $post['openingQuantity'],
                $purchaseQuantity                   = $post['purchaseQuantity'],
                $productionReceiveQuantity          = $post['productionReceiveQuantity'],
                $productionIssueQuantity            = $post['productionIssueQuantity'],
                $salesQuantity          = $post['salesQuantity'],
                $damageQuantity         = $post['damageQuantity'],
                $ongoingQuantity        = $post['ongoingQuantity'],
                $remainingQuantity      = $post['remainingQuantity'],

                $action             ="<div class='btn-group card-option'><button type='button' class='btn btn-notify' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fas fa-ellipsis-h'></i></button>
<ul class='list-unstyled card-option dropdown-info dropdown-menu dropdown-menu-right'>
<li class='dropdown-item'> <a data-action='{$viewUrl}' href='?process=postView-{$post['id']}&check=show#modal' id='postView-{$post['id']}' >View</a></li>
<li class='dropdown-item'> <a href='{$editUrl}'>Edit</a></li>
<li class='dropdown-item'> <a  data-action='{$deleteUrl}' class='remove' data-id='{$post['id']}' href='javascript:'>Remove</a></li>
</ul></div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }




    /**
     * Print a Item entity.
     *
     * @Route("/{id}/{mode}/print", methods={"GET"}, name="inventory_item_print")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_ITEM')")
     */

    public function print(Item $item,$mode)
    {

    }


}
