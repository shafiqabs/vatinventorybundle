<?php

namespace Terminalbd\InventoryBundle\Controller;

use App\Entity\Application\Inventory;
use App\Entity\Application\Production;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\Category;
use Terminalbd\InventoryBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\MasterItem;
use Terminalbd\InventoryBundle\Entity\Setting;
use Terminalbd\InventoryBundle\Form\ItemFormType;
use Terminalbd\InventoryBundle\Form\MasterItemFormType;
use Terminalbd\InventoryBundle\Repository\ItemRepository;
use Terminalbd\InventoryBundle\Repository\TaxTariffRepository;

/**
 * @Route("/inventory/master-item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class MasterItemController extends AbstractController
{

    /**
     * @Route("/", methods={"GET"}, name="inventory_masteritem")
     */
    public function index(Request $request): Response
    {
        $productTypes = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Setting')->getChildRecords("product-type");
        $productGroups = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Setting')->getChildRecords("product-group");
        $brands = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Setting')->getChildRecords("brand");
      //  $categories = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Category')->getChildRecords("brand");
        return $this->render('@TerminalbdInventory/masteritem/index.html.twig', [
            'productTypes' => $productTypes,
            'productGroups' => $productGroups,
            'brands' => $brands,
            'categories' => '',
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_ITEM')")
     * @Route("/new", methods={"GET", "POST"}, name="inventory_masteritem_create")
     */
    public function create(Request $request, TranslatorInterface $translator): Response
    {
        $entity = new MasterItem();
        $data = $request->request->all();
        $form = $this->createForm(MasterItemFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {
            $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
            $entity->setConfig($config);
            if(isset($data['hsCode'])){
                $hsCode = $data['hsCode'];
                $entity->setHsCode($hsCode);
            }
            $em->persist($entity);
            $em->flush();
            $this->addFlash('success',$translator->trans('post.insert_successfully'));
            if(!empty($entity->getHsCode())){
                $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:MasterItem')->updateTaxValue($entity);
            }
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:ItemKeyValue')->masterInsertKeyValue($entity,$data);
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:ItemVat')->masterInsertSurcharge($entity,$data);
            return $this->redirectToRoute('inventory_masteritem');

        }else{
           $this->getErrorsFromForm($form);
        }
        $surchargeItems = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Setting')->getChildObjectRecords('treasury-deposit');
        return $this->render('@TerminalbdInventory/masteritem/new.html.twig', [
            'id' => 'postForm',
            'entity' => $entity,
            'surchargeItems' => $surchargeItems,
            'form' => $form->createView(),
            'actionUrl' => $this->generateUrl('inventory_masteritem_create'),
        ]);
    }

    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit",methods={"GET", "POST"}, name="inventory_masteritem_edit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_ITEM')")
     */
    public function edit(Request $request, MasterItem $entity, TranslatorInterface $translator): Response
    {

      //  $entity = $this->getDoctrine()->getRepository(MasterItem::class)->find($id);
        $form = $this->createForm(MasterItemFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $data = $request->request->all();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success',$translator->trans('post.updated_successfully'));
            if($entity->getHsCode() != $data['hsCode']){
                $entity->setHsCode($data['hsCode']);
                $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:MasterItem')->updateTaxValue($entity);
            }
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:ItemKeyValue')->masterInsertKeyValue($entity,$data);
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:ItemVat')->masterInsertSurcharge($entity,$data);
            return $this->redirectToRoute('inventory_masteritem_edit',['id'=>$entity->getId()]);
        }
        $surchargeItems = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Setting')->getChildObjectRecords('treasury-deposit');
        return $this->render('@TerminalbdInventory/masteritem/new.html.twig', [
            'actionUrl' => $this->generateUrl('inventory_masteritem_edit',array('id'=> $entity->getId())),
            'entity' => $entity,
            'surchargeItems' => $surchargeItems,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="inventory_masteritem_show")
     * @Security("is_granted('ROLE_IINVENTORY_ITEM') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id): Response
    {
        $post = $this->getDoctrine()->getRepository(MasterItem::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="inventory_masteritem_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_ITEM')")
     */
    public function delete($id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()->getRepository(MasterItem::class)->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            return new Response('Data has been deleted successfully');

        } catch (ForeignKeyConstraintViolationException $e) {
            return new Response('Data has been relation another Table');
        }

    }



    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="inventory_masteritem_data_table")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY')")
     */

    public function dataTable(Request $request)
    {

        $query = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $iTotalRecords = $this->getDoctrine()->getRepository(MasterItem::class)->count(array('config'=> $config));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:MasterItem')->findWithSearch($config,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post Item */

        foreach ($result as $post):

            $editUrl = $this->generateUrl('inventory_masteritem_edit',array('id' => $post['id']));
            $deleteUrl = $this->generateUrl('inventory_masteritem_delete',array('id'=> $post['id']));
            $viewUrl = $this->generateUrl('inventory_masteritem_show',array('id'=> $post['id']));
            $records["data"][] = array(
                $id                 = $i,
                $hsCode             = $post['hsCode'],
                $groupName          = $post['groupName'],
                $categoryName            = $post['categoryName'],
                $name               = $post['name'],
                $unitName             = $post['unitName'],
                $vat              = $post['vat'],
                $cd              = $post['cd'],
                $sd              = $post['sd'],
                $rd              = $post['rd'],
                $ait              = $post['ait'],
                $at              = $post['at'],
                $action             ="<div class='btn-group card-option'><button type='button' class='btn btn-notify' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fas fa-ellipsis-h'></i></button>
<ul class='list-unstyled card-option dropdown-info dropdown-menu dropdown-menu-right'>
<li class='dropdown-item'> <a data-action='{$viewUrl}' href='?process=postView-{$post['id']}&check=show#modal' id='postView-{$post['id']}' >View</a></li>
<li class='dropdown-item'> <a href='{$editUrl}'>Edit</a></li>
<li class='dropdown-item'> <a  data-action='{$deleteUrl}' class='remove' data-id='{$post['id']}' href='javascript:'>Remove</a></li>
</ul></div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }


    /**
     * @Route("/product-hs-code", methods={"GET"}, name="inventory_item_tax_tarrif")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY')")
     */

    public function hsCodeSearch()
    {
        $item = $_REQUEST['q'];
        $data = array();
        $country = array();
        if ($item) {
            $items = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:TaxTariff')->searchAutoComplete($item);
            foreach ($items as $row)
            $data[] = array("id"=>$row['id'], "text"=>$row['text']);
        }
        $country['results'] = $data;
        return new JsonResponse($country);
    }

    /**
     * @Route("/{pram}/product-hs-code-search-selected", methods={"GET"}, name="inventory_item_tax_tarrif_search_selected")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY')")
     */

    public function hsCodeSearchSelected($pram)
    {
        return new JsonResponse(array(
            'id' => $pram,
            'text' => $pram
        ));
    }


}
