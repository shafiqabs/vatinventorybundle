<?php

namespace Terminalbd\InventoryBundle\Controller;

use App\Entity\Application\Inventory;
use App\Entity\Application\Production;
use App\Entity\Core\Setting;
use App\Entity\Core\SettingType;
use App\Form\Core\SettingFormType;
use App\Repository\Core\SettingTypeRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Terminalbd\InventoryBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\ProductionIssue;


/**
 * @Route("/inventory/receive")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ProductionReceiveController extends AbstractController
{

    /**
     * @Route("/", methods={"GET"}, name="production_receive")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_ITEM')")
     */
    public function index(Request $request): Response
    {
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $modes = array("raw-material","consumable","stockable","assembled");
        $products = $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:Item")->modeWiseStockItem($config,$modes);
        return $this->render('@TerminalbdInventory/production/index.html.twig',[
                'products' => $products
            ]
        );

    }


    /**
     * Search a Setting entity.
     *
     * @Route("/{id}/issue-item-search", methods={"GET"}, name="production_receive_item_search")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_SALES')")
     */

    public function particularSearch(Item $particular)
    {
        $unit = !empty($particular->getUnit() && !empty($particular->getUnit()->getName())) ? $particular->getUnit()->getName():'Unit';
        return new Response(json_encode(array('productId'=> $particular->getId() ,'salesPrice'=> $particular->getSalesPrice(), 'purchasePrice'=> $particular->getPurchasePrice(), 'remaining'=> $particular->getRemainingQuantity(), 'quantity'=> 1 , 'unit'=> $unit)));
    }

    /**
     * Check if mobile available for registering
     * @Route("/issue-item-insert", methods={"POST"}, name="inventory_create_receive_item")
     * @param   string
     * @return  bool
     */
    function itemInsertAjax(Request $reques) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $em = $this->getDoctrine()->getManager();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $item = $this->getDoctrine()->getRepository(Item::class)->find($data['productId']);
        $unit = !empty($item->getUnit() && !empty($item->getUnit()->getName())) ? $item->getUnit()->getName():'';
        $entity = new ProductionIssue();
        $entity->setConfig($config);
        $entity->setQuantity($data['quantity']);
        $entity->setTotalQuantity($data['quantity']);
        $entity->setItem($item);
        $entity->setName($item->getName());
        $entity->setUom($unit);
        $entity->setProcess('created');
        $entity->setPurchasePrice($item->getPurchasePrice());
        $entity->setSubTotal($item->getPurchasePrice() * $entity->getTotalQuantity());
        $em->persist($entity);
        $em->flush();
        return new Response('success');

    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="production_receive_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_production_receive')")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(ProductionIssue::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }


     /**
     * Deletes a Setting entity.
     *
     * @Route("/{mode}/process-group", methods={"GET"}, name="production_receive_process_group")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_production_receive')")
     */
    public function processGroup($mode): Response
    {
        $em = $this->getDoctrine()->getManager();
        $ids = explode(',',$_REQUEST['ids']);
        foreach ($ids as $key => $id):

            $post = $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:ProductionIssue")->find($id);
            $item = $post->getItem();
            $config = $this->getDoctrine()->getRepository(Production::class)->findConfig($this->getUser());
            $post->setProcess($mode);
            $em->flush();
            if($post->getProcess() == 'approved'){
                $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionInventory')->insertProductionInventory($config,$item);
            }
        endforeach;
        return new Response('Success');

    }


     /**
     * Process a Porduction Issue entity.
     *
     * @Route("/{id}/process", methods={"GET"}, name="production_receive_process")
      * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_production_receive')")
     */
    public function process($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(ProductionIssue::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $config = $this->getDoctrine()->getRepository(Production::class)->findConfig($this->getUser());
        $this->getDoctrine()->getRepository('TerminalbdProductionBundle:ProductionInventory')->insertProductionInventory($config,$entity->getItem());
        $entity->setProcess('approved');
        $em->flush();
        if($entity->getProcess() == 'approved'){
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->productionIssueInsertQnt($entity);
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Item')->productionUpdateQnt($entity);
        }
        return new Response('Success');

    }


    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="production_receive_data_table")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_production_receive')")
     */

    public function dataTable(Request $request)
    {

        $query = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $iTotalRecords = $this->getDoctrine()->getRepository(ProductionIssue::class)->count(array('config' => $config));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart, 'limit' => $iDisplayLength, 'orderBy' => $columnName, "order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:ProductionIssue')->findWithSearch($config, $parameter, $query);

        $i = $iDisplayStart > 0 ? ($iDisplayStart + 1) : 1;

        /* @var $post Item */

        foreach ($result as $post):

            $deleteUrl = $this->generateUrl('production_receive_delete', array('id' => $post['id']));
            $processUrl = $this->generateUrl('production_receive_process', array('id' => $post['id']));
            $checkBox = "";
            if ($post['process'] != "approved") {
                $checkBox = "<input class='process' type='checkbox' name='process' id='process' value='{$post['id']}' /> ";
            }
            $action = '';
            if ($post['process'] == "created"){
                $action = "<a  data-action='{$processUrl}' class='btn approve indigo-bg white-font btn-mini' data-id='{$post['id']}' href='javascript:'><i class='feather icon-check'></i>Check</a>
 <a  data-action='{$deleteUrl}' class='btn  btn-transparent btn-mini red-font remove' data-id='{$post['id']}' href='javascript:'><i class='fa fa-remove'></i></a>";
            }elseif ($post['process'] == "checked"){
                $action = "<a  data-action='{$processUrl}' class='btn approve green-bg white-font btn-mini' data-id='{$post['id']}' href='javascript:'><i class='feather icon-check-square'></i>Approve</a>
 <a  data-action='{$deleteUrl}' class='btn  btn-transparent btn-mini red-font remove' data-id='{$post['id']}' href='javascript:'><i class='fa fa-remove'></i></a>";
            }
            $created = $post['created']->format('d-m-Y');
            $records["data"][] = array(
                $checkbox           = $checkBox,
                $id                 = $i,
                $created            = $created,
                $name               = $post['name'],
                $uom                = $post['uom'],
                $quantity           = $post['quantity'],
                $purchasePrice      = $post['purchasePrice'],
                $subTotal           = $post['subTotal'],
                $process            = ucfirst($post['process']),
                $action             ="<div class='btn-group'>{$action}</div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }




}
