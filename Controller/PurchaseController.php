<?php
namespace Terminalbd\InventoryBundle\Controller;

use App\Entity\Admin\ProductUnit;
use App\Entity\Application\Accounting;
use App\Entity\Application\Inventory;
use App\Entity\Application\Nbrvat;
use App\Entity\Core\Setting;
use App\Entity\Core\Vendor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\InvoiceKeyValue;
use Terminalbd\InventoryBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\MasterItem;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Form\PurchaseImportFormType;
use Terminalbd\InventoryBundle\Form\PurchaseLocalFormType;
use Terminalbd\InventoryBundle\Repository\ItemRepository;


/**
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY')")
 * @Route("/inventory/purchase")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class PurchaseController extends AbstractController
{


    /**
     * @Route("/", methods={"GET"}, name="inventory_purchase")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function index(Request $request): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $vendors = $this->getDoctrine()->getRepository(Vendor::class)->findBy(array('terminal'=>$terminal,'status'=>1));
        return $this->render('@TerminalbdInventory/purchase/index.html.twig', [
            'vendors' => $vendors,
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     * @Route("/{mode}/new", methods={"GET", "POST"}, name="inventory_purchase_create")
     */

    public function create(Request $request,$mode): Response
    {
        $entity = new Purchase();
        $terminal = $this->getUser()->getTerminal();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $em = $this->getDoctrine()->getManager();
        $entity->setConfig($config);
        $receiveDate = new \DateTime('now');
        $entity->setReceiveDate($receiveDate);
        $entity->setBillOfEntryDate($receiveDate);
        $entity->setProcess('created');
        $entity->setCreatedBy($this->getUser());
        $entity->setAssignPerson($this->getUser()->getName());
        $entity->setAssignPersonDesignation($this->getUser()->getDesignation()->getName());
        $entity->setMode($mode);
        $em->persist($entity);
        $em->flush();
        return $this->redirectToRoute('inventory_purchase_edit',['id'=> $entity->getId()]);

    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit",methods={"GET", "POST"}, name="inventory_purchase_edit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function edit(Request $request, Purchase $entity): Response
    {

        if(in_array($entity->getProcess(),array("checked","approved","done"))){
            return $this->redirectToRoute('inventory_purchase_show',['id'=> $entity->getId()]);
        }
        $terminal = $this->getUser()->getTerminal();
        $account = $this->getDoctrine()->getRepository(Accounting::class)->findConfig($this->getUser());
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        if($entity->getMode() == "foreign") {
            $form = $this->createForm(PurchaseImportFormType::class, $entity, array('terminal' => $terminal, 'account' => $account ,'inventory' => $config))->add('SaveAndCreate', SubmitType::class);
        }else{
            $form = $this->createForm(PurchaseLocalFormType::class, $entity, array('terminal' => $terminal, 'account' => $account))->add('SaveAndCreate', SubmitType::class);
        }

        $data = $request->request->all();
        $form->handleRequest($request);
        $modes = array("raw-material","consumable","stockable","assembled","finish-goods");
        $products = $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:Item")->modeWiseStockItem($config,$modes);
        if ($form->isSubmitted() && $form->isValid()) {
            $entity->upload();
            if(empty($entity->getVendor())){
                $entity->setProcess('in-progress');
            }
            $this->getDoctrine()->getManager()->flush();
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:InvoiceKeyValue')->insertPurchaseKeyValue($entity,$data);
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:InvoiceKeyValue')->insertPurchaseKeyValue($entity,$data);
            return $this->redirectToRoute('inventory_purchase_edit',['id'=> $entity->getId()]);
        }

        if($entity->getMode() == "foreign"){
           $mode = "foreign";
            $purchaseTaxes = $this->getDoctrine()->getRepository("TerminalbdNbrvatBundle:Setting")->getPurchaseInputTax("part-4",'foreign');
        }else{
            $mode = "local";
            $purchaseTaxes = $this->getDoctrine()->getRepository("TerminalbdNbrvatBundle:Setting")->getPurchaseInputTax("part-4",'local');
        }
        $productGroups = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Setting')->getChildRecords("product-group");
        $units = $this->getDoctrine()->getRepository(ProductUnit::class)->findBy(array('status'=>1),array('name'=>"ASC"));

        return $this->render("@TerminalbdInventory/purchase/{$mode}/new.html.twig", [
            'actionUrl' => $this->generateUrl('inventory_purchase_edit',array('id'=> $entity->getId())),
            'dataAction' => $this->generateUrl('inventory_purchase_ajax',array('id'=> $entity->getId())),
            'terminal' => $terminal,
            'entity' => $entity,
            'productGroups' => $productGroups,
            'units' => $units,
            'products' => $products,
            'purchaseTaxes' => $purchaseTaxes,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-submit",methods={"GET", "POST"}, name="inventory_purchase_ajax")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */

    public function ajaxSubmit(Request $request, Purchase $entity): Response
    {
        $data = $_REQUEST;
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $account = $this->getDoctrine()->getRepository(Accounting::class)->findConfig($this->getUser());
        if($entity->getMode() == "foreign") {
            $form = $this->createForm(PurchaseImportFormType::class, $entity, array('terminal' => $terminal, 'account' => $account ,'inventory' => $config));
        }else{
            $form = $this->createForm(PurchaseLocalFormType::class, $entity, array('terminal' => $terminal, 'account' => $account));
        }
        $form->handleRequest($request);
        if ($form->isValid()) {
            if(empty($entity->getVendor())){
                $entity->setProcess('in-progress');
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
        }
        $invoice = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Purchase')->updatePurchaseTotalPrice($entity);
        $result = $this->returnResultData($invoice);
        return new Response(json_encode($result));
    }

    /**
     * @Route("/vendor-info", methods={"GET"}, name="inventory_vendor_info")
     * @param   string
     * @return  bool
     */
    function showVendorAjax(Request $request) : Response
    {
        $post = $_REQUEST['vendor'];
        $vendor = $this->getDoctrine()->getRepository(Vendor::class)->find($post);
        return new Response(json_encode(array('binNo'=> $vendor->getBinNo() ,'vendorMobile'=> $vendor->getMobile(), 'vendorAddress'=> $vendor->getAddress())));

    }

     /**
     * Check if mobile available for registering
     * @Route("/{purchase}/create-purchase-vendor", methods={"POST"}, name="inventory_vendor_create_ajax")
     * @param   string
     * @return  bool
     */
    function createVendorAjax(Request $request,Purchase $purchase) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $mode = "creatable";
        $status = $this->getDoctrine()->getRepository(Vendor::class)->checkAvailable($terminal,$mode,$data);
        if($status == 'true') {
            $post = new Vendor();
            $post->setTerminal($terminal);
            $vendorType = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('name' => $data['vendorType']));
            if ($vendorType) {
                $post->setVendorType($vendorType);
            }
            $post->setName($data['name']);
            $post->setCompanyName($data['company']);
            $post->setMobile($data['mobile']);
            $businessType = $this->getDoctrine()->getRepository(Setting::class)->findOneBy(array('name' => $data['businessType']));
            if ($businessType) {
                $post->setBusinessType($businessType);
            }
            if($data['address']){
                $post->setAddress($data['address']);
            }
            if($data['binNo']){
                $post->setBinNo($data['binNo']);
            }
            if($data['nidNo']){
                $post->setBinNo($data['nidNo']);
            }
            $this->getDoctrine()->getManager()->persist($post);
            $this->getDoctrine()->getManager()->flush();
            $purchase->setVendor($post);
            $this->getDoctrine()->getManager()->persist($purchase);
            $this->getDoctrine()->getManager()->flush();
            return new Response('valid');
        }else{
            return new Response('invalid');
        }
    }

    /**
     * @Route("/{purchase}/create-purchase-master-item", methods={"POST"}, name="inventory_purchase_item_create_ajax")
     * @param   string
     * @return  bool
     */
    function createMasterItemAjax(Request $request,Purchase $purchase) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $status = $this->getDoctrine()->getRepository(Item::class)->checkAvailable($config->getId(),$data);
        if($status == 'true') {
            $master = $this->getDoctrine()->getRepository(MasterItem::class)->insertInstantMasterItem($config,$data);
            $this->getDoctrine()->getRepository(Item::class)->insertInstantItem($config,$master,$data);
            return new Response('valid');
        }else{
            return new Response('invalid');
        }
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="inventory_purchase_show")
     * @Security("is_granted('ROLE_Iinventory_purchas') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(Purchase::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        if($entity->getMode() == "foreign"){
            $mode = "foreign";
        }else{
            $mode = "local";
        }
        return $this->render("@TerminalbdInventory/purchase/{$mode}/show.html.twig", [
            'entity' => $entity,
        ]);
    }



    private function returnResultData(Purchase $invoice,$msg=''){

        if($invoice->getMode() == "foreign") {
            $invoiceItems = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:PurchaseItem')->getPurchaseForeignItems($invoice);
        }else{
            $invoiceItems = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:PurchaseItem')->getPurchaseLocalItems($invoice);
        }

        $subTotal = $invoice->getSubTotal() > 0 ? $invoice->getSubTotal() : 0;
        $netTotal = $invoice->getNetTotal() > 0 ? $invoice->getNetTotal() : 0;
        $total = $invoice->getTotal() > 0 ? $invoice->getTotal() : 0;
        $due = $invoice->getDue() > 0 ? $invoice->getDue() : 0;
        $discount = $invoice->getDiscount() > 0 ? $invoice->getDiscount() : 0;
        $data = array(
            'quantity' => $invoice->getQuantity(),
            'subTotal' => number_format($subTotal,2),
            'cd' => number_format($invoice->getCustomsDuty(),2),
            'rd' => number_format($invoice->getRegulatoryDuty(),2),
            'sd' => number_format($invoice->getSupplementaryDuty(),2),
            'vat' => number_format($invoice->getValueAddedTax(),2),
            'ait' => number_format($invoice->getAdvanceIncomeTax(),2),
            'at' => number_format($invoice->getAdvanceIncomeTax(),2),
            'tti' => number_format($invoice->getTotalTaxIncidence(),2),
            'netTotal' => number_format($netTotal,2),
            'paymentTotal' => $netTotal,
            'total' => number_format($total,2),
            'due' => number_format($due,2),
            'discount' => number_format($discount,2),
            'discountMode' => $invoice->getDiscountMode(),
            'discountPercent' => $invoice->getDiscountCalculation(),
            'invoiceItems' => $invoiceItems ,
            'msg' => $msg ,
            'success' => 'success'
        );

        return $data;

    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/product-search", methods={"GET"}, name="inventory_purchase_item_search")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */

    public function particularSearch(Item $particular)
    {
        $unit = !empty($particular->getUnit() && !empty($particular->getUnit()->getName())) ? $particular->getUnit()->getName():'Unit';
      //  $vatMode = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:Setting')->supplyInputTax($particular);
        return new Response(json_encode(array('productId'=> $particular->getId() ,'salesPrice'=> $particular->getSalesPrice(), 'purchasePrice'=> $particular->getPurchasePrice(), 'quantity'=> 1 , 'unit'=> $unit,'vatMode' => '')));
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/purchase-input-tax", methods={"GET"}, name="inventory_purchase_input_tax")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */

    public function purchaseInputTax()
    {
        $particular = $_REQUEST['particular'];
        $data = "";
        $config = $this->getDoctrine()->getRepository(Nbrvat::class)->findConfig($this->getUser());
        $vatMode = $this->getDoctrine()->getRepository('TerminalbdNbrvatBundle:TaxSetup')->getTaxSetupOutput($config,$particular);
        return new Response($data);

    }

    /**
     * Check if mobile available for registering
     * @Route("/{id}/purchase-item-insert", methods={"POST"}, name="inventory_create_purchase_item")
     * @param   string
     * @return  bool
     */
    function itemInsertAjax(Request $request, Purchase $purchase) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $em = $this->getDoctrine()->getManager();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        if($purchase->getMode() == "local"){
            $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:PurchaseItem")->insertUpdateLocalItem($config,$purchase,$data);
        }else{
            $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:PurchaseItem")->insertForeignUpdateItem($config,$purchase,$data);
        }

        $invoice = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Purchase')->updatePurchaseTotalPrice($purchase);
        $result = $this->returnResultData($invoice);
        return new Response(json_encode($result));

    }

    /**
     * Check if mobile available for registering
     * @Route("/purchase-item-update", methods={"POST"}, name="inventory_create_purchase_item_update")
     * @param   string
     * @return  bool
     */
    function itemUpdateAjax(Request $request) : Response
    {
        $data = $_REQUEST;
        $item = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:PurchaseItem')->find($data['purchaseItem']);
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        if($item->getPurchase()->getMode() == 'local'){
            $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:PurchaseItem")->updateLocalItem($item,$data);
        }else{
            $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:PurchaseItem")->updateForeignItem($item,$data);
        }
        $invoice = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Purchase')->updatePurchaseTotalPrice($item->getPurchase());
        $result = $this->returnResultData($invoice);
        return new Response(json_encode($result));

    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{invoice}/{item}/item-delete", methods={"GET"}, name="inventory_purchase_item_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function deleteItem(Purchase $invoice,$item): Response
    {
        $post = $this->getDoctrine()->getRepository(PurchaseItem::class)->find($item);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $purchase = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Purchase')->updatePurchaseTotalPrice($invoice);
        $result = $this->returnResultData($purchase);
        return new Response(json_encode($result));
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{meta}/delete-invoice-meta", methods={"GET"}, name="inventory_purchase_delete_invoice_meta")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function deleteInvoiceMeta(InvoiceKeyValue $meta): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($meta);
        $em->flush();
        return new Response('success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="inventory_purchase_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(Purchase::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/{mode}/preview", methods={"GET"}, name="inventory_purchase_preview")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function print($id,$mode): Response
    {
        $entity = $this->getDoctrine()->getRepository(Purchase::class)->find($id);
        $domain =$this->getUser()->getTerminal();


        if($mode == "pdf"){

          /*  $mpdf = new Mpdf(['tempDir' => 'public/uploads/temp']);
            $html = $this->renderView(
                'print/preview.html.twig',[
                'post' => $post,
                'mode' => 'pdf',
            ]);*/
            $mpdf->WriteHTML($html);
            $mpdf->Output();

        }elseif($mode == "mushok"){

            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Nicola Asuni');
            $pdf->SetTitle('TCPDF Example 001');
            $pdf->SetSubject('TCPDF Tutorial');
            $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
            $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            // set font
        //    $pdf->SetFont('', '', 10);
            $fontname = \TCPDF_FONTS::addTTFfont('http://uipoka.local/Fonts/SolaimanLipi.ttf', 'TrueTypeUnicode', '', 96);
            $pdf->SetFont($fontname, '', 10, '', false);
// add a page
            $pdf->AddPage();
            $html = $this->renderView('@TerminalbdInventory/mushok/test.twig', $data = array());
            $pdf->writeHTML($html, true, false, true, false, '');

            $pdf->Output('example_001.pdf', 'I');
            exit;

        }elseif($mode == "print"){

            return $this->render('@TerminalbdInventory/mushok/mushok_6_3.html.twig', [
                'domain' => $domain,
                'entity' => $entity,
                'mode' => 'print',
            ]);

        }else{

            return $this->render('@TerminalbdInventory/purchase/local/show.html.twig', [
                'domain' => $domain,
                'entity' => $entity,
                'products' => $products,
                'form' => $form->createView(),
            ]);
        }

    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/mushok6", methods={"GET"}, name="inventory_purchase_mushok6")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function mushok6($id): Response
    {
        $post = $this->getDoctrine()->getRepository(Purchase::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Approved a Setting entity.
     *
     * @Route("/{id}/{process}/process", methods={"GET"}, name="inventory_purchase_process")
     * @Security("is_granted('ROLE_Iinventory_purchas') or is_granted('ROLE_DOMAIN')")
     */
    public function process($id,$process): Response
    {
        $entity = $this->getDoctrine()->getRepository(Purchase::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        if ($entity->getProcess() == 'done'){
            $entity->setCheckedBy($this->getUser());
            $entity->setProcess($process);
        }elseif ($entity->getProcess() == 'checked'){
            $entity->setApprovedBy($this->getUser());
            $entity->setProcess($process);
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Purchase')->updateVAT($this->getUser(),$entity);
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->getPurchaseInsertQnt($entity);
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Item')->getPurchaseUpdateQnt($entity);
        }
        $em->persist($entity);
        $em->flush();
        return new Response('success');
    }

    /**
     * Approved a Setting entity.
     *
     * @Route("/{id}/reverse", methods={"GET"}, name="inventory_purchase_reverse")
     * @Security("is_granted('ROLE_INVENTORY_REVERSE') or is_granted('ROLE_DOMAIN')")
     */
    public function reverse($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(Purchase::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->createQuery("DELETE TerminalbdInventoryBundle:StockItem e WHERE e.purchase = '{$entity->getId()}'")->execute();
        $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Item')->getPurchaseUpdateQnt($entity);
        $entity->setReverse(true);
        $entity->setProcess('created');
        $em->persist($entity);
        $em->flush();
        return new Response('success');
    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="inventory_purchase_data_table")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */

    public function dataTable(Request $request)
    {

        $query = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $iTotalRecords = $this->getDoctrine()->getRepository(Purchase::class)->count(array('config'=> $config));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Purchase')->findWithSearch($config,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post Item */

        foreach ($result as $post):

            $editUrl = $this->generateUrl('inventory_purchase_edit',array('id' => $post['id']));
            $deleteUrl = $this->generateUrl('inventory_purchase_delete',array('id'=> $post['id']));
            $viewUrl = $this->generateUrl('inventory_purchase_show',array('id'=> $post['id']));
            $created = $post['created']->format('d-m-Y');
            $received = $post['received']->format('d-m-Y');
            if($post['discountMode'] == "percent"){
                $calculation = $post['calculation']."%";
            }else{
                $calculation = $post['calculation'];
            }
            $records["data"][] = array(
                $id                 = $i,
                $created            = $created,
                $received            = $received,
                $invoice            = $post['invoice'],
                $companyName        = $post['companyName'],
                $method             = ucfirst($post['method']),
                $subTotal           = $post['subTotal'],
                $discount           = $post['discount'],
                $calculation        = $calculation,
                $taxTotal           = $post['taxTotal'],
                $netTotal           = $post['netTotal'],
                $payment           = $post['payment'],
                $due                = $post['due'],
                $mode               = ucfirst($post['mode']),
                $process            = ucfirst($post['process']),
                $action             ="<div class='btn-group card-option'><button type='button' class='btn btn-notify' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fas fa-ellipsis-h'></i></button>
<ul class='list-unstyled card-option dropdown-info dropdown-menu dropdown-menu-right'>
 <li class='dropdown-item'> <a href='{$viewUrl}' >View</a></li>
<li class='dropdown-item'> <a href='{$editUrl}'>Edit</a></li>
<li class='dropdown-item'> <a  data-action='{$deleteUrl}' class='remove' data-id='{$post['id']}' href='javascript:'>Remove</a></li>
</ul></div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }

}
