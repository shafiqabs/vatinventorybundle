<?php

namespace Terminalbd\InventoryBundle\Controller;
use App\Entity\Application\Inventory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\ItemKeyValue;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Form\PurchaseItemFormType;


/**
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY')")
 * @Route("/inventory/purchase-item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class PurchaseItemController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="inventory_purchaseitem")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function index(Request $request): Response
    {
        $entities = $this->getDoctrine()->getRepository(PurchaseItem::class)->findBy(array('mode'=>"opening"));
        return $this->render('@TerminalbdInventory/purchase-item/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     * @Route("/new", methods={"GET","POST"}, name="inventory_purchaseitem_new")
     */
    public function new(Request $request): Response
    {

        $entity = new PurchaseItem();
        $data = $request->request->all();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $form = $this->createForm(PurchaseItemFormType::class , $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($config);
            $entity->setMode("opening");
            $entity->setTotalQuantity($entity->getQuantity());
            $subTotal = ($entity->getQuantity() * $entity->getPurchasePrice());
            $entity->setSubTotal($subTotal);
            $em->persist($entity);
            $em->flush();
            $this->addFlash('success', 'post.created_successfully');
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('inventory_purchaseitem_new');
            }
            return $this->redirectToRoute('inventory_purchaseitem');
        }
        return $this->render('@TerminalbdInventory/purchase-item/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="inventory_purchaseitem_edit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function edit(Request $request, PurchaseItem $entity): Response
    {
        $data = $request->request->all();
        $form = $this->createForm(PurchaseItemFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'post.updated_successfully');
            $this->getDoctrine()->getRepository(ItemKeyValue::class)->insertPurchaseItemKeyValue($entity,$data);
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('inventory_purchaseitem_edit', ['id' => $entity->getId()]);
            }

            return $this->redirectToRoute('inventory_purchaseitem');
        }
        return $this->render('@TerminalbdInventory/purchase-item/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a PurchaseItem entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="inventory_purchaseitem_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function delete($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(PurchaseItem::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

   
}
