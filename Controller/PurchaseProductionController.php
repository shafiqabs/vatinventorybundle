<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\InventoryBundle\Controller;

use App\Entity\Application\Inventory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\Category;
use Terminalbd\InventoryBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\Setting;
use Terminalbd\InventoryBundle\Form\ItemFormType;
use Terminalbd\InventoryBundle\Repository\ItemRepository;

/**
 * @Security("is_granted('ROLE_DOMAIN') or (is_granted('ROLE_INVENTORY') and is_granted('ROLE_PRODUCTION_PURCHASE'))")
 * @Route("/inventory/purchase/production")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class PurchaseProductionController extends AbstractController
{

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }


    /**
     * @Route("/", methods={"GET"}, name="inventory_purchase_production")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function index(Request $request): Response
    {
        $productTypes = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Setting')->getChildRecords("product-type");
        $productGroups = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Setting')->getChildRecords("product-group");
        $brands = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Setting')->getChildRecords("brand");
      //  $categories = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Category')->getChildRecords("brand");
        return $this->render('@TerminalbdInventory/purchase/production/index.html.twig', [
            'productTypes' => $productTypes,
            'productGroups' => $productGroups,
            'brands' => $brands,
            'categories' => '',
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     * @Route("/new", methods={"GET", "POST"}, name="inventory_purchase_production_create")
     */
    public function create(Request $request): Response
    {
        $entity = new Purchase();
        $form = $this->createForm(ItemFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {
            $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
            $entity->setConfig($config);
            $em->persist($entity);
            $em->flush();
            $msg = $this->translator->trans('post.insert_successfully');
            return new Response($msg);
        }
        return $this->render('@TerminalbdInventory/purchase/production/new.html.twig', [
            'id' => 'postForm',
            'form' => $form->createView(),
            'actionUrl' => $this->generateUrl('inventory_purchase_production_create'),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit",methods={"GET", "POST"}, name="inventory_purchase_production_edit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function edit(Request $request, Purchase $entity): Response
    {

      //  $entity = $this->getDoctrine()->getRepository(Item::class)->find($id);
        $form = $this->createForm(ItemFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $msg = $this->translator->trans('post.updated_successfully');
            return new Response($msg);
        }
        return $this->render('@TerminalbdInventory/purchase/production/new.html.twig', [
            'actionUrl' => $this->generateUrl('inventory_purchase_production_edit',array('id'=> $entity->getId())),
            'post' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="inventory_purchase_production_show")
     * @Security("is_granted('ROLE_Iinventory_purchase_production') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id): Response
    {
        $post = $this->getDoctrine()->getRepository(Purchase::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="inventory_purchase_production_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(Purchase::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="inventory_purchase_production_data_table")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */

    public function dataTable(Request $request)
    {

        $query = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $iTotalRecords = $this->getDoctrine()->getRepository(Purchase::class)->count(array('config'=> $config));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Purchase')->findWithSearch($config,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post Item */

        foreach ($result as $post):

            $editUrl = $this->generateUrl('inventory_purchase_production_edit',array('id' => $post['id']));
            $deleteUrl = $this->generateUrl('inventory_purchase_production_delete',array('id'=> $post['id']));
            $viewUrl = $this->generateUrl('inventory_purchase_production_show',array('id'=> $post['id']));
            $records["data"][] = array(
                $id                 = $i,
                $productType        = $post['productType'],
                $groupName               = $post['groupName'],
                $categoryName            = $post['categoryName'],
                $name               = $post['name'],
                $unitName             = $post['unitName'],
                $barcode             = $post['barcode'],
                $sku              = $post['sku'],
                $purchasePrice              = $post['purchasePrice'],
                $salesPrice              = $post['salesPrice'],
                $openingQuantity              = $post['openingQuantity'],
                $purchaseQuantity              = $post['purchaseQuantity'],
                $purchaseReturnQuantity              = $post['purchaseReturnQuantity'],
                $salesQuantity              = $post['salesQuantity'],
                $salesReturnQuantity              = $post['salesReturnQuantity'],
                $damageQuantity              = $post['damageQuantity'],
                $ongoingQuantity              = $post['ongoingQuantity'],
                $remainingQuantity              = $post['remainingQuantity'],

                $action             ="<div class='btn-group card-option'><button type='button' class='btn btn-notify' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fas fa-ellipsis-h'></i></button>
<ul class='list-unstyled card-option dropdown-info dropdown-menu dropdown-menu-right'>
 <li class='dropdown-item'> <a data-action='{$viewUrl}' href='?process=postView-{$post['id']}&check=show#modal' id='postView-{$post['id']}' >View</a></li>
<li class='dropdown-item'> <a data-action='{$editUrl}' href='?process=postEdit-{$post['id']}&check=edit#modal' id='postEdit-{$post['id']}'>Edit</a></li>
<li class='dropdown-item'> <a  data-action='{$deleteUrl}' class='remove' data-id='{$post['id']}' href='javascript:'>Remove</a></li>
</ul></div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }

}
