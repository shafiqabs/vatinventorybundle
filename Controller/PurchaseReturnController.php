<?php
namespace Terminalbd\InventoryBundle\Controller;

use App\Entity\Application\Accounting;
use App\Entity\Application\Inventory;
use App\Entity\Core\Setting;
use App\Entity\Core\Vendor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\InvoiceKeyValue;
use Terminalbd\InventoryBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Entity\PurchaseReturn;
use Terminalbd\InventoryBundle\Form\PurchaseReturnExportFormType;
use Terminalbd\InventoryBundle\Form\PurchaseReturnLocalFormType;
use Terminalbd\InventoryBundle\Repository\ItemRepository;


/**
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY')")
 * @Route("/inventory/purchase-return")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class PurchaseReturnController extends AbstractController
{


    /**
     * @Route("/", methods={"GET"}, name="inventory_purchase_return")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE_RETURN')")
     */
    public function index(Request $request): Response
    {
        $productTypes = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Setting')->getChildRecords("product-type");
        return $this->render('@TerminalbdInventory/purchase-return/index.html.twig', [
            'categories' => '',
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE_RETURN')")
     * @Route("/{mode}/new", methods={"GET", "POST"}, name="inventory_purchase_return_create")
     */

    public function create(Request $request,$mode): Response
    {
        $entity = new PurchaseReturn();
        $terminal = $this->getUser()->getTerminal();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $em = $this->getDoctrine()->getManager();
        $entity->setConfig($config);
        $receiveDate = new \DateTime('now');
        $entity->setReceiveDate($receiveDate);
        $entity->setProcess('created');
        $entity->setCreatedBy($this->getUser());
        $entity->setAssignPerson($this->getUser()->getName());
        $entity->setAssignPersonDesignation($this->getUser()->getDesignation()->getName());
        $entity->setMode($mode);
        $em->persist($entity);
        $em->flush();
        return $this->redirectToRoute('inventory_purchase_return_edit',['id'=> $entity->getId()]);

    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit",methods={"GET", "POST"}, name="inventory_purchase_return_edit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE_RETURN')")
     */
    public function edit(Request $request, PurchaseReturn $entity): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        if(in_array($entity->getProcess(),array("checked","approved"))){
            return $this->redirectToRoute('inventory_purchase_return_show',['id'=> $entity->getId()]);
        }
        if($entity->getMode() == "foreign") {
            $form = $this->createForm(PurchaseReturnExportFormType::class, $entity,array('inventory' => $inventory))
                ->add('SaveAndCreate', SubmitType::class);
        }else{
            $form = $this->createForm(PurchaseReturnLocalFormType::class, $entity,array('inventory' => $inventory))
                ->add('SaveAndCreate', SubmitType::class);
        }
        $data = $request->request->all();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $purchaseId = $data['purchaseInvoiceId'];
            if($purchaseId){
                $purchase = $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:Purchase")->find($purchaseId);
                $entity->setPurchase($purchase);
            }
            $entity->upload();
            $this->getDoctrine()->getManager()->flush();
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:PurchaseReturnItem')->insertPurchaseKeyItem($entity,$data);
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:PurchaseReturn')->updatePurchaseTotalPrice($entity);
            if($entity->getProcess() == 'done'){
                return $this->redirectToRoute('inventory_purchase_return_show',['id'=> $entity->getId()]);
            }
            return $this->redirectToRoute('inventory_purchase_return_edit',['id'=> $entity->getId()]);
        }
        $errors = $this->getErrorsFromForm($form);
        if($entity->getMode() == "foreign"){
            $mode = "foreign";
        }else{
            $mode = "local";
        }
        $returnItems = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:PurchaseReturnItem')->getPurchaseReturnList($entity);
        return $this->render("@TerminalbdInventory/purchase-return/{$mode}/new.html.twig", [
            'actionUrl' => $this->generateUrl('inventory_purchase_return_edit',array('id'=> $entity->getId())),
            'dataAction' => $this->generateUrl('inventory_purchase_return_ajax',array('id'=> $entity->getId())),
            'entity' => $entity,
            'returnItems' => $returnItems,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-submit",methods={"GET", "POST"}, name="inventory_purchase_return_ajax")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE_RETURN')")
     */

    public function ajaxSubmit(Request $request, PurchaseReturn $entity): Response
    {
        $data = $_REQUEST;
        $data = $request->request->all();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        if($entity->getMode() == "foreign") {
            $form = $this->createForm(PurchaseReturnExportFormType::class, $entity, array('inventory' => $config));
        }else{
            $form = $this->createForm(PurchaseReturnLocalFormType::class, $entity, array('inventory' => $config));
        }
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
        }
        $invoice = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:PurchaseReturn')->updatePurchaseTotalPrice($entity);
        return new Response('success');
    }

    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }



    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="inventory_purchase_return_show")
     * @Security("is_granted('ROLE_INVENTORY_PURCHASE_RETURN') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(PurchaseReturn::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        if($entity->getMode() == "foreign"){
            $mode = "foreign";
        }else{
            $mode = "local";
        }
        return $this->render("@TerminalbdInventory/purchase-return/{$mode}/show.html.twig", [
            'entity' => $entity,
        ]);
    }

    /**
     * Approved a Setting entity.
     *
     * @Route("/{id}/{process}/process", methods={"GET"}, name="inventory_purchase_return_process")
     * @Security("is_granted('ROLE_INVENTORY_PURCHASE_RETURN') or is_granted('ROLE_DOMAIN')")
     */
    public function process($id,$process): Response
    {
        $entity = $this->getDoctrine()->getRepository(PurchaseReturn::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        if ($entity->getProcess() == 'done'){
            $entity->setCheckedBy($this->getUser());
            $entity->setProcess($process);
        }elseif ($entity->getProcess() == 'checked'){
            $entity->setApprovedBy($this->getUser());
            $entity->setProcess($process);
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->getPurchaseReturnInsertQnt($entity);
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Item')->getPurchaseReturnUpdateQnt($entity);

        }
        $em->persist($entity);
        $em->flush();
        return new Response('success');
    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="inventory_purchase_return_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(PurchaseReturn::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="inventory_purchase_return_data_table")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */

    public function dataTable(Request $request)
    {

        $query = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $iTotalRecords = $this->getDoctrine()->getRepository(PurchaseReturn::class)->count(array('config'=> $config));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:PurchaseReturn')->findWithSearch($config,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post Item */

        foreach ($result as $post):

            $editUrl = $this->generateUrl('inventory_purchase_return_edit',array('id' => $post['id']));
            $deleteUrl = $this->generateUrl('inventory_purchase_return_delete',array('id'=> $post['id']));
            $viewUrl = $this->generateUrl('inventory_purchase_return_show',array('id'=> $post['id']));
            $created = $post['created']->format('d-m-Y');
            $records["data"][] = array(
                $id                 = $i,
                $created            = $created,
                $invoice            = $post['invoice'],
                $companyName        = $post['companyName'],
                $purchaseInvoice    = $post['purchaseInvoice'],
                $invoiceMode        = $post['invoiceMode'],
                $subTotal           = $post['subTotal'],
                $taxTotal           = $post['taxTotal'],
                $netTotal           = $post['netTotal'],
                $mode               = ucfirst($post['mode']),
                $process            = ucfirst($post['process']),
                $action             ="<div class='btn-group card-option'><button type='button' class='btn btn-notify' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fas fa-ellipsis-h'></i></button>
<ul class='list-unstyled card-option dropdown-info dropdown-menu dropdown-menu-right'>
 <li class='dropdown-item'> <a href='{$viewUrl}' >View</a></li>
<li class='dropdown-item'> <a href='{$editUrl}'>Edit</a></li>
<li class='dropdown-item'> <a  data-action='{$deleteUrl}' class='remove' data-id='{$post['id']}' href='javascript:'>Remove</a></li>
</ul></div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }

}
