<?php
namespace Terminalbd\InventoryBundle\Controller;

use App\Entity\Application\Accounting;
use App\Entity\Application\Inventory;
use App\Entity\Core\Setting;
use App\Entity\Core\Vendor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\InvoiceKeyValue;
use Terminalbd\InventoryBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Form\PurchaseLocalFormType;
use Terminalbd\InventoryBundle\Repository\ItemRepository;


/**
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY')")
 * @Route("/inventory/purchase-service")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class PurchaseServiceController extends AbstractController
{


    /**
     * @Route("/", methods={"GET"}, name="inventory_purchase_service")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function index(Request $request): Response
    {
        $productTypes = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Setting')->getChildRecords("product-type");
        return $this->render('@TerminalbdInventory/purchase/service/index.html.twig', [
            'categories' => '',
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     * @Route("/{mode}/new", methods={"GET", "POST"}, name="inventory_purchase_service_create")
     */

    public function create(Request $request,$mode): Response
    {
        $entity = new Purchase();
        $terminal = $this->getUser()->getTerminal();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $em = $this->getDoctrine()->getManager();
        $entity->setConfig($config);
        $receiveDate = new \DateTime('now');
        $entity->setReceiveDate($receiveDate);
        $entity->setProcess('created');
        $entity->setCreatedBy($this->getUser());
        $entity->setAssignPerson($this->getUser()->getName());
        $entity->setAssignPersonDesignation($this->getUser()->getDesignation()->getName());
        $entity->setMode($mode);
        $entity->setInvoiceType('service');
        $em->persist($entity);
        $em->flush();
        return $this->redirectToRoute('inventory_purchase_service_edit',['id'=> $entity->getId()]);

    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit",methods={"GET", "POST"}, name="inventory_purchase_service_edit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function edit(Request $request, Purchase $entity): Response
    {
        $terminal = $this->getUser()->getTerminal();
        if(in_array($entity->getProcess(),array("checked","approved"))){
            return $this->redirectToRoute('inventory_purchase_service_show',['id'=> $entity->getId()]);
        }
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $account = $this->getDoctrine()->getRepository(Accounting::class)->findConfig($this->getUser());
        $form = $this->createForm(PurchaseLocalFormType::class, $entity,array('terminal' => $terminal,'account' => $account))
            ->add('SaveAndCreate', SubmitType::class);
        $data = $request->request->all();
        $form->handleRequest($request);
        $modes = array("service","assembled");
        $products = $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:MasterItem")->modeWiseStockItem($config,$modes);
        if ($form->isSubmitted() && $form->isValid()) {
            $entity->upload();
            $this->getDoctrine()->getManager()->flush();
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:InvoiceKeyValue')->insertPurchaseKeyValue($entity,$data);
            return $this->redirectToRoute('inventory_purchase_service_edit',['id'=> $entity->getId()]);
        }

        if($entity->getMode() == "registered"){
           $mode = "registered";
        }else{
            $mode = "non-registered";
        }
        return $this->render("@TerminalbdInventory/purchase/service/{$mode}.html.twig", [
            'actionUrl' => $this->generateUrl('inventory_purchase_service_edit',array('id'=> $entity->getId())),
            'dataAction' => $this->generateUrl('inventory_purchase_service_ajax',array('id'=> $entity->getId())),
            'entity' => $entity,
            'products' => $products,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-submit",methods={"GET", "POST"}, name="inventory_purchase_service_ajax")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */

    public function ajaxSubmit(Request $request, Purchase $entity): Response
    {
        $data = $_REQUEST;
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal();
        $account = $this->getDoctrine()->getRepository(Accounting::class)->findConfig($this->getUser());
        $form = $this->createForm(PurchaseLocalFormType::class, $entity,array('terminal' => $terminal,'account' => $account));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
        }
        $invoice = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Purchase')->updatePurchaseTotalPrice($entity);
        $result = $this->returnResultData($invoice);
        return new Response(json_encode($result));
    }



    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="inventory_purchase_service_show")
     * @Security("is_granted('ROLE_Iinventory_purchas') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(Purchase::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        return $this->render('@TerminalbdInventory/purchase/local/show.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * Approved a Setting entity.
     *
     * @Route("/{id}/{process}/process", methods={"GET"}, name="inventory_purchase_service_process")
     * @Security("is_granted('ROLE_Iinventory_purchas') or is_granted('ROLE_DOMAIN')")
     */
    public function process($id,$process): Response
    {
        $entity = $this->getDoctrine()->getRepository(Purchase::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        if ($entity->getProcess() == 'done'){
            $entity->setCheckedBy($this->getUser());
            $entity->setProcess($process);
        }elseif ($entity->getProcess() == 'checked'){
            $entity->setApprovedBy($this->getUser());
            $entity->setProcess($process);
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:StockItem')->getSalesInsertQnt($entity);
            $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Item')->getSalesUpdateQnt($entity);

        }
        $em->persist($entity);
        $em->flush();
        return $this->render('@TerminalbdInventory/purchase/local/show.html.twig', [
            'entity' => $entity,
        ]);
    }

    private function returnResultData(Purchase $invoice,$msg=''){


        if($invoice->getInvoiceType() == "service" and $invoice->getMode() =='registered') {
            $invoiceItems = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:PurchaseItem')->getPurchaseRegisteredItems($invoice);
        }elseif($invoice->getInvoiceType() == "service" and $invoice->getMode() =='non-registered'){
            $invoiceItems = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:PurchaseItem')->getPurchaseNonRegisteredItems($invoice);
        }

        $subTotal = $invoice->getSubTotal() > 0 ? $invoice->getSubTotal() : 0;
        $netTotal = $invoice->getNetTotal() > 0 ? $invoice->getNetTotal() : 0;
        $totalVat = $invoice->getTotalTaxIncidence() > 0 ? $invoice->getTotalTaxIncidence() : 0;
        $total = $invoice->getTotal() > 0 ? $invoice->getTotal() : 0;
        $due = $invoice->getDue() > 0 ? $invoice->getDue() : 0;
        $discount = $invoice->getDiscount() > 0 ? $invoice->getDiscount() : 0;
        $data = array(
            'subTotal' => number_format($subTotal,2),
            'netTotal' => number_format($netTotal,2),
            'paymentTotal' => $netTotal,
            'total' => number_format($total,2),
            'totalVat' => number_format($totalVat,2),
            'due' => number_format($due,2),
            'discount' => number_format($discount,2),
            'discountMode' => $invoice->getDiscountMode(),
            'discountPercent' => $invoice->getDiscountCalculation(),
            'invoiceItems' => $invoiceItems ,
            'msg' => $msg ,
            'success' => 'success'
        );

        return $data;

    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/product-search", methods={"GET"}, name="inventory_purchase_service_item_search")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */

    public function particularSearch(Item $particular)
    {
        $unit = !empty($particular->getUnit() && !empty($particular->getUnit()->getName())) ? $particular->getUnit()->getName():'Unit';
        return new Response(json_encode(array('productId'=> $particular->getId() ,'salesPrice'=> $particular->getSalesPrice(), 'purchasePrice'=> $particular->getPurchasePrice(), 'quantity'=> 1 , 'unit'=> $unit)));
    }

    /**
     * Check if mobile available for registering
     * @Route("/{id}/purchase-service-item-insert", methods={"POST"}, name="inventory_create_purchase_service_item")
     * @param   string
     * @return  bool
     */
    function itemInsertAjax(Request $request, Purchase $purchase) : Response
    {
        $data = $_REQUEST;
        $terminal = $this->getUser()->getTerminal()->getId();
        $em = $this->getDoctrine()->getManager();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:PurchaseItem")->insertUpdateServiceItem($config,$purchase,$data);
        $invoice = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Purchase')->updatePurchaseTotalPrice($purchase);
        $result = $this->returnResultData($invoice);
        return new Response(json_encode($result));

    }

    /**
     * Check if mobile available for registering
     * @Route("/purchase-service-item-update", methods={"POST"}, name="inventory_purchase_service_item_update")
     * @param   string
     * @return  bool
     */
    function itemUpdateAjax(Request $request) : Response
    {
        $data = $_REQUEST;
        $item = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:PurchaseItem')->find($data['purchaseItem']);
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $this->getDoctrine()->getRepository("TerminalbdInventoryBundle:PurchaseItem")->updateItem($item,$data);
        $invoice = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Purchase')->updatePurchaseTotalPrice($item->getPurchase());
        $result = $this->returnResultData($invoice);
        return new Response(json_encode($result));

    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{invoice}/{item}/item-delete", methods={"GET"}, name="inventory_purchase_service_item_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function deleteItem(Purchase $invoice,$item): Response
    {
        $post = $this->getDoctrine()->getRepository(PurchaseItem::class)->find($item);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $purchase = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Purchase')->updatePurchaseTotalPrice($invoice);
        $result = $this->returnResultData($purchase);
        return new Response(json_encode($result));
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{meta}/delete-invoice-meta", methods={"GET"}, name="inventory_purchase_service_delete_invoice_meta")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function deleteInvoiceMeta(InvoiceKeyValue $meta): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($meta);
        $em->flush();
        return new Response('success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="inventory_purchase_service_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(Purchase::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/{mode}/preview", methods={"GET"}, name="inventory_purchase_service_preview")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function print($id,$mode): Response
    {
        $entity = $this->getDoctrine()->getRepository(Purchase::class)->find($id);
        $domain =$this->getUser()->getTerminal();


        if($mode == "pdf"){

          /*  $mpdf = new Mpdf(['tempDir' => 'public/uploads/temp']);
            $html = $this->renderView(
                'print/preview.html.twig',[
                'post' => $post,
                'mode' => 'pdf',
            ]);*/
            $mpdf->WriteHTML($html);
            $mpdf->Output();

        }elseif($mode == "mushok"){

            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Nicola Asuni');
            $pdf->SetTitle('TCPDF Example 001');
            $pdf->SetSubject('TCPDF Tutorial');
            $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
            $pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
            $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            // set font
        //    $pdf->SetFont('', '', 10);
            $fontname = \TCPDF_FONTS::addTTFfont('http://uipoka.local/Fonts/SolaimanLipi.ttf', 'TrueTypeUnicode', '', 96);
            $pdf->SetFont($fontname, '', 10, '', false);
// add a page
            $pdf->AddPage();
            $html = $this->renderView('@TerminalbdInventory/mushok/test.twig', $data = array());
            $pdf->writeHTML($html, true, false, true, false, '');

            $pdf->Output('example_001.pdf', 'I');
            exit;

        }elseif($mode == "print"){

            return $this->render('@TerminalbdInventory/mushok/mushok_6_3.html.twig', [
                'domain' => $domain,
                'entity' => $entity,
                'mode' => 'print',
            ]);

        }else{

            return $this->render('@TerminalbdInventory/purchase/local/show.html.twig', [
                'domain' => $domain,
                'entity' => $entity,
                'products' => $products,
                'form' => $form->createView(),
            ]);
        }

    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/mushok6", methods={"GET"}, name="inventory_purchase_service_mushok6")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */
    public function mushok6($id): Response
    {
        $post = $this->getDoctrine()->getRepository(Purchase::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="inventory_purchase_service_data_table")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_PURCHASE')")
     */

    public function dataTable(Request $request)
    {

        $query = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $iTotalRecords = $this->getDoctrine()->getRepository(Purchase::class)->count(array('config'=> $config));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository('TerminalbdInventoryBundle:Purchase')->findWithSearch($config,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post Item */

        foreach ($result as $post):

            $editUrl = $this->generateUrl('inventory_purchase_service_edit',array('id' => $post['id']));
            $deleteUrl = $this->generateUrl('inventory_purchase_service_delete',array('id'=> $post['id']));
            $viewUrl = $this->generateUrl('inventory_purchase_service_show',array('id'=> $post['id']));
            $created = $post['created']->format('d-m-Y');
            if($post['discountMode'] == "percent"){
                $calculation = $post['calculation']."%";
            }else{
                $calculation = $post['calculation'];
            }
            $records["data"][] = array(
                $id                 = $i,
                $created            = $created,
                $invoice            = $post['invoice'],
                $companyName        = $post['companyName'],
                $method             = $post['method'],
                $subTotal           = $post['subTotal'],
                $discount           = $post['discount'],
                $calculation        = $calculation,
                $taxTotal           = $post['taxTotal'],
                $netTotal           = $post['netTotal'],
                $payment           = $post['payment'],
                $due                = $post['due'],
                $mode               = ucfirst($post['mode']),
                $process            = ucfirst($post['process']),
                $action             ="<div class='btn-group card-option'><button type='button' class='btn btn-notify' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fas fa-ellipsis-h'></i></button>
<ul class='list-unstyled card-option dropdown-info dropdown-menu dropdown-menu-right'>
 <li class='dropdown-item'> <a href='{$viewUrl}' >View</a></li>
<li class='dropdown-item'> <a href='{$editUrl}'>Edit</a></li>
<li class='dropdown-item'> <a  data-action='{$deleteUrl}' class='remove' data-id='{$post['id']}' href='javascript:'>Remove</a></li>
</ul></div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }

}
