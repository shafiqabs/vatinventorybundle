<?php

namespace Terminalbd\InventoryBundle\Controller;
use App\Entity\Application\Inventory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\ItemKeyValue;
use Terminalbd\InventoryBundle\Entity\Setting;
use Terminalbd\InventoryBundle\Form\SettingFormType;


/**
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY')")
 * @Route("/inventory/setting")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class SettingController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="inventory_setting")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_SETTING')")
     */
    public function index(Request $request): Response
    {
        $entities = $this->getDoctrine()->getRepository(Setting::class)->findAll();
        return $this->render('@TerminalbdInventory/setting/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_SETTING')")
     * @Route("/new", methods={"GET", "POST"}, name="inventory_setting_new")
     */
    public function new(Request $request): Response
    {

        $entity = new Setting();
        $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
        $data = $request->request->all();
        $form = $this->createForm(SettingFormType::class , $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($entity);
            $em->persist($entity);
            $em->flush();
            $this->getDoctrine()->getRepository(ItemKeyValue::class)->insertSettingKeyValue($entity,$data);
            $this->addFlash('success', 'post.created_successfully');
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('inventory_setting_new');
            }
            return $this->redirectToRoute('inventory_setting');
        }
        return $this->render('@TerminalbdInventory/setting/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="inventory_setting_edit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_inventory_setting')")
     */
    public function edit(Request $request, Setting $entity): Response
    {
        $data = $request->request->all();
        $form = $this->createForm(SettingFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $config = $this->getDoctrine()->getRepository(Inventory::class)->findConfig($this->getUser());
            $entity->setConfig($config);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'post.updated_successfully');
            $this->getDoctrine()->getRepository(ItemKeyValue::class)->insertSettingKeyValue($entity,$data);
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('inventory_setting_edit', ['id' => $entity->getId()]);
            }

            return $this->redirectToRoute('inventory_setting');
        }
        return $this->render('@TerminalbdInventory/setting/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="inventory_setting_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_inventory_setting')")
     */
    public function delete($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(Setting::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/meta-delete", methods={"GET"}, name="inventory_setting_meta_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_inventory_setting')")
     */
    public function metaDelete($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(ItemKeyValue::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

}
