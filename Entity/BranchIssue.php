<?php

namespace Terminalbd\InventoryBundle\Entity;

use App\Entity\Admin\Bank;
use App\Entity\Application\Inventory;
use App\Entity\Core\Customer;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Terminalbd\AccountingBundle\Entity\AccountBank;


/**
 * Sales
 *
 * @ORM\Table(name="inv_branch_issue")
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\BranchIssueRepository")
 * @Gedmo\Uploadable(filenameGenerator="SHA1", allowOverwrite=true, appendNumber=true)
 */
    class BranchIssue
    {
        /**
         * @ORM\Id
         * @ORM\Column(name="id", type="guid")
         * @ORM\GeneratedValue(strategy="UUID")
         */
        protected $id;

        /**
         * @ORM\ManyToOne(targetEntity="App\Entity\Application\Inventory")
         * @ORM\JoinColumn(onDelete="CASCADE")
         **/
        private $config;

        /**
         * @var $accountBank AccountBank
         * @ORM\ManyToOne(targetEntity="Terminalbd\AccountingBundle\Entity\AccountBank")
         **/
        private $accountBank;

        /**
         * @var $accountMobile AccountBank
         * @ORM\ManyToOne(targetEntity="Terminalbd\AccountingBundle\Entity\AccountBank")
         **/
        private $accountMobile;


        /**
         * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\BranchIssueItem", mappedBy="barnchIssue"  )
         **/
        private  $branchIssueItems;


        /**
         * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\InvoiceKeyValue", mappedBy="branchIssue")
         **/
        private $invoiceKeyValues;

        /**
         *
         * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting", inversedBy="branchIssues")
         **/
        private  $branch;


        /**
         * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Country")
         **/
        private  $country;

        /**
         * @var string
         *
         * @ORM\Column(type="string", nullable=true)
         */
        private $billingAddress;

        /**
         * @var string
         * @ORM\Column(type="string", nullable=true)
         */
        private $shippingAddress;


        /**
         * @var string
         * @ORM\Column(name="lcNo", type="string", length = 50, nullable=true)
         */
        private $lcNo;

        /**
         * @var string
         * @ORM\Column(type="string", length = 50, nullable=true)
         */
        private $transportInfo;


        /**
         * @var string
         * @ORM\Column(type="string", length = 50, nullable=true)
         */
        private $billOfEntryNo;

        /**
         * @var \DateTime
         * @ORM\Column(type="datetime", nullable=true)
         */
        private $billOfEntryDate;


        /**
         * @var string
         * @ORM\Column(type="string", nullable=true)
         */
        private $vehicleInfo;

        /**
         * @var string
         * @ORM\Column(type="string", nullable=true)
         */
        private $clearForwardingFirm;


        /**
         * @var \DateTime
         * @ORM\Column(type="datetime", nullable=true)
         */
        private $lcDate;


        /**
         * @var $bank Bank
         * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Bank")
         **/
        private  $bank;

        /**
         * @ORM\ManyToOne(targetEntity="App\Entity\User")
         **/
        private $salesBy;


        /**
         * @ORM\ManyToOne(targetEntity="App\Entity\User")
         **/
        private  $createdBy;


        /**
         * @ORM\ManyToOne(targetEntity="App\Entity\User")
         **/
        private  $checkedBy;


        /**
         * @ORM\ManyToOne(targetEntity="App\Entity\User")
         **/
        private  $approvedBy;


        /**
         * @ORM\ManyToOne(targetEntity="App\Entity\Core\Customer")
         **/
        private  $customer;


        /**
         * @var float
         *
         * @ORM\Column(name="subTotal", type="float", nullable=true)
         */
        private $subTotal = 0;

        /**
         * @var float
         *
         * @ORM\Column(name="netTotal", type="float", nullable=true)
         */
        private $netTotal = 0;

        /**
         * @var float
         *
         * @ORM\Column(name="total", type="float", nullable=true)
         */
        private $total = 0;

        /**
         * @var float
         *
         * @ORM\Column(name="due", type="float", nullable=true)
         */
        private $due = 0;


        /**
         * @var float
         *
         * @ORM\Column(name="customsDuty", type="float", nullable=true)
         */
        private $customsDuty = 0.00;


        /**
         * @var float
         *
         * @ORM\Column(name="supplementaryDuty", type="float", nullable=true)
         */
        private $supplementaryDuty = 0.00;

        /**
         * @var float
         *
         * @ORM\Column(name="valueAddedTax", type="float", nullable=true)
         */
        private $valueAddedTax = 0.00;


        /**
         * @var float
         *
         * @ORM\Column(name="advanceIncomeTax", type="float", nullable=true)
         */
        private $advanceIncomeTax = 0.00;


        /**
         * @var float
         *
         * @ORM\Column(name="regulatoryDuty", type="float", nullable=true)
         */
        private $regulatoryDuty = 0.00;


        /**
         * @var float
         *
         * @ORM\Column(name="advanceTradeVat", type="float", nullable=true)
         */
        private $advanceTradeVat = 0.00;


         /**
         * @var float
         *
         * @ORM\Column(name="advanceTax", type="float", nullable=true)
         */
        private $advanceTax = 0.00;


         /**
         * @var float
         *
         * @ORM\Column(name="taxTariffCalculation", type="float", nullable=true)
         */
        private $taxTariffCalculation = 0.00;


        /**
         * @var float
         *
         * @ORM\Column(name="totalTaxIncidence", type="float", nullable=true)
         */
        private $totalTaxIncidence = 0.00;


        /**
         * @var float
         *
         * @ORM\Column(name="amount", type="float", nullable=true)
         */
        private $amount = 0;


        /**
         * @var float
         *
         * @ORM\Column(name="rebate", type="float", nullable=true)
         */
        private $rebate = 0;

         /**
         * @var float
         *
         * @ORM\Column(name="vatDeductionSource", type="float", nullable=true)
         */
        private $vatDeductionSource = 0;

         /**
         * @var float
         *
         * @ORM\Column(name="discount", type="float", nullable=true)
         */
        private $discount = 0;

         /**
         * @var float
         *
         * @ORM\Column(type="float", nullable=true)
         */
        private $discountCalculation = 0;


        /**
         * @var string
         *
         * @ORM\Column(type="string", length = 50, nullable=true)
         */
        private $discountMode;

        /**
         * @var string
         *
         * @ORM\Column(type="string", length = 50, nullable=true)
         */
        private $transactionMethod;

        /**
         * @var string
         *
         * @ORM\Column(name="paymentCard", type="string", length = 50, nullable=true)
         */
        private $paymentCard;

        /**
         * @var string
         *
         * @ORM\Column(name="cardNo", type="string", length=100, nullable=true)
         */
        private $cardNo;

        /**
         * @var string
         *
         * @ORM\Column(name="paymentMobile", type="string", length=50, nullable=true)
         */
        private $paymentMobile;

        /**
         * @var string
         *
         * @ORM\Column(name="paymentInWord", type="string", length=255, nullable=true)
         */
        private $paymentInWord;

        /**
         * @var string
         *
         * @ORM\Column(name="transactionId", type="string", length=100, nullable=true)
         */
        private $transactionId;

        /**
         * @var boolean
         *
         * @ORM\Column(name="revised", type="boolean" )
         */
        private $revised = false;

        /**
         * @var boolean
         *
         * @ORM\Column(type="boolean" )
         */
        private $deemedExport = false;


        /**
         * @var string
         *
         * @ORM\Column(type="string", length = 50, nullable=true)
         */
        private $invoice;

        /**
         * @var string
         *
         * @ORM\Column(name="challanNo", type="string", length = 50, nullable=true)
         */
        private $challanNo;

        /**
         * @var string
         *
         * @ORM\Column(name="mode", type="string", length = 50, nullable=true)
         */
        private $mode = "local";

        /**
         * @var string
         *
         * @ORM\Column(name="issuePerson", type="string", length = 100, nullable=true)
         */
        private $issuePerson;

        /**
         * @var string
         *
         * @ORM\Column(name="issueDesignation", type="string", length = 50, nullable=true)
         */
        private $issueDesignation;



        /**
         * @var integer
         *
         * @ORM\Column(name="code", type="integer",  nullable=true)
         */
        private $code;

        /**
         * @var string
         *
         * @ORM\Column(type="text", nullable = true)
         */
        private $narration;

        /**
         * @var string
         *
         * @ORM\Column(type="text", nullable = true)
         */
        private $deliveryAddress;

        /**
         * @var string
         *
         * @ORM\Column(type="text", length=50, nullable = true)
         */
        private $issueTime;


        /**
         * @var \DateTime
         * @Gedmo\Timestampable(on="create")
         * @ORM\Column(name="issueDate", type="datetime")
         */
        private $issueDate;
        

        /**
         * @var \DateTime
         * @Gedmo\Timestampable(on="create")
         * @ORM\Column(name="created", type="datetime")
         */
        private $created;

        /**
         * @var \DateTime
         * @ORM\Column(name="updated", type="datetime", nullable = true)
         */
        private $updated;


        /**
         * @var string
         *
         * @ORM\Column(name="process", type="string", length=50, nullable = true)
         */
        private $process;


        /**
         * @var string
         *
         * @ORM\Column(name="processType", type="string", length=50, nullable = true)
         */
        private $processType;

        /**
         * @var float
         *
         * @ORM\Column(type="float", nullable=true)
         */
        private $quantity = 0;

        /**
         * @var float
         *
         * @ORM\Column(type="float", nullable=true)
         */
        private $totalItem = 0;


        /**
         * @ORM\Column(name="path", type="string", nullable=true)
         * @Gedmo\UploadableFilePath
         */
        protected $path;

        /**
         * @Assert\File(maxSize="8388608")
         */
        protected $file;

        

        /**
         * Get id
         *
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }



        /**
         * @return \DateTime
         */
        public function getCreated()
        {
            return $this->created;
        }

        /**
         * @param \DateTime $created
         */
        public function setCreated($created)
        {
            $this->created = $created;
        }

        /**
         * @return \DateTime
         */
        public function getUpdated()
        {
            return $this->updated;
        }

        /**
         * @param \DateTime $updated
         */
        public function setUpdated($updated)
        {
            $this->updated = $updated;
        }
        

        /**
         * @return User
         */
        public function getCreatedBy()
        {
            return $this->createdBy;
        }

        /**
         * @param User $createdBy
         */
        public function setCreatedBy($createdBy)
        {
            $this->createdBy = $createdBy;
        }


        /**
         * @return User
         */
        public function getCheckedBy()
        {
            return $this->checkedBy;
        }

        /**
         * @param User $checkedBy
         */
        public function setCheckedBy($checkedBy)
        {
            $this->checkedBy = $checkedBy;
        }


        /**
         * @return User
         */
        public function getApprovedBy()
        {
            return $this->approvedBy;
        }

        /**
         * @param User $approvedBy
         */
        public function setApprovedBy($approvedBy)
        {
            $this->approvedBy = $approvedBy;
        }

        /**
         * @return string
         */
        public function getProcess()
        {
            return $this->process;
        }

        /**
         * @param string $process
         */
        public function setProcess($process)
        {
            $this->process = $process;
        }


        /**
         * Local
         * Foreign
         * Service
         * @return string
         */
        public function getProcessType()
        {
            return $this->processType;
        }

        /**
         * @param string $processType
         */
        public function setProcessType($processType)
        {
            $this->processType = $processType;
        }



        /**
         * @return int
         */
        public function getCode()
        {
            return $this->code;
        }

        /**
         * @param int $code
         */
        public function setCode($code)
        {
            $this->code = $code;
        }


        /**
         * @return string
         */
        public function getTransactionMethod()
        {
            return $this->transactionMethod;
        }

        /**
         * @param string $transactionMethod
         */
        public function setTransactionMethod($transactionMethod)
        {
            $this->transactionMethod = $transactionMethod;
        }

        /**
         * @return AccountBank
         */
        public function getAccountBank()
        {
            return $this->accountBank;
        }

        /**
         * @param AccountBank $accountBank
         */
        public function setAccountBank($accountBank)
        {
            $this->accountBank = $accountBank;
        }

        /**
         * @return AccountBank
         */
        public function getAccountMobile()
        {
            return $this->accountMobile;
        }

        /**
         * @param AccountBank $accountMobile
         */
        public function setAccountMobile($accountMobile)
        {
            $this->accountMobile = $accountMobile;
        }


        /**
         * @return salesItem
         */
        public function getsalesItems()
        {
            return $this->salesItems;
        }

        /**
         * @return float
         */
        public function getTotalTaxIncidence()
        {
            return $this->totalTaxIncidence;
        }

        /**
         * @param float $totalTaxIncidence
         */
        public function setTotalTaxIncidence($totalTaxIncidence)
        {
            $this->totalTaxIncidence = $totalTaxIncidence;
        }

        /**
         * @return float
         */
        public function getAdvanceTradeVat()
        {
            return $this->advanceTradeVat;
        }

        /**
         * @param float $advanceTradeVat
         */
        public function setAdvanceTradeVat($advanceTradeVat)
        {
            $this->advanceTradeVat = $advanceTradeVat;
        }

        /**
         * @return float
         */
        public function getRegulatoryDuty()
        {
            return $this->regulatoryDuty;
        }

        /**
         * @param float $regulatoryDuty
         */
        public function setRegulatoryDuty(float $regulatoryDuty)
        {
            $this->regulatoryDuty = $regulatoryDuty;
        }



        /**
         * @return float
         */
        public function getAdvanceIncomeTax()
        {
            return $this->advanceIncomeTax;
        }

        /**
         * @param float $advanceIncomeTax
         */
        public function setAdvanceIncomeTax($advanceIncomeTax)
        {
            $this->advanceIncomeTax = $advanceIncomeTax;
        }

        /**
         * @return float
         */
        public function getValueAddedTax()
        {
            return $this->valueAddedTax;
        }

        /**
         * @param float $valueAddedTax
         */
        public function setValueAddedTax($valueAddedTax)
        {
            $this->valueAddedTax = $valueAddedTax;
        }

        /**
         * @return float
         */
        public function getSupplementaryDuty()
        {
            return $this->supplementaryDuty;
        }

        /**
         * @param float $supplementaryDuty
         */
        public function setSupplementaryDuty($supplementaryDuty)
        {
            $this->supplementaryDuty = $supplementaryDuty;
        }

        /**
         * @return float
         */
        public function getCustomsDuty()
        {
            return $this->customsDuty;
        }

        /**
         * @param float $customsDuty
         */
        public function setCustomsDuty($customsDuty)
        {
            $this->customsDuty = $customsDuty;
        }

        /**
         * @return float
         */
        public function getAdvanceTax(): float
        {
            return $this->advanceTax;
        }

        /**
         * @param float $advanceTax
         */
        public function setAdvanceTax(float $advanceTax)
        {
            $this->advanceTax = $advanceTax;
        }



        /**
         * @return float
         */
        public function getTaxTariffCalculation()
        {
            return $this->taxTariffCalculation;
        }

        /**
         * @param float $taxTariffCalculation
         */
        public function setTaxTariffCalculation(float $taxTariffCalculation)
        {
            $this->taxTariffCalculation = $taxTariffCalculation;
        }



        /**
         * @return float
         */
        public function getNetTotal()
        {
            return $this->netTotal;
        }

        /**
         * @param float $netTotal
         */
        public function setNetTotal($netTotal)
        {
            $this->netTotal = $netTotal;
        }

        /**
         * @return float
         */
        public function getTotal(): float
        {
            return $this->total;
        }

        /**
         * @param float $total
         */
        public function setTotal(float $total)
        {
            $this->total = $total;
        }


        /**
         * @return float
         */
        public function getSubTotal()
        {
            return $this->subTotal;
        }

        /**
         * @param float $subTotal
         */
        public function setSubTotal($subTotal)
        {
            $this->subTotal = $subTotal;
        }

        /**
         * @return float
         */
        public function getRebate()
        {
            return $this->rebate;
        }

        /**
         * @param float $rebate
         */
        public function setRebate($rebate)
        {
            $this->rebate = $rebate;
        }

        /**
         * @return float
         */
        public function getVatDeductionSource()
        {
            return $this->vatDeductionSource;
        }

        /**
         * @param float $vatDeductionSource
         */
        public function setVatDeductionSource($vatDeductionSource)
        {
            $this->vatDeductionSource = $vatDeductionSource;
        }

        /**
         * @return float
         */
        public function getDiscount()
        {
            return $this->discount;
        }

        /**
         * @param float $discount
         */
        public function setDiscount($discount)
        {
            $this->discount = $discount;
        }

        /**
         * @return string
         */
        public function getGrn()
        {
            return $this->grn;
        }

        /**
         * @param string $grn
         */
        public function setGrn($grn)
        {
            $this->grn = $grn;
        }

        /**
         * @return string
         */
        public function getChallanNo()
        {
            return $this->challanNo;
        }

        /**
         * @param string $challanNo
         */
        public function setChallanNo($challanNo)
        {
            $this->challanNo = $challanNo;
        }

        /**
         * @return string
         */
        public function getLcNo()
        {
            return $this->lcNo;
        }

        /**
         * @param string $lcNo
         */
        public function setLcNo($lcNo)
        {
            $this->lcNo = $lcNo;
        }

        /**
         * @return mixed
         */
        public function getPoDate()
        {
            return $this->poDate;
        }

        /**
         * @param mixed $poDate
         */
        public function setPoDate($poDate)
        {
            $this->poDate = $poDate;
        }

        /**
         * @return Accountsales
         */
        public function getAccountsales()
        {
            return $this->accountsales;
        }

        /**
         * @return salesOrder
         */
        public function getsalesOrder()
        {
            return $this->salesOrder;
        }

        /**
         * @param salesOrder $salesOrder
         */
        public function setsalesOrder($salesOrder)
        {
            $this->salesOrder = $salesOrder;
        }

        /**
         * @return Inventory
         */
        public function getConfig()
        {
            return $this->config;
        }

        /**
         * @param Inventory  $config
         */
        public function setConfig($config)
        {
            $this->config = $config;
        }


        /**
         * @return StockItem
         */
        public function getStockItems()
        {
            return $this->stockItems;
        }

        /**
         * @return string
         */
        public function getMode(): string
        {
            return $this->mode;
        }

        /**
         * @param string $mode
         */
        public function setMode(string $mode)
        {
            $this->mode = $mode;
        }

        /**
         * @return float
         */
        public function getDue()
        {
            return $this->due;
        }

        /**
         * @param float $due
         */
        public function setDue(float $due)
        {
            $this->due = $due;
        }

        /**
         * @return string
         */
        public function getDiscountMode()
        {
            return $this->discountMode;
        }

        /**
         * @param string $discountMode
         */
        public function setDiscountMode($discountMode)
        {
            $this->discountMode = $discountMode;
        }

        /**
         * @return string
         */
        public function getIssueTime(): ? string
        {
            return $this->issueTime;
        }

        /**
         * @param string $issueTime
         */
        public function setIssueTime(string $issueTime)
        {
            $this->issueTime = $issueTime;
        }



        /**
         * Sets file.
         *
         * @param User $file
         */
        public function setFile(UploadedFile $file = null)
        {
            $this->file = $file;
        }

        /**
         * Get file.
         *
         * @return User
         */
        public function getFile()
        {
            return $this->file;
        }

        public function getAbsolutePath()
        {
            return null === $this->path
                ? null
                : $this->getUploadRootDir(). $this->path;
        }

        public function getWebPath()
        {
            return null === $this->path
                ? null
                : $this->getUploadDir().'/'.$this->path;
        }

        /**
         * @ORM\PostRemove()
         */
        public function removeUpload()
        {
            if ($file = $this->getAbsolutePath()) {
                unlink($file);
            }
        }

        protected function getUploadRootDir()
        {
            return WEB_ROOT .'/uploads/'.$this->getUploadDir();
        }

        protected function getUploadDir()
        {
            return 'user/';
        }

        public function upload()
        {
            // the file property can be empty if the field is not required
            if (null === $this->getFile()) {
                return;
            }

            $filename = date('YmdHmi') . "_" . $this->getFile()->getClientOriginalName();

            $this->getFile()->move(
                $this->getUploadRootDir(),
                $filename
            );
            // set the path property to the filename where you've saved the file
            $this->path = $filename;

            // clean up the file property as you won't need it anymore
            $this->file = null;
        }

        /**
         * @return string
         */
        public function getInvoice(): ? string
        {
            return $this->invoice;
        }

        /**
         * @param string $invoice
         */
        public function setInvoice(string $invoice)
        {
            $this->invoice = $invoice;
        }

        /**
         * @return string
         */
        public function getNarration(): ? string
        {
            return $this->narration;
        }

        /**
         * @param string $narration
         */
        public function setNarration(string $narration)
        {
            $this->narration = $narration;
        }
        

        /**
         * @return float
         */
        public function getDiscountCalculation()
        {
            return $this->discountCalculation;
        }

        /**
         * @param float $discountCalculation
         */
        public function setDiscountCalculation(float $discountCalculation)
        {
            $this->discountCalculation = $discountCalculation;
        }



        /**
         * @return Bank
         */
        public function getBank()
        {
            return $this->bank;
        }

        /**
         * @param Bank $bank
         */
        public function setBank($bank)
        {
            $this->bank = $bank;
        }

        /**
         * @return User
         */
        public function getSalesBy()
        {
            return $this->salesBy;
        }

        /**
         * @param User $salesBy
         */
        public function setSalesBy($salesBy)
        {
            $this->salesBy = $salesBy;
        }

        /**
         * @return float
         */
        public function getAmount()
        {
            return $this->amount;
        }

        /**
         * @param float $amount
         */
        public function setAmount($amount)
        {
            $this->amount = $amount;
        }

        /**
         * @return string
         */
        public function getPaymentCard(): ? string
        {
            return $this->paymentCard;
        }

        /**
         * @param string $paymentCard
         */
        public function setPaymentCard(string $paymentCard)
        {
            $this->paymentCard = $paymentCard;
        }

        /**
         * @return string
         */
        public function getCardNo(): ? string
        {
            return $this->cardNo;
        }

        /**
         * @param string $cardNo
         */
        public function setCardNo(string $cardNo)
        {
            $this->cardNo = $cardNo;
        }

        /**
         * @return string
         */
        public function getPaymentMobile(): ? string
        {
            return $this->paymentMobile;
        }

        /**
         * @param string $paymentMobile
         */
        public function setPaymentMobile(string $paymentMobile)
        {
            $this->paymentMobile = $paymentMobile;
        }

        /**
         * @return string
         */
        public function getPaymentInWord(): ? string
        {
            return $this->paymentInWord;
        }

        /**
         * @param string $paymentInWord
         */
        public function setPaymentInWord(string $paymentInWord)
        {
            $this->paymentInWord = $paymentInWord;
        }

        /**
         * @return string
         */
        public function getTransactionId(): ? string
        {
            return $this->transactionId;
        }

        /**
         * @param string $transactionId
         */
        public function setTransactionId(string $transactionId)
        {
            $this->transactionId = $transactionId;
        }

        /**
         * @return bool
         */
        public function isRevised(): bool
        {
            return $this->revised;
        }

        /**
         * @param bool $revised
         */
        public function setRevised(bool $revised)
        {
            $this->revised = $revised;
        }

        /**
         * @return string
         */
        public function getDeliveryAddress(): ? string
        {
            return $this->deliveryAddress;
        }

        /**
         * @param string $deliveryAddress
         */
        public function setDeliveryAddress(string $deliveryAddress)
        {
            $this->deliveryAddress = $deliveryAddress;
        }

        /**
         * @return \DateTime
         */
        public function getIssueDate()
        {
            return $this->issueDate;
        }

        /**
         * @param \DateTime $issueDate
         */
        public function setIssueDate($issueDate)
        {
            $this->issueDate = $issueDate;
        }

        /**
         * @return InvoiceKeyValue
         */
        public function getInvoiceKeyValues()
        {
            return $this->invoiceKeyValues;
        }

        /**
         * @return Customer
         */
        public function getCustomer()
        {
            return $this->customer;
        }

        /**
         * @param Customer $customer
         */
        public function setCustomer($customer)
        {
            $this->customer = $customer;
        }

        /**
         * @return string
         */
        public function getIssueDesignation(): ? string
        {
            return $this->issueDesignation;
        }

        /**
         * @param string $issueDesignation
         */
        public function setIssueDesignation(string $issueDesignation)
        {
            $this->issueDesignation = $issueDesignation;
        }

        /**
         * @return string
         */
        public function getIssuePerson(): ? string
        {
            return $this->issuePerson;
        }

        /**
         * @param string $issuePerson
         */
        public function setIssuePerson(string $issuePerson)
        {
            $this->issuePerson = $issuePerson;
        }

        /**
         * @return Setting
         */
        public function getCustomHouse()
        {
            return $this->customHouse;
        }

        /**
         * @param Setting $customHouse
         */
        public function setCustomHouse($customHouse)
        {
            $this->customHouse = $customHouse;
        }

        /**
         * @return mixed
         */
        public function getCountry()
        {
            return $this->country;
        }

        /**
         * @param mixed $country
         */
        public function setCountry($country)
        {
            $this->country = $country;
        }

        /**
         * @return mixed
         */
        public function getBillingAddress()
        {
            return $this->billingAddress;
        }

        /**
         * @param mixed $billingAddress
         */
        public function setBillingAddress($billingAddress)
        {
            $this->billingAddress = $billingAddress;
        }

        /**
         * @return mixed
         */
        public function getShippingAddress()
        {
            return $this->shippingAddress;
        }

        /**
         * @param mixed $shippingAddress
         */
        public function setShippingAddress($shippingAddress)
        {
            $this->shippingAddress = $shippingAddress;
        }

        /**
         * @return string
         */
        public function getVehicleInfo(): ? string
        {
            return $this->vehicleInfo;
        }

        /**
         * @param string $vehicleInfo
         */
        public function setVehicleInfo(string $vehicleInfo)
        {
            $this->vehicleInfo = $vehicleInfo;
        }

        /**
         * @return string
         */
        public function getClearForwardingFirm(): ? string
        {
            return $this->clearForwardingFirm;
        }

        /**
         * @param string $clearForwardingFirm
         */
        public function setClearForwardingFirm(string $clearForwardingFirm)
        {
            $this->clearForwardingFirm = $clearForwardingFirm;
        }

        /**
         * @return \DateTime
         */
        public function getLcDate()
        {
            return $this->lcDate;
        }

        /**
         * @param \DateTime $lcDate
         */
        public function setLcDate($lcDate)
        {
            $this->lcDate = $lcDate;
        }

        /**
         * @return mixed
         */
        public function getBillOfEntryNo()
        {
            return $this->billOfEntryNo;
        }

        /**
         * @param mixed $billOfEntryNo
         */
        public function setBillOfEntryNo($billOfEntryNo)
        {
            $this->billOfEntryNo = $billOfEntryNo;
        }

        /**
         * @return \DateTime
         */
        public function getBillOfEntryDate()
        {
            return $this->billOfEntryDate;
        }

        /**
         * @param \DateTime $billOfEntryDate
         */
        public function setBillOfEntryDate($billOfEntryDate)
        {
            $this->billOfEntryDate = $billOfEntryDate;
        }

        /**
         * @return WorkOrder
         */
        public function getWorkOrder()
        {
            return $this->workOrder;
        }

        /**
         * @param WorkOrder $workOrder
         */
        public function setWorkOrder($workOrder)
        {
            $this->workOrder = $workOrder;
        }

        /**
         * @return Setting
         */
        public function getCustomsHouse()
        {
            return $this->customsHouse;
        }

        /**
         * @param Setting $customsHouse
         */
        public function setCustomsHouse($customsHouse)
        {
            $this->customsHouse = $customsHouse;
        }

        /**
         * @return string
         */
        public function getTransportInfo(): ? string
        {
            return $this->transportInfo;
        }

        /**
         * @param string $transportInfo
         */
        public function setTransportInfo(string $transportInfo)
        {
            $this->transportInfo = $transportInfo;
        }

        /**
         * @return bool
         */
        public function isDeemedExport()
        {
            return $this->deemedExport;
        }

        /**
         * @param bool $deemedExport
         */
        public function setDeemedExport($deemedExport)
        {
            $this->deemedExport = $deemedExport;
        }

        /**
         * @return mixed
         */
        public function getQuantity()
        {
            return $this->quantity;
        }

        /**
         * @param mixed $quantity
         */
        public function setQuantity($quantity)
        {
            $this->quantity = $quantity;
        }

        /**
         * @return mixed
         */
        public function getTotalItem()
        {
            return $this->totalItem;
        }

        /**
         * @param mixed $totalItem
         */
        public function setTotalItem($totalItem)
        {
            $this->totalItem = $totalItem;
        }

        /**
         * @return \App\Entity\Core\Setting
         */
        public function getBranch()
        {
            return $this->branch;
        }

        /**
         * @return \App\Entity\Core\Setting $branch
         */
        public function setBranch($branch)
        {
            $this->branch = $branch;
        }

        /**
         * @return BranchIssueItem
         */
        public function getBranchIssueItems()
        {
            return $this->branchIssueItems;
        }

        /**
         * @param BranchIssueItem $branchIssueItems
         */
        public function setBranchIssueItems($branchIssueItems)
        {
            $this->branchIssueItems = $branchIssueItems;
        }



    }

