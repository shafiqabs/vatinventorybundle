<?php

namespace Terminalbd\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * InvoiceKeyValue
 *
 * @ORM\Table(name="inv_key_value")
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\InvoiceKeyValueRepository")
 */
class InvoiceKeyValue
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Purchase", inversedBy="invoiceKeyValues" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $purchase;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Sales", inversedBy="invoiceKeyValues" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $sales;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\BranchIssue", inversedBy="invoiceKeyValues" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $branchIssue;


    /**
     * @var string
     *
     * @ORM\Column(name="metaKey", type="string", length=255, nullable = true)
     */
    private $metaKey;

    /**
     * @var string
     *
     * @ORM\Column(name="metaValue", type="string", length=255 , nullable = true)
     */
    private $metaValue;

    /**
     * @var Integer
     *
     * @ORM\Column(name="sorting", type="smallint", length=2, nullable = true)
     */
    private $sorting;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getMetaKey()
    {
        return $this->metaKey;
    }

    /**
     * @param string $metaKey
     */
    public function setMetaKey($metaKey)
    {
        $this->metaKey = $metaKey;
    }

    /**
     * @return string
     */
    public function getMetaValue()
    {
        return $this->metaValue;
    }

    /**
     * @param string $metaValue
     */
    public function setMetaValue($metaValue)
    {
        $this->metaValue = $metaValue;
    }


    /**
     * @return int
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param int $sorting
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
    }


    /**
     * @return Purchase
     */
    public function getPurchase()
    {
        return $this->purchase;
    }

    /**
     * @param Purchase $purchase
     */
    public function setPurchase($purchase)
    {
        $this->purchase = $purchase;
    }

    /**
     * @return Sales
     */
    public function getSales()
    {
        return $this->sales;
    }

    /**
     * @param Sales $sales
     */
    public function setSales($sales)
    {
        $this->sales = $sales;
    }

    /**
     * @return BranchIssue
     */
    public function getBranchIssue()
    {
        return $this->branchIssue;
    }

    /**
     * @param BranchIssue $branchIssue
     */
    public function setBranchIssue($branchIssue)
    {
        $this->branchIssue = $branchIssue;
    }


}

