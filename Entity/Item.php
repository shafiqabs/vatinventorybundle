<?php

namespace Terminalbd\InventoryBundle\Entity;

use App\Entity\Admin\ProductUnit;
use App\Entity\Application\Inventory;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Terminalbd\ProductionBundle\Entity\ProductionItem;

/**
 * Product
 *
 * @ORM\Table("inv_item")
 * @UniqueEntity(fields="name",message="This product name already existing,Please try another.")
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\ItemRepository")
 */
class Item
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Application\Inventory")
     * @ORM\JoinColumn(onDelete="CASCADE")
	 **/
	private  $config;


    /**
	 * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\Setting")
	 **/
	private  $inputTax;


    /**
	 * @ORM\OneToOne(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionItem", mappedBy="item" )
	 **/
	private  $productionItem;


    /**
	 * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\Damage", mappedBy="item" )
	 **/
	private  $damages;


     /**
	 * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\ProductionIssue", mappedBy="item" )
	 **/
	private  $productionItems;

    /**
	 * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\PurchaseReturnItem", mappedBy="item" )
	 **/
	private  $purchaseReturnItems;


    /**
	 * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Setting", inversedBy="items" )
	 **/
	private  $productGroup;

    /**
	 * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\MasterItem", inversedBy="items" )
     * @Assert\NotBlank(message="Master product must be required")
     **/
	private  $masterItem;

    /**
	 * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Category", inversedBy="items" )
	 **/
	private  $category;


	/**
	 * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Setting", inversedBy="brandItems" )
	 **/
	private  $brand;

    /**
	 * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Setting", inversedBy="typeItems" )
	 **/
	private  $productType;


    /**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Admin\ProductUnit")
	 **/
	private  $unit;


    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\ItemMetaAttribute", mappedBy="item" , cascade={"remove"}  )
     **/
    private  $itemMetaAttributes;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\ItemKeyValue", mappedBy="item" , cascade={"remove"}  )
     * @ORM\OrderBy({"sorting" = "ASC"})
     **/
    private  $itemKeyValues;


    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $appModule;


    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="Master product must be required")
     */
    private $name;


    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false, separator="-")
     * @ORM\Column(length=255, unique=true)
     */
    private $slug;


    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $assuranceType;


     /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $warningLabel;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50,nullable = true)
     */
    private $sku;

     /**
     * @var string
     *
     * @ORM\Column(type="string", length=100,nullable = true)
     */
    private $manufacture;


    /**
     * @var integer
     *
     * @ORM\Column(type="integer", length=50, nullable=true)
     */
    private $code;


    /**
	 * @var integer
	 *
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $quantity;

	 /**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $unitPrice;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $purchasePrice;


	/**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $salesPrice;

    /**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $discountPrice;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $minPrice;

    /**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $productionPrice;

    /**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $vatPercent;


     /**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $sdPercent;


    /**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $salesAvgPrice;


     /**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $purchaseAvgPrice;


    /**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $openingQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $minQuantity = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $maxQuantity;


    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $bonusQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $bonusPurchaseQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $bonusSalesQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $transferQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $spoilQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $ongoingQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $orderCreateItem;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $remainingQuantity = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $purchaseQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $purchaseReturnQuantity=0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $salesQuantity = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $salesReturnQuantity = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $damageQuantity = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $disposalQuantity = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $assetsQuantity = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $assetsReturnQuantity = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $productionIssueQuantity;


    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $productionInventoryReturnQuantity;


    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $productionStockQuantity = 0;


    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $productionStockReturnQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $productionBatchItemQuantity = 0;


    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $productionBatchItemReturnQuantity = 0;


    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $productionExpenseQuantity = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $productionExpenseReturnQuantity;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $barcode;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $status = true;

     /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $taxOverride = false;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $itemPrefix;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    private $serialGeneration = 'auto';


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $itemDimension;

    /**
     * @var integer
     *
     * @ORM\Column(type="smallint", length=5)
     */
    private $serialFormat = 2;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $path;

    /**
     * @Assert\File(maxSize="8388608")
     */
    protected $file;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Item
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Item
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Product
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }



	/**
	 * @return Branches
	 */
	public function getBranch() {
		return $this->branch;
	}

	/**
	 * @param Branches $branch
	 */
	public function setBranch( $branch ) {
		$this->branch = $branch;
	}

	/**
	 * @return Category
	 */
	public function getCategory() {
		return $this->category;
	}

	/**
	 * @param Category $category
	 */
	public function setCategory( $category ) {
		$this->category = $category;
	}


	/**
	 * @return float
	 */
	public function getQuantity() {
		return $this->quantity;
	}

	/**
	 * @param float $quantity
	 */
	public function setQuantity( $quantity ) {
		$this->quantity = $quantity;
	}

	/**
	 * @return string
	 */
	public function getAssuranceType() {
		return $this->assuranceType;
	}

	/**
	 * @param string $assuranceType
	 */
	public function setAssuranceType( $assuranceType ) {
		$this->assuranceType = $assuranceType;
	}


	/**
	 * @return float
	 */
	public function getPurchasePrice() {
		return $this->purchasePrice;
	}

	/**
	 * @param float $purchasePrice
	 */
	public function setPurchasePrice( $purchasePrice ) {
		$this->purchasePrice = $purchasePrice;
	}


	/**
	 * @return integer
	 */
	public function getCode() {
		return $this->code;
	}

	/**
	 * @param integer $code
	 */
	public function setCode( $code ) {
		$this->code = $code;
	}

	/**
	 * @return Distribution
	 */
	public function getDistributions() {
		return $this->distributions;
	}

	public function productItem()
	{
		return $product = $this->getItem()->getName().' ('.$this->getName().')';
	}

	public function productItemSerial()
	{
		return $product = $this->getItem()->getName().' ('.$this->getName().') - '.$this->getSerialNo() .'=> BDT.'.$this->getBookValue();
	}



    /**
     * @return Setting
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * @param Setting $productType
     */
    public function setProductType($productType)
    {
        $this->productType = $productType;
    }


    /**
     * @return Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param Brand $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * Sets file.
     *
     * @param Item $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return Item
     */
    public function getFile()
    {
        return $this->file;
    }

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }


    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/domain/'.$this->getConfig()->getGlobalOption()->getId().'/assets/product-group/';
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $filename = date('YmdHmi') . "_" . $this->getFile()->getClientOriginalName();
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $filename
        );

        // set the path property to the filename where you've saved the file
        $this->path = $filename;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }



    /**
     * @return ItemMetaAttribute
     */
    public function getItemMetaAttributes()
    {
        return $this->itemMetaAttributes;
    }

    /**
     * @return ItemKeyValue
     */
    public function getItemKeyValues()
    {
        return $this->itemKeyValues;
    }

    /**
     * @return
     */
    public function getItemWarning()
    {
        return $this->itemWarning;
    }

    /**
     * @param ItemWarning $itemWarning
     */
    public function setItemWarning($itemWarning)
    {
        $this->itemWarning = $itemWarning;
    }

    /**
     * @return mixed
     */
    public function getItemPrefix()
    {
        return $this->itemPrefix;
    }

    /**
     * @param mixed $itemPrefix
     */
    public function setItemPrefix($itemPrefix)
    {
        $this->itemPrefix = $itemPrefix;
    }

    /**
     * @return string
     */
    public function getSerialGeneration()
    {
        return $this->serialGeneration;
    }

    /**
     * @param string $serialGeneration
     */
    public function setSerialGeneration($serialGeneration)
    {
        $this->serialGeneration = $serialGeneration;
    }

    /**
     * @return mixed
     */
    public function getSerialFormat()
    {
        return $this->serialFormat;
    }

    /**
     * @param mixed $serialFormat
     */
    public function setSerialFormat($serialFormat)
    {
        $this->serialFormat = $serialFormat;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return float
     */
    public function getOpeningQuantity()
    {
        return $this->openingQuantity;
    }

    /**
     * @param float $openingQuantity
     */
    public function setOpeningQuantity($openingQuantity)
    {
        $this->openingQuantity = $openingQuantity;
    }


    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getManufacture()
    {
        return $this->manufacture;
    }

    /**
     * @param mixed $manufacture
     */
    public function setManufacture($manufacture)
    {
        $this->manufacture = $manufacture;
    }

    /**
     * @return string
     */
    public function getWarningLabel()
    {
        return $this->warningLabel;
    }

    /**
     * @param string $warningLabel
     */
    public function setWarningLabel($warningLabel)
    {
        $this->warningLabel = $warningLabel;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return ProductUnit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param ProductUnit $unit
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
    }

    /**
     * @return mixed
     */
    public function getSTRPadCode()
    {
        $code = str_pad($this->getCode(),3, '0', STR_PAD_LEFT);
        return $code;
    }


    /**
     * @return mixed
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @param mixed $unitPrice
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @return float
     */
    public function getSalesPrice()
    {
        return $this->salesPrice;
    }

    /**
     * @param float $salesPrice
     */
    public function setSalesPrice($salesPrice)
    {
        $this->salesPrice = $salesPrice;
    }


    /**
     * @return \Terminalbd\InventoryBundle\Entity\PurchaseItem
     */
    public function getPurchaseItems()
    {
        return $this->purchaseItems;
    }

    /**
     * @return DepreciationModel
     */
    public function getDepreciationModel()
    {
        return $this->depreciationModel;
    }

    /**
     * @return PurchaseRequisitionItem
     */
    public function getPurchaseRequisitionItems()
    {
        return $this->purchaseRequisitionItems;
    }

    /**
     * @return PurchaseOrderItem
     */
    public function getPurchaseOrderItems()
    {
        return $this->purchaseOrderItems;
    }

    /**
     * @return Setting
     */
    public function getProductGroup()
    {
        return $this->productGroup;
    }

    /**
     * @param Setting $productGroup
     */
    public function setProductGroup($productGroup)
    {
        $this->productGroup = $productGroup;
    }


    /**
     * @return TaxTariff
     */
    public function getVatProduct()
    {
        return $this->vatProduct;
    }


    /**
     * @param TaxTariff $vatProduct
     */
    public function setVatProduct($vatProduct)
    {
        $this->vatProduct = $vatProduct;
    }

    /**
     * @return string
     */
    public function getVatName()
    {
        return $this->vatName;
    }

    /**
     * @param string $vatName
     */
    public function setVatName($vatName)
    {
        $this->vatName = $vatName;
    }


    /**
     * @return Inventory
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Inventory $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }



    /**
     * @return Damage
     */
    public function getDamages()
    {
        return $this->damages;
    }



    /**
     * @return Setting
     */
    public function getTaxNature()
    {
        return $this->taxNature;
    }

    /**
     * @param Setting $taxNature
     */
    public function setTaxNature($taxNature)
    {
        $this->taxNature = $taxNature;
    }


    /**
     * @return string
     */
    public function getAppModule(): ? string
    {
        return $this->appModule;
    }

    /**
     * @param string $appModule
     */
    public function setAppModule(string $appModule)
    {
        $this->appModule = $appModule;
    }

    /**
     * @return float
     */
    public function getDiscountPrice(): ? float
    {
        return $this->discountPrice;
    }

    /**
     * @param float $discountPrice
     */
    public function setDiscountPrice(float $discountPrice)
    {
        $this->discountPrice = $discountPrice;
    }

    /**
     * @return float
     */
    public function getMinPrice():  ? float
    {
        return $this->minPrice;
    }

    /**
     * @param float $minPrice
     */
    public function setMinPrice(float $minPrice)
    {
        $this->minPrice = $minPrice;
    }

    /**
     * @return float
     */
    public function getProductionPrice(): ? float
    {
        return $this->productionPrice;
    }

    /**
     * @param float $productionPrice
     */
    public function setProductionPrice($productionPrice)
    {
        $this->productionPrice = $productionPrice;
    }

    /**
     * @return float
     */
    public function getSalesAvgPrice(): ? float
    {
        return $this->salesAvgPrice;
    }

    /**
     * @param float $salesAvgPrice
     */
    public function setSalesAvgPrice(float $salesAvgPrice)
    {
        $this->salesAvgPrice = $salesAvgPrice;
    }

    /**
     * @return float
     */
    public function getPurchaseAvgPrice(): ? float
    {
        return $this->purchaseAvgPrice;
    }

    /**
     * @param float $purchaseAvgPrice
     */
    public function setPurchaseAvgPrice(float $purchaseAvgPrice)
    {
        $this->purchaseAvgPrice = $purchaseAvgPrice;
    }



    /**
     * @return mixed
     */
    public function getProductions()
    {
        return $this->productions;
    }


    /**
     * @return mixed
     */
    public function getPriceMethod()
    {
        return $this->priceMethod;
    }

    /**
     * @param mixed $priceMethod
     */
    public function setPriceMethod($priceMethod)
    {
        $this->priceMethod = $priceMethod;
    }

    /**
     * @return MasterItem
     */
    public function getMasterItem()
    {
        return $this->masterItem;
    }

    /**
     * @param MasterItem $masterItem
     */
    public function setMasterItem($masterItem)
    {
        $this->masterItem = $masterItem;
    }

    /**
     * @return mixed
     */
    public function getProductionItems()
    {
        return $this->productionItems;
    }

    /**
     * @param mixed $productionItems
     */
    public function setProductionItems($productionItems)
    {
        $this->productionItems = $productionItems;
    }

    /**
     * @return string
     */
    public function getItemDimension(): ? string
    {
        return $this->itemDimension;
    }

    /**
     * @param string $itemDimension
     */
    public function setItemDimension(string $itemDimension)
    {
        $this->itemDimension = $itemDimension;
    }


    /**
     * @return float
     */
    public function getMinQuantity(): ? float
    {
        return $this->minQuantity;
    }

    /**
     * @param float $minQuantity
     */
    public function setMinQuantity(float $minQuantity)
    {
        $this->minQuantity = $minQuantity;
    }

    /**
     * @return float
     */
    public function getMaxQuantity(): ? float
    {
        return $this->maxQuantity;
    }

    /**
     * @param float $maxQuantity
     */
    public function setMaxQuantity(float $maxQuantity)
    {
        $this->maxQuantity = $maxQuantity;
    }


    /**
     * @return float
     */
    public function getBonusQuantity(): ? float
    {
        return $this->bonusQuantity;
    }

    /**
     * @param float $bonusQuantity
     */
    public function setBonusQuantity(float $bonusQuantity)
    {
        $this->bonusQuantity = $bonusQuantity;
    }

    /**
     * @return float
     */
    public function getBonusPurchaseQuantity(): ? float
    {
        return $this->bonusPurchaseQuantity;
    }

    /**
     * @param float $bonusPurchaseQuantity
     */
    public function setBonusPurchaseQuantity(float $bonusPurchaseQuantity)
    {
        $this->bonusPurchaseQuantity = $bonusPurchaseQuantity;
    }

    /**
     * @return float
     */
    public function getBonusSalesQuantity(): ? float
    {
        return $this->bonusSalesQuantity;
    }

    /**
     * @param float $bonusSalesQuantity
     */
    public function setBonusSalesQuantity(float $bonusSalesQuantity)
    {
        $this->bonusSalesQuantity = $bonusSalesQuantity;
    }

    /**
     * @return float
     */
    public function getTransferQuantity(): ? float
    {
        return $this->transferQuantity;
    }

    /**
     * @param float $transferQuantity
     */
    public function setTransferQuantity(float $transferQuantity)
    {
        $this->transferQuantity = $transferQuantity;
    }

    /**
     * @return float
     */
    public function getSpoilQuantity(): ? float
    {
        return $this->spoilQuantity;
    }

    /**
     * @param float $spoilQuantity
     */
    public function setSpoilQuantity(float $spoilQuantity)
    {
        $this->spoilQuantity = $spoilQuantity;
    }

    /**
     * @return float
     */
    public function getOngoingQuantity(): ? float
    {
        return $this->ongoingQuantity;
    }

    /**
     * @param float $ongoingQuantity
     */
    public function setOngoingQuantity(float $ongoingQuantity)
    {
        $this->ongoingQuantity = $ongoingQuantity;
    }

    /**
     * @return float
     */
    public function getOrderCreateItem(): ? float
    {
        return $this->orderCreateItem;
    }

    /**
     * @param float $orderCreateItem
     */
    public function setOrderCreateItem(float $orderCreateItem)
    {
        $this->orderCreateItem = $orderCreateItem;
    }

    /**
     * @return float
     */
    public function getRemainingQuantity(): ? float
    {
        return $this->remainingQuantity;
    }

    /**
     * @param float $remainingQuantity
     */
    public function setRemainingQuantity(float $remainingQuantity)
    {
        $this->remainingQuantity = $remainingQuantity;
    }

    /**
     * @return float
     */
    public function getPurchaseQuantity(): ? float
    {
        return $this->purchaseQuantity;
    }

    /**
     * @param float $purchaseQuantity
     */
    public function setPurchaseQuantity(float $purchaseQuantity)
    {
        $this->purchaseQuantity = $purchaseQuantity;
    }

    /**
     * @return float
     */
    public function getPurchaseReturnQuantity(): ? float
    {
        return $this->purchaseReturnQuantity;
    }

    /**
     * @param float $purchaseReturnQuantity
     */
    public function setPurchaseReturnQuantity(float $purchaseReturnQuantity)
    {
        $this->purchaseReturnQuantity = $purchaseReturnQuantity;
    }


    /**
     * @return float
     */
    public function getSalesReturnQuantity(): ? float
    {
        return $this->salesReturnQuantity;
    }

    /**
     * @param float $salesReturnQuantity
     */
    public function setSalesReturnQuantity(float $salesReturnQuantity)
    {
        $this->salesReturnQuantity = $salesReturnQuantity;
    }

    /**
     * @return float
     */
    public function getDamageQuantity():  ? float
    {
        return $this->damageQuantity;
    }

    /**
     * @param float $damageQuantity
     */
    public function setDamageQuantity(float $damageQuantity)
    {
        $this->damageQuantity = $damageQuantity;
    }

    /**
     * @return float
     */
    public function getDisposalQuantity(): ?  float
    {
        return $this->disposalQuantity;
    }

    /**
     * @param float $disposalQuantity
     */
    public function setDisposalQuantity(float $disposalQuantity)
    {
        $this->disposalQuantity = $disposalQuantity;
    }

    /**
     * @return float
     */
    public function getAssetsQuantity():  ? float
    {
        return $this->assetsQuantity;
    }

    /**
     * @param float $assetsQuantity
     */
    public function setAssetsQuantity(float $assetsQuantity)
    {
        $this->assetsQuantity = $assetsQuantity;
    }

    /**
     * @return float
     */
    public function getAssetsReturnQuantity()
    {
        return $this->assetsReturnQuantity;
    }

    /**
     * @param float $assetsReturnQuantity
     */
    public function setAssetsReturnQuantity($assetsReturnQuantity)
    {
        $this->assetsReturnQuantity = $assetsReturnQuantity;
    }

    /**
     * @return float
     */
    public function getSalesQuantity()
    {
        return $this->salesQuantity;
    }

    /**
     * @param float $salesQuantity
     */
    public function setSalesQuantity($salesQuantity)
    {
        $this->salesQuantity = $salesQuantity;
    }

    /**
     * @return float
     */
    public function getProductionIssueQuantity()
    {
        return $this->productionIssueQuantity;
    }

    /**
     * @param float $productionIssueQuantity
     */
    public function setProductionIssueQuantity($productionIssueQuantity)
    {
        $this->productionIssueQuantity = $productionIssueQuantity;
    }


    /**
     * @return float
     */
    public function getProductionInventoryReturnQuantity()
    {
        return $this->productionInventoryReturnQuantity;
    }

    /**
     * @param float $productionInventoryReturnQuantity
     */
    public function setProductionInventoryReturnQuantity($productionInventoryReturnQuantity)
    {
        $this->productionInventoryReturnQuantity = $productionInventoryReturnQuantity;
    }

    /**
     * @return float
     */
    public function getProductionStockQuantity()
    {
        return $this->productionStockQuantity;
    }

    /**
     * @param float $productionStockQuantity
     */
    public function setProductionStockQuantity($productionStockQuantity)
    {
        $this->productionStockQuantity = $productionStockQuantity;
    }

    /**
     * @return float
     */
    public function getProductionStockReturnQuantity()
    {
        return $this->productionStockReturnQuantity;
    }

    /**
     * @param float $productionStockReturnQuantity
     */
    public function setProductionStockReturnQuantity($productionStockReturnQuantity)
    {
        $this->productionStockReturnQuantity = $productionStockReturnQuantity;
    }

    /**
     * @return float
     */
    public function getProductionExpenseQuantity()
    {
        return $this->productionExpenseQuantity;
    }

    /**
     * @param float $productionExpenseQuantity
     */
    public function setProductionExpenseQuantity(float $productionExpenseQuantity)
    {
        $this->productionExpenseQuantity = $productionExpenseQuantity;
    }

    /**
     * @return float
     */
    public function getProductionExpenseReturnQuantity()
    {
        return $this->productionExpenseReturnQuantity;
    }

    /**
     * @param float $productionExpenseReturnQuantity
     */
    public function setProductionExpenseReturnQuantity(float $productionExpenseReturnQuantity)
    {
        $this->productionExpenseReturnQuantity = $productionExpenseReturnQuantity;
    }

    /**
     * @return float
     */
    public function getProductionBatchItemQuantity()
    {
        return $this->productionBatchItemQuantity;
    }

    /**
     * @param float $productionBatchItemQuantity
     */
    public function setProductionBatchItemQuantity(float $productionBatchItemQuantity)
    {
        $this->productionBatchItemQuantity = $productionBatchItemQuantity;
    }

    /**
     * @return float
     */
    public function getProductionBatchItemReturnQuantity()
    {
        return $this->productionBatchItemReturnQuantity;
    }

    /**
     * @param float $productionBatchItemReturnQuantity
     */
    public function setProductionBatchItemReturnQuantity(float $productionBatchItemReturnQuantity)
    {
        $this->productionBatchItemReturnQuantity = $productionBatchItemReturnQuantity;
    }

    /**
     * @return float
     */
    public function getVatPercent()
    {
        return $this->vatPercent;
    }

    /**
     * @param float $vatPercent
     */
    public function setVatPercent($vatPercent)
    {
        $this->vatPercent = $vatPercent;
    }

    /**
     * @return float
     */
    public function getSdPercent()
    {
        return $this->sdPercent;
    }

    /**
     * @param float $sdPercent
     */
    public function setSdPercent($sdPercent)
    {
        $this->sdPercent = $sdPercent;
    }

    /**
     * @return bool
     */
    public function isTaxOverride(): bool
    {
        return $this->taxOverride;
    }

    /**
     * @param bool $taxOverride
     */
    public function setTaxOverride(bool $taxOverride)
    {
        $this->taxOverride = $taxOverride;
    }

    /**
     * @return \Terminalbd\NbrvatBundle\Entity\Setting
     */
    public function getInputTax()
    {
        return $this->inputTax;
    }

    /**
     * @param \Terminalbd\NbrvatBundle\Entity\Setting $inputTax
     */
    public function setInputTax($inputTax)
    {
        $this->inputTax = $inputTax;
    }

    /**
     * @return ProductionItem
     */
    public function getProductionItem()
    {
        return $this->productionItem;
    }





}

