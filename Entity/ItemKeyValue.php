<?php

namespace Terminalbd\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ItemMetaAttribute
 *
 * @ORM\Table(name="inv_item_key_value")
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\ItemKeyValueRepository")
 */
class ItemKeyValue
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Item", inversedBy="itemKeyValues" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $item;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\MasterItem", inversedBy="itemKeyValues" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $masterItem;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Setting", inversedBy="itemKeyValues" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $setting;



    /**
     * @var string
     *
     * @ORM\Column(name="metaKey", type="string", length=255, nullable = true)
     */
    private $metaKey;

    /**
     * @var string
     *
     * @ORM\Column(name="metaValue", type="string", length=255 , nullable = true)
     */
    private $metaValue;

    /**
     * @var Integer
     *
     * @ORM\Column(name="sorting", type="smallint", length=2, nullable = true)
     */
    private $sorting;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getMetaKey()
    {
        return $this->metaKey;
    }

    /**
     * @param string $metaKey
     */
    public function setMetaKey($metaKey)
    {
        $this->metaKey = $metaKey;
    }

    /**
     * @return string
     */
    public function getMetaValue()
    {
        return $this->metaValue;
    }

    /**
     * @param string $metaValue
     */
    public function setMetaValue($metaValue)
    {
        $this->metaValue = $metaValue;
    }

   
    /**
     * @return int
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param int $sorting
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
    }



    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return mixed
     */
    public function getMasterItem()
    {
        return $this->masterItem;
    }

    /**
     * @param mixed $masterItem
     */
    public function setMasterItem($masterItem)
    {
        $this->masterItem = $masterItem;
    }

    /**
     * @return Setting
     */
    public function getSetting()
    {
        return $this->setting;
    }

    /**
     * @param Setting $setting
     */
    public function setSetting($setting)
    {
        $this->setting = $setting;
    }




}

