<?php

namespace Terminalbd\InventoryBundle\Entity;

use Appstore\Bundle\AccountingBundle\Entity\VoucherItem;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * ItemMetaAttribute
 *
 * @ORM\Table(name="inv_item_meta_attribute")
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\ItemMeatAttributeRepository")
 */
class ItemMetaAttribute
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Item", inversedBy="itemMetaAttributes" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $item;



     /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\PurchaseItem", inversedBy="itemMetaAttributes" )
     * @ORM\JoinColumn(onDelete="CASCADE")
    **/
    private  $purchaseItem;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\CategoryMeta", inversedBy="itemMetaAttributes" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $categoryMeta;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;


    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status = true;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return boolean
     */
    public function isStatus()
    {
        return $this->status;
    }



    /**
     * @return CategoryMeta
     */
    public function getCategoryMeta()
    {
        return $this->categoryMeta;
    }

    /**
     * @param CategoryMeta $categoryMeta
     */
    public function setCategoryMeta($categoryMeta)
    {
        $this->categoryMeta = $categoryMeta;
    }



    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return PurchaseItem
     */
    public function getPurchaseItem()
    {
        return $this->purchaseItem;
    }

    /**
     * @param PurchaseItem $purchaseItem
     */
    public function setPurchaseItem($purchaseItem)
    {
        $this->purchaseItem = $purchaseItem;
    }


}

