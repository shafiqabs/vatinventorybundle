<?php

namespace Terminalbd\InventoryBundle\Entity;

use App\Entity\Admin\ProductUnit;
use App\Entity\Application\Inventory;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Product
 *
 * @ORM\Table("inv_item_vat")
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\ItemVatRepository")
 */
class ItemVat
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
	 * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\MasterItem", inversedBy="vatItem")
     * @ORM\JoinColumn(onDelete="CASCADE")
	 **/
	private  $masterItem;

    /**
	 * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Setting")
	 **/
	private  $surcharge;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", nullable=true)
     */
    private $amount = 0.00;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $mode = 'percent';


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return MasterItem
     */
    public function getMasterItem()
    {
        return $this->masterItem;
    }

    /**
     * @param MasterItem $masterItem
     */
    public function setMasterItem($masterItem)
    {
        $this->masterItem = $masterItem;
    }


    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }



    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode(string $mode)
    {
        $this->mode = $mode;
    }

    /**
     * @return Setting
     */
    public function getSurcharge()
    {
        return $this->surcharge;
    }

    /**
     * @param Setting $surcharge
     */
    public function setSurcharge($surcharge)
    {
        $this->surcharge = $surcharge;
    }




}

