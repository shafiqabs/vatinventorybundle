<?php

namespace Terminalbd\InventoryBundle\Entity;

use App\Entity\Admin\ProductUnit;
use App\Entity\Application\Inventory;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Terminalbd\NbrvatBundle\Entity\TaxTariff;

/**
 * Product
 *
 * @ORM\Table("inv_master_item")
 * @UniqueEntity(fields="name",message="Item name already existing,Please try another.")
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\MasterItemRepository")
 */
class MasterItem
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Application\Inventory")
     * @ORM\JoinColumn(onDelete="CASCADE")
	 **/
	private  $config;


    /**
	 * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Setting", inversedBy="items" )
     * @Assert\NotBlank(message="Item group not blank")
	 **/
	private  $productGroup;

    /**
	 * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\ItemVat", mappedBy="masterItem" )
	 **/
	private  $vatItem;

    /**
	 * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Category", inversedBy="items" )
	 **/
	private  $category;


	/**
	 * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Setting", inversedBy="brandItems" )
	 **/
	private  $brand;

    /**
	 * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Setting", inversedBy="typeItems" )
	 **/
	private  $productType;

    /**
	 * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Setting")
	 **/
	private  $taxNature;

     /**
	 * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\Setting", inversedBy="supplyOutputTaxItems" )
	 **/
	private  $supplyOutputTax;

    /**
	 * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\Setting")
	 **/
	private  $inputTax;


    /**
	 * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\Setting")
	 **/
	private  $inputImportTax;


    /**
	 * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Setting")
	 **/
	private  $priceMethod;


    /**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Admin\ProductUnit")
	 **/
	private  $unit;


    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\ItemMetaAttribute", mappedBy="item" , cascade={"remove"}  )
     **/
    private  $itemMetaAttributes;

     /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\Item", mappedBy="masterItem" , cascade={"remove"}  )
     **/
    private  $items;

     /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\PurchaseItem", mappedBy="masterItem" , cascade={"remove"}  )
     **/
    private  $purchaseItems;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\ItemKeyValue", mappedBy="masterItem" , cascade={"remove"}  )
     * @ORM\OrderBy({"sorting" = "ASC"})
     **/
    private  $itemKeyValues;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\TaxTariff")
     **/
    private  $taxTariff;


    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $appModule;


    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="Product name must be required")
     */
    private $name;


    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false, separator="-")
     * @ORM\Column(length=255, unique=true)
     */
    private $slug;


     /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $vatName;


     /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $hsCode;



    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $assuranceType;


     /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $warningLabel;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50,nullable = true)
     */
    private $sku;

     /**
     * @var string
     *
     * @ORM\Column(type="string", length=100,nullable = true)
     */
    private $manufacture;


    /**
     * @var integer
     *
     * @ORM\Column(type="integer", length=50, nullable=true)
     */
    private $code;


    /**
	 * @var integer
	 *
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $quantity;

	 /**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $unitPrice;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $purchasePrice;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $salesPrice;

    /**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $discountPrice;

	/**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $minPrice;

    /**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $productionPrice;


    /**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $salesAvgPrice;


     /**
	 * @var float
	 *
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $purchaseAvgPrice;


    /**
	 * @var float
	 *
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $openingQuantity;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $minQuantity = 0;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $maxQuantity;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $productionQuantity;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $productionReturnQuantity;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $bonusQuantity;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $bonusPurchaseQuantity;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $bonusSalesQuantity;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $transferQuantity;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $spoilQuantity;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ongoingQuantity;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orderCreateItem;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $remainingQuantity = 0;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $purchaseQuantity;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $purchaseReturnQuantity=0;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $salesQuantity = 0;

    /**
     * @var integer
     *
     * @ORM\Column( type="integer", nullable=true)
     */
    private $salesReturnQuantity = 0;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $damageQuantity = 0;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $disposalQuantity = 0;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $assetsQuantity = 0;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $assetsReturnQuantity = 0;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $barcode;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $status = true;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $itemPrefix;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    private $serialGeneration = 'auto';

    /**
     * @var integer
     *
     * @ORM\Column(type="smallint", length=5)
     */
    private $serialFormat = 2;

    /**
     * @var float
     *
     * @ORM\Column(name="customsDuty", type="float", nullable=true)
     */
    private $customsDuty = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $vatDeductionSource = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="supplementaryDuty", type="float", nullable=true)
     */
    private $supplementaryDuty = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="recurringDeposit", type="float", nullable=true)
     */
    private $recurringDeposit = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="valueAddedTax", type="float", nullable=true)
     */
    private $valueAddedTax = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $minValueAddedTax = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="advanceIncomeTax", type="float", nullable=true)
     */
    private $advanceIncomeTax = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="advanceTradeVat", type="float", nullable=true)
     */
    private $advanceTradeVat = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="regulatoryDuty", type="float", nullable=true)
     */
    private $regulatoryDuty = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="advanceTax", type="float", nullable=true)
     */
    private $advanceTax = 0.00;



    /**
     * @var float
     *
     * @ORM\Column(name="rebate", type="float", nullable=true)
     */
    private $rebate = 0.00;



    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $path;

    /**
     * @Assert\File(maxSize="8388608")
     */
    protected $file;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Item
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Item
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Product
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }



	/**
	 * @return Branches
	 */
	public function getBranch() {
		return $this->branch;
	}

	/**
	 * @param Branches $branch
	 */
	public function setBranch( $branch ) {
		$this->branch = $branch;
	}

	/**
	 * @return Category
	 */
	public function getCategory() {
		return $this->category;
	}

	/**
	 * @param Category $category
	 */
	public function setCategory( $category ) {
		$this->category = $category;
	}



	/**
	 * @return float
	 */
	public function getQuantity() {
		return $this->quantity;
	}

	/**
	 * @param float $quantity
	 */
	public function setQuantity( $quantity ) {
		$this->quantity = $quantity;
	}

	/**
	 * @return string
	 */
	public function getAssuranceType() {
		return $this->assuranceType;
	}

	/**
	 * @param string $assuranceType
	 */
	public function setAssuranceType( $assuranceType ) {
		$this->assuranceType = $assuranceType;
	}


	/**
	 * @return float
	 */
	public function getPurchasePrice() {
		return $this->purchasePrice;
	}

	/**
	 * @param float $purchasePrice
	 */
	public function setPurchasePrice( $purchasePrice ) {
		$this->purchasePrice = $purchasePrice;
	}


	/**
	 * @return integer
	 */
	public function getCode() {
		return $this->code;
	}

	/**
	 * @param integer $code
	 */
	public function setCode( $code ) {
		$this->code = $code;
	}

	/**
	 * @return Distribution
	 */
	public function getDistributions() {
		return $this->distributions;
	}

	public function productItem()
	{
		return $product = $this->getItem()->getName().' ('.$this->getName().')';
	}

	public function productItemSerial()
	{
		return $product = $this->getItem()->getName().' ('.$this->getName().') - '.$this->getSerialNo() .'=> BDT.'.$this->getBookValue();
	}



    /**
     * @return Setting
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * @param Setting $productType
     */
    public function setProductType($productType)
    {
        $this->productType = $productType;
    }


    /**
     * @return Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param Brand $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * Sets file.
     *
     * @param Item $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return Item
     */
    public function getFile()
    {
        return $this->file;
    }

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }


    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/domain/'.$this->getConfig()->getGlobalOption()->getId().'/assets/product-group/';
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $filename = date('YmdHmi') . "_" . $this->getFile()->getClientOriginalName();
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $filename
        );

        // set the path property to the filename where you've saved the file
        $this->path = $filename;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }



    /**
     * @return ItemMetaAttribute
     */
    public function getItemMetaAttributes()
    {
        return $this->itemMetaAttributes;
    }

    /**
     * @return ItemKeyValue
     */
    public function getItemKeyValues()
    {
        return $this->itemKeyValues;
    }

    /**
     * @return
     */
    public function getItemWarning()
    {
        return $this->itemWarning;
    }

    /**
     * @param ItemWarning $itemWarning
     */
    public function setItemWarning($itemWarning)
    {
        $this->itemWarning = $itemWarning;
    }

    /**
     * @return mixed
     */
    public function getItemPrefix()
    {
        return $this->itemPrefix;
    }

    /**
     * @param mixed $itemPrefix
     */
    public function setItemPrefix($itemPrefix)
    {
        $this->itemPrefix = $itemPrefix;
    }

    /**
     * @return string
     */
    public function getSerialGeneration()
    {
        return $this->serialGeneration;
    }

    /**
     * @param string $serialGeneration
     */
    public function setSerialGeneration($serialGeneration)
    {
        $this->serialGeneration = $serialGeneration;
    }

    /**
     * @return mixed
     */
    public function getSerialFormat()
    {
        return $this->serialFormat;
    }

    /**
     * @param mixed $serialFormat
     */
    public function setSerialFormat($serialFormat)
    {
        $this->serialFormat = $serialFormat;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return float
     */
    public function getOpeningQuantity()
    {
        return $this->openingQuantity;
    }

    /**
     * @param float $openingQuantity
     */
    public function setOpeningQuantity($openingQuantity)
    {
        $this->openingQuantity = $openingQuantity;
    }

    /**
     * @return int
     */
    public function getMinQuantity()
    {
        return $this->minQuantity;
    }

    /**
     * @param int $minQuantity
     */
    public function setMinQuantity($minQuantity)
    {
        $this->minQuantity = $minQuantity;
    }

    /**
     * @return int
     */
    public function getMaxQuantity()
    {
        return $this->maxQuantity;
    }

    /**
     * @param int $maxQuantity
     */
    public function setMaxQuantity($maxQuantity)
    {
        $this->maxQuantity = $maxQuantity;
    }

    /**
     * @return int
     */
    public function getRemainingQuantity()
    {
        return $this->remainingQuantity;
    }

    /**
     * @param int $remainingQuantity
     */
    public function setRemainingQuantity($remainingQuantity)
    {
        $this->remainingQuantity = $remainingQuantity;
    }

    /**
     * @return int
     */
    public function getPurchaseQuantity()
    {
        return $this->purchaseQuantity;
    }

    /**
     * @param int $purchaseQuantity
     */
    public function setPurchaseQuantity($purchaseQuantity)
    {
        $this->purchaseQuantity = $purchaseQuantity;
    }

    /**
     * @return int
     */
    public function getPurchaseReturnQuantity()
    {
        return $this->purchaseReturnQuantity;
    }

    /**
     * @param int $purchaseReturnQuantity
     */
    public function setPurchaseReturnQuantity($purchaseReturnQuantity)
    {
        $this->purchaseReturnQuantity = $purchaseReturnQuantity;
    }

    /**
     * @return int
     */
    public function getSalesQuantity()
    {
        return $this->salesQuantity;
    }

    /**
     * @param int $salesQuantity
     */
    public function setSalesQuantity($salesQuantity)
    {
        $this->salesQuantity = $salesQuantity;
    }

    /**
     * @return int
     */
    public function getSalesReturnQuantity()
    {
        return $this->salesReturnQuantity;
    }

    /**
     * @param int $salesReturnQuantity
     */
    public function setSalesReturnQuantity($salesReturnQuantity)
    {
        $this->salesReturnQuantity = $salesReturnQuantity;
    }

    /**
     * @return int
     */
    public function getOrderCreateItem()
    {
        return $this->orderCreateItem;
    }

    /**
     * @param int $orderCreateItem
     */
    public function setOrderCreateItem($orderCreateItem)
    {
        $this->orderCreateItem = $orderCreateItem;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getManufacture()
    {
        return $this->manufacture;
    }

    /**
     * @param mixed $manufacture
     */
    public function setManufacture($manufacture)
    {
        $this->manufacture = $manufacture;
    }

    /**
     * @return string
     */
    public function getWarningLabel()
    {
        return $this->warningLabel;
    }

    /**
     * @param string $warningLabel
     */
    public function setWarningLabel($warningLabel)
    {
        $this->warningLabel = $warningLabel;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return ProductUnit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param ProductUnit $unit
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
    }

    /**
     * @return mixed
     */
    public function getSTRPadCode()
    {
        $code = str_pad($this->getCode(),3, '0', STR_PAD_LEFT);
        return $code;
    }

    /**
     * @return int
     */
    public function getDamageQuantity()
    {
        return $this->damageQuantity;
    }

    /**
     * @param int $damageQuantity
     */
    public function setDamageQuantity($damageQuantity)
    {
        $this->damageQuantity = $damageQuantity;
    }

    /**
     * @return int
     */
    public function getDisposalQuantity()
    {
        return $this->disposalQuantity;
    }

    /**
     * @param int $disposalQuantity
     */
    public function setDisposalQuantity($disposalQuantity)
    {
        $this->disposalQuantity = $disposalQuantity;
    }

    /**
     * @return mixed
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @param mixed $unitPrice
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @return float
     */
    public function getSalesPrice()
    {
        return $this->salesPrice;
    }

    /**
     * @param float $salesPrice
     */
    public function setSalesPrice($salesPrice)
    {
        $this->salesPrice = $salesPrice;
    }


    /**
     * @return \Terminalbd\InventoryBundle\Entity\PurchaseItem
     */
    public function getPurchaseItems()
    {
        return $this->purchaseItems;
    }

    /**
     * @return DepreciationModel
     */
    public function getDepreciationModel()
    {
        return $this->depreciationModel;
    }

    /**
     * @return PurchaseRequisitionItem
     */
    public function getPurchaseRequisitionItems()
    {
        return $this->purchaseRequisitionItems;
    }

    /**
     * @return PurchaseOrderItem
     */
    public function getPurchaseOrderItems()
    {
        return $this->purchaseOrderItems;
    }

    /**
     * @return Setting
     */
    public function getProductGroup()
    {
        return $this->productGroup;
    }

    /**
     * @param Setting $productGroup
     */
    public function setProductGroup($productGroup)
    {
        $this->productGroup = $productGroup;
    }


    /**
     * @return TaxTariff
     */
    public function getVatProduct()
    {
        return $this->vatProduct;
    }


    /**
     * @param TaxTariff $vatProduct
     */
    public function setVatProduct($vatProduct)
    {
        $this->vatProduct = $vatProduct;
    }

    /**
     * @return string
     */
    public function getVatName()
    {
        return $this->vatName;
    }

    /**
     * @param string $vatName
     */
    public function setVatName($vatName)
    {
        $this->vatName = $vatName;
    }

    /**
     * @return int
     */
    public function getAssetsQuantity()
    {
        return $this->assetsQuantity;
    }

    /**
     * @param int $assetsQuantity
     */
    public function setAssetsQuantity($assetsQuantity)
    {
        $this->assetsQuantity = $assetsQuantity;
    }

    /**
     * @return Inventory
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Inventory $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }


    /**
     * @return int
     */
    public function getAssetsReturnQuantity()
    {
        return $this->assetsReturnQuantity;
    }

    /**
     * @param int $assetsReturnQuantity
     */
    public function setAssetsReturnQuantity($assetsReturnQuantity)
    {
        $this->assetsReturnQuantity = $assetsReturnQuantity;
    }

    /**
     * @return Damage
     */
    public function getDamages()
    {
        return $this->damages;
    }

    /**
     * @return string
     */
    public function getHsCode()
    {
        return $this->hsCode;
    }

    /**
     * @param string $hsCode
     */
    public function setHsCode($hsCode)
    {
        $this->hsCode = $hsCode;
    }

    /**
     * @return int
     */
    public function getOngoingQuantity(): ? int
    {
        return $this->ongoingQuantity;
    }

    /**
     * @param int $ongoingQuantity
     */
    public function setOngoingQuantity(int $ongoingQuantity)
    {
        $this->ongoingQuantity = $ongoingQuantity;
    }

    /**
     * @return Setting
     */
    public function getTaxNature()
    {
        return $this->taxNature;
    }

    /**
     * @param Setting $taxNature
     */
    public function setTaxNature($taxNature)
    {
        $this->taxNature = $taxNature;
    }

    /**
     * @return ItemVat
     */
    public function getVatItem()
    {
        return $this->vatItem;
    }


    /**
     * @return float
     */
    public function getCustomsDuty()
    {
        return $this->customsDuty;
    }

    /**
     * @param float $customsDuty
     */
    public function setCustomsDuty($customsDuty)
    {
        $this->customsDuty = $customsDuty;
    }

    /**
     * @return float
     */
    public function getSupplementaryDuty()
    {
        return $this->supplementaryDuty;
    }

    /**
     * @param float $supplementaryDuty
     */
    public function setSupplementaryDuty($supplementaryDuty)
    {
        $this->supplementaryDuty = $supplementaryDuty;
    }

    /**
     * @return float
     */
    public function getValueAddedTax()
    {
        return $this->valueAddedTax;
    }

    /**
     * @param float $valueAddedTax
     */
    public function setValueAddedTax($valueAddedTax)
    {
        $this->valueAddedTax = $valueAddedTax;
    }

    /**
     * @return float
     */
    public function getAdvanceIncomeTax()
    {
        return $this->advanceIncomeTax;
    }

    /**
     * @param float $advanceIncomeTax
     */
    public function setAdvanceIncomeTax($advanceIncomeTax)
    {
        $this->advanceIncomeTax = $advanceIncomeTax;
    }


    /**
     * @return float
     */
    public function getAdvanceTradeVat()
    {
        return $this->advanceTradeVat;
    }

    /**
     * @param float $advanceTradeVat
     */
    public function setAdvanceTradeVat($advanceTradeVat)
    {
        $this->advanceTradeVat = $advanceTradeVat;
    }

    /**
     * @return float
     */
    public function getRebate()
    {
        return $this->rebate;
    }

    /**
     * @param float $rebate
     */
    public function setRebate($rebate)
    {
        $this->rebate = $rebate;
    }

    /**
     * @return float
     */
    public function getRegulatoryDuty()
    {
        return $this->regulatoryDuty;
    }

    /**
     * @param float $regulatoryDuty
     */
    public function setRegulatoryDuty($regulatoryDuty)
    {
        $this->regulatoryDuty = $regulatoryDuty;
    }

    /**
     * @return float
     */
    public function getAdvanceTax()
    {
        return $this->advanceTax;
    }

    /**
     * @param float $advanceTax
     */
    public function setAdvanceTax($advanceTax)
    {
        $this->advanceTax = $advanceTax;
    }

    /**
     * @return int
     */
    public function getProductionQuantity(): ? int
    {
        return $this->productionQuantity;
    }

    /**
     * @param int $productionQuantity
     */
    public function setProductionQuantity(int $productionQuantity)
    {
        $this->productionQuantity = $productionQuantity;
    }

    /**
     * @return string
     */
    public function getAppModule(): ? string
    {
        return $this->appModule;
    }

    /**
     * @param string $appModule
     */
    public function setAppModule(string $appModule)
    {
        $this->appModule = $appModule;
    }

    /**
     * @return float
     */
    public function getDiscountPrice()
    {
        return $this->discountPrice;
    }

    /**
     * @param float $discountPrice
     */
    public function setDiscountPrice(float $discountPrice)
    {
        $this->discountPrice = $discountPrice;
    }

    /**
     * @return float
     */
    public function getMinPrice()
    {
        return $this->minPrice;
    }

    /**
     * @param float $minPrice
     */
    public function setMinPrice(float $minPrice)
    {
        $this->minPrice = $minPrice;
    }

    /**
     * @return float
     */
    public function getProductionPrice()
    {
        return $this->productionPrice;
    }

    /**
     * @param float $productionPrice
     */
    public function setProductionPrice(float $productionPrice)
    {
        $this->productionPrice = $productionPrice;
    }

    /**
     * @return float
     */
    public function getSalesAvgPrice()
    {
        return $this->salesAvgPrice;
    }

    /**
     * @param float $salesAvgPrice
     */
    public function setSalesAvgPrice(float $salesAvgPrice)
    {
        $this->salesAvgPrice = $salesAvgPrice;
    }

    /**
     * @return float
     */
    public function getPurchaseAvgPrice()
    {
        return $this->purchaseAvgPrice;
    }

    /**
     * @param float $purchaseAvgPrice
     */
    public function setPurchaseAvgPrice(float $purchaseAvgPrice)
    {
        $this->purchaseAvgPrice = $purchaseAvgPrice;
    }

    /**
     * @return int
     */
    public function getProductionReturnQuantity(): int
    {
        return $this->productionReturnQuantity;
    }

    /**
     * @param int $productionReturnQuantity
     */
    public function setProductionReturnQuantity(int $productionReturnQuantity)
    {
        $this->productionReturnQuantity = $productionReturnQuantity;
    }

    /**
     * @return int
     */
    public function getBonusQuantity(): int
    {
        return $this->bonusQuantity;
    }

    /**
     * @param int $bonusQuantity
     */
    public function setBonusQuantity(int $bonusQuantity)
    {
        $this->bonusQuantity = $bonusQuantity;
    }

    /**
     * @return int
     */
    public function getBonusPurchaseQuantity(): int
    {
        return $this->bonusPurchaseQuantity;
    }

    /**
     * @param int $bonusPurchaseQuantity
     */
    public function setBonusPurchaseQuantity(int $bonusPurchaseQuantity)
    {
        $this->bonusPurchaseQuantity = $bonusPurchaseQuantity;
    }

    /**
     * @return int
     */
    public function getBonusSalesQuantity(): int
    {
        return $this->bonusSalesQuantity;
    }

    /**
     * @param int $bonusSalesQuantity
     */
    public function setBonusSalesQuantity(int $bonusSalesQuantity)
    {
        $this->bonusSalesQuantity = $bonusSalesQuantity;
    }

    /**
     * @return int
     */
    public function getTransferQuantity(): int
    {
        return $this->transferQuantity;
    }

    /**
     * @param int $transferQuantity
     */
    public function setTransferQuantity(int $transferQuantity)
    {
        $this->transferQuantity = $transferQuantity;
    }

    /**
     * @return int
     */
    public function getSpoilQuantity(): int
    {
        return $this->spoilQuantity;
    }

    /**
     * @param int $spoilQuantity
     */
    public function setSpoilQuantity(int $spoilQuantity)
    {
        $this->spoilQuantity = $spoilQuantity;
    }

    /**
     * @return mixed
     */
    public function getProductions()
    {
        return $this->productions;
    }


    /**
     * @return mixed
     */
    public function getPriceMethod()
    {
        return $this->priceMethod;
    }

    /**
     * @param mixed $priceMethod
     */
    public function setPriceMethod($priceMethod)
    {
        $this->priceMethod = $priceMethod;
    }

    /**
     * @return float
     */
    public function getRecurringDeposit()
    {
        return $this->recurringDeposit;
    }

    /**
     * @param float $recurringDeposit
     */
    public function setRecurringDeposit(float $recurringDeposit)
    {
        $this->recurringDeposit = $recurringDeposit;
    }

    /**
     * @return Item
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return float
     */
    public function getVatDeductionSource()
    {
        return $this->vatDeductionSource;
    }

    /**
     * @param float $vatDeductionSource
     */
    public function setVatDeductionSource(float $vatDeductionSource)
    {
        $this->vatDeductionSource = $vatDeductionSource;
    }

    /**
     * @return float
     */
    public function getMinValueAddedTax()
    {
        return $this->minValueAddedTax;
    }

    /**
     * @param float $minValueAddedTax
     */
    public function setMinValueAddedTax(float $minValueAddedTax)
    {
        $this->minValueAddedTax = $minValueAddedTax;
    }

    /**
     * @return TaxTariff
     */
    public function getTaxTariff()
    {
        return $this->taxTariff;
    }

    /**
     * @param TaxTariff $taxTariff
     */
    public function setTaxTariff($taxTariff)
    {
        $this->taxTariff = $taxTariff;
    }

    /**
     * @return \Terminalbd\NbrvatBundle\Entity\Setting
     */
    public function getSupplyOutputTax()
    {
        return $this->supplyOutputTax;
    }

    /**
     * @param \Terminalbd\NbrvatBundle\Entity\Setting $supplyOutputTax
     */
    public function setSupplyOutputTax($supplyOutputTax)
    {
        $this->supplyOutputTax = $supplyOutputTax;
    }

    /**
     * @return Setting
     */
    public function getPurchaseInputTax()
    {
        return $this->purchaseInputTax;
    }

    /**
     * @param Setting $purchaseInputTax
     */
    public function setPurchaseInputTax($purchaseInputTax)
    {
        $this->purchaseInputTax = $purchaseInputTax;
    }

    /**
     * @return \Terminalbd\NbrvatBundle\Entity\Setting
     */
    public function getInputTax()
    {
        return $this->inputTax;
    }

    /**
     * @param \Terminalbd\NbrvatBundle\Entity\Setting $inputTax
     */
    public function setInputTax($inputTax)
    {
        $this->inputTax = $inputTax;
    }

    /**
     * @return mixed
     */
    public function getInputImportTax()
    {
        return $this->inputImportTax;
    }

    /**
     * @param mixed $inputImportTax
     */
    public function setInputImportTax($inputImportTax)
    {
        $this->inputImportTax = $inputImportTax;
    }




}

