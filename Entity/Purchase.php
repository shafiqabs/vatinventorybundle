<?php

namespace Terminalbd\InventoryBundle\Entity;

use App\Entity\Admin\Country;
use App\Entity\Application\Inventory;
use App\Entity\Core\Vendor;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Terminalbd\AccountingBundle\Entity\AccountBank;
use Terminalbd\NbrvatBundle\Entity\VdsCertificate;


/**
 * Purchase
 *
 * @ORM\Table(name="inv_purchase")
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\PurchaseRepository")
 * @Gedmo\Uploadable(filenameGenerator="SHA1", allowOverwrite=true, appendNumber=true)
 */
    class Purchase
    {
        /**
         * @ORM\Id
         * @ORM\Column(name="id", type="guid")
         * @ORM\GeneratedValue(strategy="UUID")
         */
        protected $id;

        /**
         * @ORM\ManyToOne(targetEntity="App\Entity\Application\Inventory")
         * @ORM\JoinColumn(onDelete="CASCADE")
         **/
        private $config;

        /**
         * @var $accountBank AccountBank
         * @ORM\ManyToOne(targetEntity="Terminalbd\AccountingBundle\Entity\AccountBank")
         **/
        private $accountBank;

        /**
         * @var $accountMobile AccountBank
         * @ORM\ManyToOne(targetEntity="Terminalbd\AccountingBundle\Entity\AccountBank")
         **/
        private $accountMobile;

         /**
         * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\InvoiceKeyValue", mappedBy="purchase")
         **/
        private $invoiceKeyValues;


        /**
         * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\PurchaseItem", mappedBy="purchase")
         **/
        private  $purchaseItems;

        /**
         * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\PurchaseReturn", mappedBy="purchase")
         **/
        private  $purchaseReturns;


        /**
         * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Setting")
         **/
        private  $customsHouse;

        /**
         * @ORM\OneToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\VdsCertificate", mappedBy="purchase")
         **/
        private $vdsCertifacte;

        /**
         * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Country",inversedBy="purchase")
         * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
         **/
        private  $country;


        /**
         * @ORM\ManyToOne(targetEntity="App\Entity\User")
         **/
        private  $createdBy;


        /**
         * @ORM\ManyToOne(targetEntity="App\Entity\User")
         **/
        private  $checkedBy;


        /**
         * @ORM\ManyToOne(targetEntity="App\Entity\User")
         **/
        private  $approvedBy;


        /**
         * @ORM\ManyToOne(targetEntity="App\Entity\Core\Vendor")
         **/
        private  $vendor;


        /**
         * @var float
         *
         * @ORM\Column(name="subTotal", type="float", nullable=true)
         */
        private $subTotal = 0;

        /**
         * @var float
         *
         * @ORM\Column(name="assesableValue", type="float", nullable=true)
         */
        private $assesableValue = 0;

        /**
         * @var float
         *
         * @ORM\Column(name="netTotal", type="float", nullable=true)
         */
        private $netTotal = 0;

        /**
         * @var float
         *
         * @ORM\Column(type="float", nullable=true)
         */
        private $taxableVat = 0;

        /**
         * @var float
         *
         * @ORM\Column(type="float", nullable=true)
         */
        private $taxableSd = 0;


        /**
         * @var float
         *
         * @ORM\Column(name="total", type="float", nullable=true)
         */
        private $total = 0;

        /**
         * @var float
         *
         * @ORM\Column(name="due", type="float", nullable=true)
         */
        private $due = 0;


        /**
         * @var float
         *
         * @ORM\Column(name="customsDuty", type="float", nullable=true)
         */
        private $customsDuty = 0.00;


        /**
         * @var float
         *
         * @ORM\Column(name="supplementaryDuty", type="float", nullable=true)
         */
        private $supplementaryDuty = 0.00;

        /**
         * @var float
         *
         * @ORM\Column(name="valueAddedTax", type="float", nullable=true)
         */
        private $valueAddedTax = 0.00;


        /**
         * @var float
         *
         * @ORM\Column(name="advanceIncomeTax", type="float", nullable=true)
         */
        private $advanceIncomeTax = 0.00;


        /**
         * @var float
         *
         * @ORM\Column(name="regulatoryDuty", type="float", nullable=true)
         */
        private $regulatoryDuty = 0.00;


        /**
         * @var float
         *
         * @ORM\Column(name="advanceTradeVat", type="float", nullable=true)
         */
        private $advanceTradeVat = 0.00;


         /**
         * @var float
         *
         * @ORM\Column(name="advanceTax", type="float", nullable=true)
         */
        private $advanceTax = 0.00;


         /**
         * @var float
         *
         * @ORM\Column(name="taxTariffCalculation", type="float", nullable=true)
         */
        private $taxTariffCalculation = 0.00;


        /**
         * @var float
         *
         * @ORM\Column(name="totalTaxIncidence", type="float", nullable=true)
         */
        private $totalTaxIncidence = 0.00;


        /**
         * @var float
         *
         * @ORM\Column(name="payment", type="float", nullable=true)
         */
        private $payment = 0;


        /**
         * @var float
         *
         * @ORM\Column(name="amount", type="float", nullable=true)
         */
        private $amount = 0;


        /**
         * @var float
         *
         * @ORM\Column(name="rebate", type="float", nullable=true)
         */
        private $rebate = 0;

         /**
         * @var float
         *
         * @ORM\Column(name="vatDeductionSource", type="float", nullable=true)
         */
        private $vatDeductionSource = 0;

         /**
         * @var float
         *
         * @ORM\Column(name="discount", type="float", nullable=true)
         */
        private $discount = 0;

         /**
         * @var float
         *
         * @ORM\Column(type="float", nullable=true)
         */
        private $discountCalculation = 0;

        /**
         * @var float
         *
         * @ORM\Column(type="float", nullable=true)
         */
        private $quantity = 0;

        /**
         * @var float
         *
         * @ORM\Column(type="float", nullable=true)
         */
        private $totalItem = 0;


        /**
         * @var string
         *
         * @ORM\Column(type="string", length = 50, nullable=true)
         */
        private $discountMode;

        /**
         * @var string
         *
         * @ORM\Column(type="string", length = 50, nullable=true)
         */
        private $transactionMethod;

        /**
         * @var string
         *
         * @ORM\Column(type="string", length = 50, nullable=true)
         */
        private $invoice;


        /**
         * @var string
         *
         * @ORM\Column(type="string", nullable=true)
         */
        private $billingAddress;

        /**
         * @var string
         *
         * @ORM\Column(type="string", nullable=true)
         */
        private $chequeNo;

         /**
         * @var string
         *
         * @ORM\Column(type="string", nullable=true)
         */
        private $chequeDate;

        /**
         * @var string
         *
         * @ORM\Column(type="string", nullable=true)
         */
        private $trxId;

        /**
         * @var string
         *
         * @ORM\Column(type="string", nullable=true)
         */
        private $trxDate;


         /**
         * @var string
         *
         * @ORM\Column(type="string", nullable=true)
         */
        private $shippingAddress;

        /**
         * @var string
         * @ORM\Column(type="string", length = 50, nullable=true)
         */
        private $billOfEntryNo;

        /**
         * @var \DateTime
         * @ORM\Column(type="datetime", nullable=true)
         */
        private $billOfEntryDate;


        /**
         * @var string
         *
         * @ORM\Column(name="challanNo", type="string", length = 50, nullable=true)
         */
        private $challanNo;

        /**
         * @var string
         *
         * @ORM\Column(name="mode", type="string", length = 50, nullable=true)
         */
        private $mode = "local";

         /**
         * @var string
         *
         * @ORM\Column(type="string", length = 50, nullable=true)
         */
        private $invoiceType = "product";


        /**
         * @var string
         *
         * @ORM\Column(name="assignPerson", type="string", length = 100, nullable=true)
         */
        private $assignPerson;

        /**
         * @var string
         *
         * @ORM\Column(name="assignPersonDesignation", type="string", length = 50, nullable=true)
         */
        private $assignPersonDesignation;


        /**
         * @var integer
         *
         * @ORM\Column(name="code", type="integer",  nullable=true)
         */
        private $code;

        /**
         * @var string
         *
         * @ORM\Column(type="text", nullable = true)
         */
        private $narration;

        /**
         * @var string
         *
         * @ORM\Column(type="text", nullable = true)
         */
        private $receiveAddress;

        /**
         * @var string
         *
         * @ORM\Column(type="text", length=50, nullable = true)
         */
        private $receiveTime;


        /**
         * @var string
         * @ORM\Column(name="lcNo", type="string", length = 50, nullable=true)
         */
        private $lcNo;

        /**
         * @var string
         * @ORM\Column(type="string", nullable=true)
         */
        private $vehicleInfo;

        /**
         * @var string
         * @ORM\Column(type="string", nullable=true)
         */
        private $clearForwardingFirm;


        /**
         * @var \DateTime
         * @ORM\Column(type="datetime", nullable=true)
         */
        private $lcDate;


        /**
         * @var \DateTime
         *
         * @ORM\Column(name="poDate", type="date", nullable = true)
         */
        private $poDate;


        /**
         * @var \DateTime
         * @Gedmo\Timestampable(on="create")
         * @ORM\Column(name="receiveDate", type="datetime")
         */
        private $receiveDate;



        /**
         * @var \DateTime
         * @Gedmo\Timestampable(on="create")
         * @ORM\Column(name="created", type="datetime")
         */
        private $created;

        /**
         * @var \DateTime
         * @ORM\Column(name="updated", type="datetime", nullable = true)
         */
        private $updated;


        /**
         * @var string
         *
         * @ORM\Column(name="process", type="string", length=50, nullable = true)
         */
        private $process;

        /**
         * @var boolean
         *
         * @ORM\Column(type="boolean", nullable = true)
         */
        private $reverse;


        /**
         * @var boolean
         *
         * @ORM\Column(type="boolean", nullable = true)
         */
        private $vdsApplicable;


        /**
         * @var \DateTime
         * @ORM\Column(type="datetime", nullable = true)
         */
        private $vdsGeneratedDate;


        /**
         * @var boolean
         *
         * @ORM\Column(type="boolean", nullable = true)
         */
        private $rebateable;


        /**
         * @var string
         *
         * @ORM\Column(name="processType", type="string", length=50, nullable = true)
         */
        private $processType;

        /**
         * @ORM\Column(name="path", type="string", nullable=true)
         * @Gedmo\UploadableFilePath
         */
        protected $path;

        /**
         * @Assert\File(maxSize="8388608")
         */
        protected $file;




        /**
         * Get id
         *
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }



        /**
         * @return \DateTime
         */
        public function getCreated()
        {
            return $this->created;
        }

        /**
         * @param \DateTime $created
         */
        public function setCreated($created)
        {
            $this->created = $created;
        }

        /**
         * @return \DateTime
         */
        public function getUpdated()
        {
            return $this->updated;
        }

        /**
         * @param \DateTime $updated
         */
        public function setUpdated($updated)
        {
            $this->updated = $updated;
        }




        /**
         * @return \DateTime
         */
        public function getReceiveDate()
        {
            return $this->receiveDate;
        }

        /**
         * @param \DateTime $receiveDate
         */
        public function setReceiveDate($receiveDate)
        {
            $this->receiveDate = $receiveDate;
        }

        /**
         * @return User
         */
        public function getCreatedBy()
        {
            return $this->createdBy;
        }

        /**
         * @param User $createdBy
         */
        public function setCreatedBy($createdBy)
        {
            $this->createdBy = $createdBy;
        }


        /**
         * @return User
         */
        public function getCheckedBy()
        {
            return $this->checkedBy;
        }

        /**
         * @param User $checkedBy
         */
        public function setCheckedBy($checkedBy)
        {
            $this->checkedBy = $checkedBy;
        }


        /**
         * @return User
         */
        public function getApprovedBy()
        {
            return $this->approvedBy;
        }

        /**
         * @param User $approvedBy
         */
        public function setApprovedBy($approvedBy)
        {
            $this->approvedBy = $approvedBy;
        }

        /**
         * @return string
         */
        public function getProcess()
        {
            return $this->process;
        }

        /**
         * @param string $process
         */
        public function setProcess($process)
        {
            $this->process = $process;
        }


        /**
         * Local
         * Foreign
         * Service
         * @return string
         */
        public function getProcessType()
        {
            return $this->processType;
        }

        /**
         * @param string $processType
         */
        public function setProcessType($processType)
        {
            $this->processType = $processType;
        }


        /**
         * @return float
         */
        public function getPayment()
        {
            return $this->payment;
        }

        /**
         * @param float $payment
         */
        public function setPayment($payment)
        {
            $this->payment = $payment;
        }



        /**
         * @return int
         */
        public function getCode()
        {
            return $this->code;
        }

        /**
         * @param int $code
         */
        public function setCode($code)
        {
            $this->code = $code;
        }


        /**
         * @return string
         */
        public function getTransactionMethod()
        {
            return $this->transactionMethod;
        }

        /**
         * @param string $transactionMethod
         */
        public function setTransactionMethod($transactionMethod)
        {
            $this->transactionMethod = $transactionMethod;
        }

        /**
         * @return AccountBank
         */
        public function getAccountBank()
        {
            return $this->accountBank;
        }

        /**
         * @param AccountBank $accountBank
         */
        public function setAccountBank($accountBank)
        {
            $this->accountBank = $accountBank;
        }

        /**
         * @return AccountBank
         */
        public function getAccountMobile()
        {
            return $this->accountMobile;
        }

        /**
         * @param AccountBank $accountMobile
         */
        public function setAccountMobile($accountMobile)
        {
            $this->accountMobile = $accountMobile;
        }


        /**
         * @return PurchaseItem
         */
        public function getPurchaseItems()
        {
            return $this->purchaseItems;
        }

        /**
         * @return float
         */
        public function getTotalTaxIncidence()
        {
            return $this->totalTaxIncidence;
        }

        /**
         * @param float $totalTaxIncidence
         */
        public function setTotalTaxIncidence($totalTaxIncidence)
        {
            $this->totalTaxIncidence = $totalTaxIncidence;
        }

        /**
         * @return float
         */
        public function getAdvanceTradeVat()
        {
            return $this->advanceTradeVat;
        }

        /**
         * @param float $advanceTradeVat
         */
        public function setAdvanceTradeVat($advanceTradeVat)
        {
            $this->advanceTradeVat = $advanceTradeVat;
        }

        /**
         * @return float
         */
        public function getRegulatoryDuty(): ? float
        {
            return $this->regulatoryDuty;
        }

        /**
         * @param float $regulatoryDuty
         */
        public function setRegulatoryDuty(float $regulatoryDuty)
        {
            $this->regulatoryDuty = $regulatoryDuty;
        }



        /**
         * @return float
         */
        public function getAdvanceIncomeTax()
        {
            return $this->advanceIncomeTax;
        }

        /**
         * @param float $advanceIncomeTax
         */
        public function setAdvanceIncomeTax($advanceIncomeTax)
        {
            $this->advanceIncomeTax = $advanceIncomeTax;
        }

        /**
         * @return float
         */
        public function getValueAddedTax()
        {
            return $this->valueAddedTax;
        }

        /**
         * @param float $valueAddedTax
         */
        public function setValueAddedTax($valueAddedTax)
        {
            $this->valueAddedTax = $valueAddedTax;
        }

        /**
         * @return float
         */
        public function getSupplementaryDuty()
        {
            return $this->supplementaryDuty;
        }

        /**
         * @param float $supplementaryDuty
         */
        public function setSupplementaryDuty($supplementaryDuty)
        {
            $this->supplementaryDuty = $supplementaryDuty;
        }

        /**
         * @return float
         */
        public function getCustomsDuty()
        {
            return $this->customsDuty;
        }

        /**
         * @param float $customsDuty
         */
        public function setCustomsDuty($customsDuty)
        {
            $this->customsDuty = $customsDuty;
        }

        /**
         * @return float
         */
        public function getAdvanceTax()
        {
            return $this->advanceTax;
        }

        /**
         * @param float $advanceTax
         */
        public function setAdvanceTax(float $advanceTax)
        {
            $this->advanceTax = $advanceTax;
        }



        /**
         * @return float
         */
        public function getTaxTariffCalculation(): ? float
        {
            return $this->taxTariffCalculation;
        }

        /**
         * @param float $taxTariffCalculation
         */
        public function setTaxTariffCalculation(float $taxTariffCalculation)
        {
            $this->taxTariffCalculation = $taxTariffCalculation;
        }



        /**
         * @return float
         */
        public function getNetTotal()
        {
            return $this->netTotal;
        }

        /**
         * @param float $netTotal
         */
        public function setNetTotal($netTotal)
        {
            $this->netTotal = $netTotal;
        }

        /**
         * @return float
         */
        public function getTotal()
        {
            return $this->total;
        }

        /**
         * @param float $total
         */
        public function setTotal(float $total)
        {
            $this->total = $total;
        }


        /**
         * @return float
         */
        public function getSubTotal()
        {
            return $this->subTotal;
        }

        /**
         * @param float $subTotal
         */
        public function setSubTotal($subTotal)
        {
            $this->subTotal = $subTotal;
        }

        /**
         * @return float
         */
        public function getRebate()
        {
            return $this->rebate;
        }

        /**
         * @param float $rebate
         */
        public function setRebate($rebate)
        {
            $this->rebate = $rebate;
        }

        /**
         * @return float
         */
        public function getVatDeductionSource()
        {
            return $this->vatDeductionSource;
        }

        /**
         * @param float $vatDeductionSource
         */
        public function setVatDeductionSource($vatDeductionSource)
        {
            $this->vatDeductionSource = $vatDeductionSource;
        }

        /**
         * @return float
         */
        public function getDiscount()
        {
            return $this->discount;
        }

        /**
         * @param float $discount
         */
        public function setDiscount($discount)
        {
            $this->discount = $discount;
        }


        /**
         * @return string
         */
        public function getChallanNo()
        {
            return $this->challanNo;
        }

        /**
         * @param string $challanNo
         */
        public function setChallanNo($challanNo)
        {
            $this->challanNo = $challanNo;
        }

        /**
         * @return string
         */
        public function getLcNo()
        {
            return $this->lcNo;
        }

        /**
         * @param string $lcNo
         */
        public function setLcNo($lcNo)
        {
            $this->lcNo = $lcNo;
        }

        /**
         * @return mixed
         */
        public function getPoDate()
        {
            return $this->poDate;
        }

        /**
         * @param mixed $poDate
         */
        public function setPoDate($poDate)
        {
            $this->poDate = $poDate;
        }

        /**
         * @return AccountPurchase
         */
        public function getAccountPurchase()
        {
            return $this->accountPurchase;
        }

        /**
         * @return PurchaseOrder
         */
        public function getPurchaseOrder()
        {
            return $this->purchaseOrder;
        }

        /**
         * @param PurchaseOrder $purchaseOrder
         */
        public function setPurchaseOrder($purchaseOrder)
        {
            $this->purchaseOrder = $purchaseOrder;
        }

        /**
         * @return Inventory
         */
        public function getConfig()
        {
            return $this->config;
        }

        /**
         * @param Inventory  $config
         */
        public function setConfig($config)
        {
            $this->config = $config;
        }

        /**
         * @return Vendor
         */
        public function getVendor()
        {
            return $this->vendor;
        }

        /**
         * @param Vendor $vendor
         */
        public function setVendor($vendor)
        {
            $this->vendor = $vendor;
        }

        /**
         * @return StockItem
         */
        public function getStockItems()
        {
            return $this->stockItems;
        }

        /**
         * @return string
         */
        public function getMode(): string
        {
            return $this->mode;
        }

        /**
         * @param string $mode
         */
        public function setMode(string $mode)
        {
            $this->mode = $mode;
        }

        /**
         * @return float
         */
        public function getDue(): ? float
        {
            return $this->due;
        }

        /**
         * @param float $due
         */
        public function setDue(float $due)
        {
            $this->due = $due;
        }

        /**
         * @return string
         */
        public function getDiscountMode()
        {
            return $this->discountMode;
        }

        /**
         * @param string $discountMode
         */
        public function setDiscountMode($discountMode)
        {
            $this->discountMode = $discountMode;
        }


        /**
         * @return string
         */
        public function getReceiveTime(): ? string
        {
            return $this->receiveTime;
        }

        /**
         * @param string $receiveTime
         */
        public function setReceiveTime(string $receiveTime)
        {
            $this->receiveTime = $receiveTime;
        }

        /**
         * Sets file.
         *
         * @param User $file
         */
        public function setFile(UploadedFile $file = null)
        {
            $this->file = $file;
        }

        /**
         * Get file.
         *
         * @return User
         */
        public function getFile()
        {
            return $this->file;
        }

        public function getAbsolutePath()
        {
            return null === $this->path
                ? null
                : $this->getUploadRootDir(). $this->path;
        }

        public function getWebPath()
        {
            return null === $this->path
                ? null
                : $this->getUploadDir().'/'.$this->path;
        }

        /**
         * @ORM\PostRemove()
         */
        public function removeUpload()
        {
            if ($file = $this->getAbsolutePath()) {
                unlink($file);
            }
        }

        protected function getUploadRootDir()
        {
            return WEB_ROOT .'/uploads/'.$this->getUploadDir();
        }

        protected function getUploadDir()
        {
            return 'user/';
        }

        public function upload()
        {
            // the file property can be empty if the field is not required
            if (null === $this->getFile()) {
                return;
            }

            $filename = date('YmdHmi') . "_" . $this->getFile()->getClientOriginalName();

            $this->getFile()->move(
                $this->getUploadRootDir(),
                $filename
            );
            // set the path property to the filename where you've saved the file
            $this->path = $filename;

            // clean up the file property as you won't need it anymore
            $this->file = null;
        }

        /**
         * @return string
         */
        public function getInvoice(): ? string
        {
            return $this->invoice;
        }

        /**
         * @param string $invoice
         */
        public function setInvoice(string $invoice)
        {
            $this->invoice = $invoice;
        }

        /**
         * @return string
         */
        public function getNarration(): ? string
        {
            return $this->narration;
        }

        /**
         * @param string $narration
         */
        public function setNarration(string $narration)
        {
            $this->narration = $narration;
        }

        /**
         * @return string
         */
        public function getReceiveAddress(): ? string
        {
            return $this->receiveAddress;
        }

        /**
         * @param string $receiveAddress
         */
        public function setReceiveAddress(string $receiveAddress)
        {
            $this->receiveAddress = $receiveAddress;
        }

        /**
         * @return float
         */
        public function getDiscountCalculation()
        {
            return $this->discountCalculation;
        }

        /**
         * @param float $discountCalculation
         */
        public function setDiscountCalculation($discountCalculation)
        {
            $this->discountCalculation = $discountCalculation;
        }

        /**
         * @return string
         */
        public function getAssignPerson(): ? string
        {
            return $this->assignPerson;
        }

        /**
         * @param string $assignPerson
         */
        public function setAssignPerson(string $assignPerson)
        {
            $this->assignPerson = $assignPerson;
        }

        /**
         * @return string
         */
        public function getAssignPersonDesignation(): ? string
        {
            return $this->assignPersonDesignation;
        }

        /**
         * @param string $assignPersonDesignation
         */
        public function setAssignPersonDesignation(string $assignPersonDesignation)
        {
            $this->assignPersonDesignation = $assignPersonDesignation;
        }

        /**
         * @return float
         */
        public function getAmount()
        {
            return $this->amount;
        }

        /**
         * @param float $amount
         */
        public function setAmount(float $amount)
        {
            $this->amount = $amount;
        }

        /**
         * @return InvoiceKeyValue
         */
        public function getInvoiceKeyValues()
        {
            return $this->invoiceKeyValues;
        }

        /**
         * @return string
         */
        public function getInvoiceType(): string
        {
            return $this->invoiceType;
        }

        /**
         * @param string $invoiceType
         */
        public function setInvoiceType(string $invoiceType)
        {
            $this->invoiceType = $invoiceType;
        }


        /**
         * @return Country
         */
        public function getCountry()
        {
            return $this->country;
        }

        /**
         * @param Country $country
         */
        public function setCountry($country)
        {
            $this->country = $country;
        }

        /**
         * @return string
         */
        public function getBillingAddress(): ? string
        {
            return $this->billingAddress;
        }

        /**
         * @param string $billingAddress
         */
        public function setBillingAddress(string $billingAddress)
        {
            $this->billingAddress = $billingAddress;
        }

        /**
         * @return string
         */
        public function getShippingAddress(): ? string
        {
            return $this->shippingAddress;
        }

        /**
         * @param string $shippingAddress
         */
        public function setShippingAddress(string $shippingAddress)
        {
            $this->shippingAddress = $shippingAddress;
        }

        /**
         * @return string
         */
        public function getVehicleInfo()
        {
            return $this->vehicleInfo;
        }

        /**
         * @param string $vehicleInfo
         */
        public function setVehicleInfo($vehicleInfo)
        {
            $this->vehicleInfo = $vehicleInfo;
        }

        /**
         * @return \DateTime
         */
        public function getLcDate()
        {
            return $this->lcDate;
        }

        /**
         * @param \DateTime $lcDate
         */
        public function setLcDate($lcDate)
        {
            $this->lcDate = $lcDate;
        }

        /**
         * @return PurchaseReturn
         */
        public function getPurchaseReturns()
        {
            return $this->purchaseReturns;
        }

        /**
         * @return string
         */
        public function getBillOfEntryNo(): ? string
        {
            return $this->billOfEntryNo;
        }

        /**
         * @param string $billOfEntryNo
         */
        public function setBillOfEntryNo(string $billOfEntryNo)
        {
            $this->billOfEntryNo = $billOfEntryNo;
        }

        /**
         * @return \DateTime
         */
        public function getBillOfEntryDate()
        {
            return $this->billOfEntryDate;
        }

        /**
         * @param \DateTime $billOfEntryDate
         */
        public function setBillOfEntryDate($billOfEntryDate)
        {
            $this->billOfEntryDate = $billOfEntryDate;
        }

        /**
         * @return mixed
         */
        public function getClearForwardingFirm()
        {
            return $this->clearForwardingFirm;
        }

        /**
         * @param mixed $clearForwardingFirm
         */
        public function setClearForwardingFirm($clearForwardingFirm)
        {
            $this->clearForwardingFirm = $clearForwardingFirm;
        }

        /**
         * @return Setting
         */
        public function getCustomsHouse()
        {
            return $this->customsHouse;
        }

        /**
         * @param Setting $customsHouse
         */
        public function setCustomsHouse($customsHouse)
        {
            $this->customsHouse = $customsHouse;
        }

        /**
         * @return float
         */
        public function getQuantity()
        {
            return $this->quantity;
        }

        /**
         * @param float $quantity
         */
        public function setQuantity($quantity)
        {
            $this->quantity = $quantity;
        }

        /**
         * @return float
         */
        public function getTotalItem()
        {
            return $this->totalItem;
        }

        /**
         * @param float $totalItem
         */
        public function setTotalItem($totalItem)
        {
            $this->totalItem = $totalItem;
        }

        /**
         * @return bool
         */
        public function isVdsApplicable()
        {
            return $this->vdsApplicable;
        }

        /**
         * @param bool $vdsApplicable
         */
        public function setVdsApplicable(bool $vdsApplicable)
        {
            $this->vdsApplicable = $vdsApplicable;
        }

        /**
         * @return b
         */
        public function getRebateable()
        {
            return $this->rebateable;
        }

        /**
         * @param boolean $rebateable
         */
        public function setRebateable($rebateable)
        {
            $this->rebateable = $rebateable;
        }

        /**
         * @return \DateTime
         */
        public function getVdsGeneratedDate()
        {
            return $this->vdsGeneratedDate;
        }

        /**
         * @param \DateTime $vdsGeneratedDate
         */
        public function setVdsGeneratedDate($vdsGeneratedDate)
        {
            $this->vdsGeneratedDate = $vdsGeneratedDate;
        }

        /**
         * @return string
         */
        public function getChequeNo()
        {
            return $this->chequeNo;
        }

        /**
         * @param string $chequeNo
         */
        public function setChequeNo(string $chequeNo)
        {
            $this->chequeNo = $chequeNo;
        }

        /**
         * @return string
         */
        public function getChequeDate()
        {
            return $this->chequeDate;
        }

        /**
         * @param string $chequeDate
         */
        public function setChequeDate(string $chequeDate)
        {
            $this->chequeDate = $chequeDate;
        }

        /**
         * @return string
         */
        public function getTrxId()
        {
            return $this->trxId;
        }

        /**
         * @param string $trxId
         */
        public function setTrxId(string $trxId)
        {
            $this->trxId = $trxId;
        }

        /**
         * @return string
         */
        public function getTrxDate()
        {
            return $this->trxDate;
        }

        /**
         * @param string $trxDate
         */
        public function setTrxDate(string $trxDate)
        {
            $this->trxDate = $trxDate;
        }

        /**
         * @return float
         */
        public function getTaxableVat()
        {
            return $this->taxableVat;
        }

        /**
         * @param float $taxableVat
         */
        public function setTaxableVat( $taxableVat)
        {
            $this->taxableVat = $taxableVat;
        }

        /**
         * @return float
         */
        public function getTaxableSd()
        {
            return $this->taxableSd;
        }

        /**
         * @param float $taxableSd
         */
        public function setTaxableSd($taxableSd)
        {
            $this->taxableSd = $taxableSd;
        }

        /**
         * @return VdsCertificate
         */
        public function getVdsCertifacte()
        {
            return $this->vdsCertifacte;
        }

        /**
         * @return float
         */
        public function getAssesableValue()
        {
            return $this->assesableValue;
        }

        /**
         * @param float $assesableValue
         */
        public function setAssesableValue(float $assesableValue)
        {
            $this->assesableValue = $assesableValue;
        }

        /**
         * @return bool
         */
        public function isReverse()
        {
            return $this->reverse;
        }

        /**
         * @param bool $reverse
         */
        public function setReverse($reverse)
        {
            $this->reverse = $reverse;
        }

        public function getVendorChallan()
        {
            $challan = $this->getVendor()->getCompanyName()." ( {$this->getInvoice()} ) - ".$this->getChallanNo();
            return $challan;
        }




    }

