<?php

namespace Terminalbd\InventoryBundle\Entity;


use App\Entity\Application\Inventory;
use App\Entity\Core\Vendor;
use Appstore\Bundle\ProcurementBundle\Entity\PurchaseOrderItem;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;



/**
 * VoucherItem
 *
 * @ORM\Table(name ="inv_purchase_item")
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\PurchaseItemRepository")
 */
class PurchaseItem
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Inventory")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private $config;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Item", inversedBy="purchaseItems" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $item;

     /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\MasterItem", inversedBy="purchaseItems" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $masterItem;

     /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Setting")
     **/
    private  $purchaseInputTax;

      /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\NbrvatBundle\Entity\Setting" )
     **/
    private  $nbrPurchaseInputTax;

     /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\Damage", mappedBy="purchaseItem" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $damages;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Purchase", inversedBy="purchaseItems" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $purchase;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Vendor")
     **/
    private  $vendor;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\WearHouse", inversedBy="purchaseItems" )
     **/
    private  $wearHouse;


    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\ItemMetaAttribute", mappedBy="purchaseItem" , cascade={"remove"}  )
     **/
    private  $itemMetaAttributes;

    /**
     * @var string
     *
     * @ORM\Column(name="assuranceType", type="string", length=50, nullable = true)
     */
    private $assuranceType;


    /**
     * @var datetime
     *
     * @ORM\Column(name="effectedDate", type="datetime", nullable=true)
     */
    private $effectedDate;

    /**
     * @var datetime
     *
     * @ORM\Column(name="expiredDate", type="datetime", nullable=true)
     */
    private $expiredDate;

    /**
     * @var array
     *
     * @ORM\Column(name="internalSerial", type="simple_array",  nullable = true)
     */
    private $internalSerial;

    /**
     * @var string
     *
     * @ORM\Column(name="externalSerial", type="text",  nullable = true)
     */
    private $externalSerial;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;


    /**
     * @var string
     *
     * @ORM\Column(name="uom", type="string", nullable=true)
     */
    private $uom;


    /**
     * @var string
     *
     * @ORM\Column(name="remark", type="text", nullable=true)
     */
    private $remark;


    /**
     * @var string
     *
     * @ORM\Column(name="mode", type="string", length=50, nullable=true)
     */
    private $mode = "purchase";

    /**
     * @var string
     *
     * @ORM\Column(name="purchaseMode", type="string", length=50, nullable=true)
     */
    private $purchaseMode = "local";

    /**
     * @var string
     *
     * @ORM\Column(name="process", type="string", length=50, nullable=true)
     */
    private $process = "In-progress";


    /**
     * @var float
     *
     * @ORM\Column(name="quantity", type="float",nullable=true)
     */
    private $quantity;


    /**
     * @var float
     *
     * @ORM\Column(name="salesQuantity", type="float",nullable=true)
     */
    private $salesQuantity;

    /**
     * @var float
     *
     * @ORM\Column(name="salesReturnQuantity", type="float",nullable=true)
     */
    private $salesReturnQuantity;

    /**
     * @var float
     *
     * @ORM\Column(name="salesReplaceQuantity", type="float",nullable=true)
     */
    private $salesReplaceQuantity;

    /**
     * @var float
     *
     * @ORM\Column(name="purchaseReturnQuantity", type="float",nullable=true)
     */
    private $purchaseReturnQuantity;

    /**
     * @var float
     *
     * @ORM\Column(name="damageQuantity", type="float",nullable=true)
     */
    private $damageQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float",nullable=true)
     */
    private $productionStockQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float",nullable=true)
     */
    private $productionStockReturnQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float",nullable=true)
     */
    private $productionExpenseQuantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float",nullable=true)
     */
    private $productionExpenseReturnQuantity;


    /**
     * @var float
     *
     * @ORM\Column(name="remainingQuantity", type="float",nullable=true)
     */
    private $remainingQuantity;


    /**
     * @var float
     *
     * @ORM\Column(name="purchasePrice", type="float", nullable = true)
     */
    private $purchasePrice;


    /**
     * @var float
     *
     * @ORM\Column(name="actualPurchasePrice", type="float", nullable = true)
     */
    private $actualPurchasePrice;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $actualSubTotal;


    /**
     * @var float
     *
     * @ORM\Column(name="avgPurchasePrice", type="float", nullable = true)
     */
    private $avgPurchasePrice;


    /**
     * @var float
     *
     * @ORM\Column(name="salesPrice", type="float", nullable = true)
     */
    private $salesPrice;


    /**
     * @var float
     *
     * @ORM\Column(name="length", type="float", nullable=true)
     */
    private $length;


    /**
     * @var float
     *
     * @ORM\Column(name="subQuantity", type="float", nullable=true)
     */
    private $subQuantity;

    /**
     * @var float
     *
     * @ORM\Column(name="totalQuantity", type="float", nullable=true)
     */
    private $totalQuantity;

    /**
     * @var float
     *
     * @ORM\Column(name="code", type="float", nullable = true)
     */
    private $code;

    /**
     * @var float
     *
     * @ORM\Column(name="bonusQuantity", type="float", nullable=true)
     */
    private $bonusQuantity = 0;


    /**
     * @var string
     *
     * @ORM\Column(name="barcode", type="string",  nullable = true)
     */
    private $barcode;


    /**
     * @var float
     *
     * @ORM\Column(name="height", type="float", nullable=true)
     */
    private $height;


    /**
     * @var float
     *
     * @ORM\Column(name="width", type="float", nullable=true)
     */
    private $width;


    /**
     * @var float
     *
     * @ORM\Column(name="customsDuty", type="float", nullable=true)
     */
    private $customsDuty = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="customsDutyPercent", type="float", nullable=true)
     */
    private $customsDutyPercent = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $vatDeductionSource = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $vatDeductionSourcePercent = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="supplementaryDuty", type="float", nullable=true)
     */
    private $supplementaryDuty = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="supplementaryDutyPercent", type="float", nullable=true)
     */
    private $supplementaryDutyPercent = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(name="valueAddedTax", type="float", nullable=true)
     */
    private $valueAddedTax = 0.00;


     /**
     * @var float
     *
     * @ORM\Column(name="valueAddedTaxPercent", type="float", nullable=true)
     */
    private $valueAddedTaxPercent = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="advanceIncomeTax", type="float", nullable=true)
     */
    private $advanceIncomeTax = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="advanceIncomeTaxPercent", type="float", nullable=true)
     */
    private $advanceIncomeTaxPercent = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="advanceTax", type="float", nullable=true)
     */
    private $advanceTax = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="advanceTaxPercent", type="float", nullable=true)
     */
    private $advanceTaxPercent = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="recurringDeposit", type="float", nullable=true)
     */
    private $recurringDeposit = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(name="recurringDepositPercent", type="float", nullable=true)
     */
    private $recurringDepositPercent = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="regulatoryDuty", type="float", nullable=true)
     */
    private $regulatoryDuty = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(name="regulatoryDutyPercent", type="float", nullable=true)
     */
    private $regulatoryDutyPercent = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="advanceTradeVat", type="float", nullable=true)
     */
    private $advanceTradeVat = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="advanceTradeVatPercent", type="float", nullable=true)
     */
    private $advanceTradeVatPercent = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="totalTaxIncidence", type="float", nullable=true)
     */
    private $totalTaxIncidence = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="rebatePercent", type="float", nullable=true)
     */
    private $rebatePercent = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="rebate", type="float", nullable=true)
     */
    private $rebate = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="subTotal", type="float", nullable = true)
     */
    private $subTotal;

    /**
     * @var float
     *
     * @ORM\Column(name="assesableValue", type="float", nullable=true)
     */
    private $assesableValue = 0;


    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float", nullable = true)
     */
    private $total;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $vdsApplicable;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $rebateApplicable = true;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }


    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return ItemWarning
     */
    public function getItemWarning() {
        return $this->itemWarning;
    }

    /**
     * @param ItemWarning $itemWarning
     */
    public function setItemWarning( $itemWarning ) {
        $this->itemWarning = $itemWarning;
    }

    /**
     * @return string
     */
    public function getAssuranceType() {
        return $this->assuranceType;
    }

    /**
     * @param string $assuranceType
     */
    public function setAssuranceType( $assuranceType ) {
        $this->assuranceType = $assuranceType;
    }

    /**
     * @return string
     */
    public function getExpiredDate() {
        return $this->expiredDate;
    }

    /**
     * @param string $expiredDate
     */
    public function setExpiredDate( $expiredDate ) {
        $this->expiredDate = $expiredDate;
    }

    /**
     * @return array
     */
    public function getInternalSerial() {
        return $this->internalSerial;
    }

    /**
     * @param array $internalSerial
     */
    public function setInternalSerial( $internalSerial ) {
        $this->internalSerial = $internalSerial;
    }

    /**
     * @return string
     */
    public function getExternalSerial() {
        return $this->externalSerial;
    }

    /**
     * @param string $externalSerial
     */
    public function setExternalSerial( $externalSerial ) {
        $this->externalSerial = $externalSerial;
    }

    /**
     * @return string
     */
    public function getEffectedDate() {
        return $this->effectedDate;
    }

    /**
     * @param string $effectedDate
     */
    public function setEffectedDate( $effectedDate ) {
        $this->effectedDate = $effectedDate;
    }

    /**
     * @return ItemMetaAttribute
     */
    public function getItemMetaAttributes() {
        return $this->itemMetaAttributes;
    }

    /**
     * @param ItemMetaAttribute $itemMetaAttributes
     */
    public function setItemMetaAttributes( $itemMetaAttributes ) {
        $this->itemMetaAttributes = $itemMetaAttributes;
    }

    /**
     * @return ItemKeyValue
     */
    public function getItemKeyValues() {
        return $this->itemKeyValues;
    }

    /**
     * @param ItemKeyValue $itemKeyValues
     */
    public function setItemKeyValues( $itemKeyValues ) {
        $this->itemKeyValues = $itemKeyValues;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
    }

    /**
     * @return DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * @return string
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;
    }


    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess($process)
    {
        $this->process = $process;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return PurchaseOrderItem
     */
    public function getPurchaseOrderItem()
    {
        return $this->purchaseOrderItem;
    }

    /**
     * @return WearHouse
     */
    public function getWearHouse()
    {
        return $this->wearHouse;
    }

    /**
     * @param WearHouse $wearHouse
     */
    public function setWearHouse($wearHouse)
    {
        $this->wearHouse = $wearHouse;
    }

    /**
     * @return float
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }

    /**
     * @param float $purchasePrice
     */
    public function setPurchasePrice($purchasePrice)
    {
        $this->purchasePrice = $purchasePrice;
    }

    /**
     * @return float
     */
    public function getSalesPrice()
    {
        return $this->salesPrice;
    }

    /**
     * @param float $salesPrice
     */
    public function setSalesPrice($salesPrice)
    {
        $this->salesPrice = $salesPrice;
    }

    /**
     * @return float
     */
    public function getCustomsDuty()
    {
        return $this->customsDuty;
    }

    /**
     * @param float $customsDuty
     */
    public function setCustomsDuty($customsDuty)
    {
        $this->customsDuty = $customsDuty;
    }

    /**
     * @return float
     */
    public function getSupplementaryDuty()
    {
        return $this->supplementaryDuty;
    }

    /**
     * @param float $supplementaryDuty
     */
    public function setSupplementaryDuty($supplementaryDuty)
    {
        $this->supplementaryDuty = $supplementaryDuty;
    }

    /**
     * @return float
     */
    public function getValueAddedTax()
    {
        return $this->valueAddedTax;
    }

    /**
     * @param float $valueAddedTax
     */
    public function setValueAddedTax($valueAddedTax)
    {
        $this->valueAddedTax = $valueAddedTax;
    }

    /**
     * @return float
     */
    public function getAdvanceIncomeTax()
    {
        return $this->advanceIncomeTax;
    }

    /**
     * @param float $advanceIncomeTax
     */
    public function setAdvanceIncomeTax($advanceIncomeTax)
    {
        $this->advanceIncomeTax = $advanceIncomeTax;
    }

    /**
     * @return float
     */
    public function getRecurringDeposit()
    {
        return $this->recurringDeposit;
    }

    /**
     * @param float $recurringDeposit
     */
    public function setRecurringDeposit($recurringDeposit)
    {
        $this->recurringDeposit = $recurringDeposit;
    }

    /**
     * @return float
     */
    public function getAdvanceTradeVat()
    {
        return $this->advanceTradeVat;
    }

    /**
     * @param float $advanceTradeVat
     */
    public function setAdvanceTradeVat($advanceTradeVat)
    {
        $this->advanceTradeVat = $advanceTradeVat;
    }

    /**
     * @return float
     */
    public function getTotalTaxIncidence()
    {
        return $this->totalTaxIncidence;
    }

    /**
     * @param float $totalTaxIncidence
     */
    public function setTotalTaxIncidence($totalTaxIncidence)
    {
        $this->totalTaxIncidence = $totalTaxIncidence;
    }

    /**
     * @return float
     */
    public function getAdvanceTradeVatPercent()
    {
        return $this->advanceTradeVatPercent;
    }

    /**
     * @param float $advanceTradeVatPercent
     */
    public function setAdvanceTradeVatPercent($advanceTradeVatPercent)
    {
        $this->advanceTradeVatPercent = $advanceTradeVatPercent;
    }

    /**
     * @return float
     */
    public function getRecurringDepositPercent()
    {
        return $this->recurringDepositPercent;
    }

    /**
     * @param float $recurringDepositPercent
     */
    public function setRecurringDepositPercent($recurringDepositPercent)
    {
        $this->recurringDepositPercent = $recurringDepositPercent;
    }

    /**
     * @return float
     */
    public function getAdvanceIncomeTaxPercent()
    {
        return $this->advanceIncomeTaxPercent;
    }

    /**
     * @param float $advanceIncomeTaxPercent
     */
    public function setAdvanceIncomeTaxPercent($advanceIncomeTaxPercent)
    {
        $this->advanceIncomeTaxPercent = $advanceIncomeTaxPercent;
    }

    /**
     * @return float
     */
    public function getValueAddedTaxPercent()
    {
        return $this->valueAddedTaxPercent;
    }

    /**
     * @param float $valueAddedTaxPercent
     */
    public function setValueAddedTaxPercent($valueAddedTaxPercent)
    {
        $this->valueAddedTaxPercent = $valueAddedTaxPercent;
    }

    /**
     * @return float
     */
    public function getSupplementaryDutyPercent()
    {
        return $this->supplementaryDutyPercent;
    }

    /**
     * @param float $supplementaryDutyPercent
     */
    public function setSupplementaryDutyPercent($supplementaryDutyPercent)
    {
        $this->supplementaryDutyPercent = $supplementaryDutyPercent;
    }

    /**
     * @return float
     */
    public function getCustomsDutyPercent()
    {
        return $this->customsDutyPercent;
    }

    /**
     * @param float $customsDutyPercent
     */
    public function setCustomsDutyPercent($customsDutyPercent)
    {
        $this->customsDutyPercent = $customsDutyPercent;
    }

    /**
     * @return float
     */
    public function getAdvanceTax(): ? float
    {
        return $this->advanceTax;
    }

    /**
     * @param float $advanceTax
     */
    public function setAdvanceTax(float $advanceTax)
    {
        $this->advanceTax = $advanceTax;
    }

    /**
     * @return float
     */
    public function getAdvanceTaxPercent(): ? float
    {
        return $this->advanceTaxPercent;
    }

    /**
     * @param float $advanceTaxPercent
     */
    public function setAdvanceTaxPercent(float $advanceTaxPercent)
    {
        $this->advanceTaxPercent = $advanceTaxPercent;
    }

    /**
     * @return float
     */
    public function getRegulatoryDuty(): ? float
    {
        return $this->regulatoryDuty;
    }

    /**
     * @param float $regulatoryDuty
     */
    public function setRegulatoryDuty(float $regulatoryDuty)
    {
        $this->regulatoryDuty = $regulatoryDuty;
    }

    /**
     * @return float
     */
    public function getRegulatoryDutyPercent(): ? float
    {
        return $this->regulatoryDutyPercent;
    }

    /**
     * @param float $regulatoryDutyPercent
     */
    public function setRegulatoryDutyPercent(float $regulatoryDutyPercent)
    {
        $this->regulatoryDutyPercent = $regulatoryDutyPercent;
    }



    /**
     * @return Purchase
     */
    public function getPurchase()
    {
        return $this->purchase;
    }

    /**
     * @param Purchase $purchase
     */
    public function setPurchase($purchase)
    {
        $this->purchase = $purchase;
    }

    /**
     * @return float
     */
    public function getRebate()
    {
        return $this->rebate;
    }

    /**
     * @param float $rebate
     */
    public function setRebate($rebate)
    {
        $this->rebate = $rebate;
    }

    /**
     * @return float
     */
    public function getRebatePercent()
    {
        return $this->rebatePercent;
    }

    /**
     * @param float $rebatePercent
     */
    public function setRebatePercent($rebatePercent)
    {
        $this->rebatePercent = $rebatePercent;
    }

    /**
     * @return Inventory
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Inventory $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return StockItem
     */
    public function getStockItems()
    {
        return $this->stockItems;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return ItemWarning
     */
    public function getAssuranceFromVendor()
    {
        return $this->assuranceFromVendor;
    }

    /**
     * @param ItemWarning $assuranceFromVendor
     */
    public function setAssuranceFromVendor($assuranceFromVendor)
    {
        $this->assuranceFromVendor = $assuranceFromVendor;
    }

    /**
     * @return ItemWarning
     */
    public function getAssuranceToCustomer()
    {
        return $this->assuranceToCustomer;
    }

    /**
     * @param ItemWarning $assuranceToCustomer
     */
    public function setAssuranceToCustomer($assuranceToCustomer)
    {
        $this->assuranceToCustomer = $assuranceToCustomer;
    }


    /**
     * @return string
     */
    public function getUserID(): ? string
    {
        return $this->userID;
    }

    /**
     * @param string $userID
     */
    public function setUserID(string $userID)
    {
        $this->userID = $userID;
    }

    /**
     * @return float
     */
    public function getActualPurchasePrice()
    {
        return $this->actualPurchasePrice;
    }

    /**
     * @param float $actualPurchasePrice
     */
    public function setActualPurchasePrice(float $actualPurchasePrice)
    {
        $this->actualPurchasePrice = $actualPurchasePrice;
    }

    /**
     * @return float
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param float $length
     */
    public function setLength(float $length)
    {
        $this->length = $length;
    }

    /**
     * @return float
     */
    public function getSubQuantity()
    {
        return $this->subQuantity;
    }

    /**
     * @param float $subQuantity
     */
    public function setSubQuantity(float $subQuantity)
    {
        $this->subQuantity = $subQuantity;
    }

    /**
     * @return float
     */
    public function getTotalQuantity()
    {
        return $this->totalQuantity;
    }

    /**
     * @param float $totalQuantity
     */
    public function setTotalQuantity(float $totalQuantity)
    {
        $this->totalQuantity = $totalQuantity;
    }

    /**
     * @return int
     */
    public function getBonusQuantity(): int
    {
        return $this->bonusQuantity;
    }

    /**
     * @param int $bonusQuantity
     */
    public function setBonusQuantity(int $bonusQuantity)
    {
        $this->bonusQuantity = $bonusQuantity;
    }

    /**
     * @return float
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param float $height
     */
    public function setHeight(float $height)
    {
        $this->height = $height;
    }

    /**
     * @return float
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param float $width
     */
    public function setWidth(float $width)
    {
        $this->width = $width;
    }


    /**
     * @param Damage $damages
     */
    public function setDamages($damages)
    {
        $this->damages = $damages;
    }

    /**
     * @return string
     */
    public function getUom(): ? string
    {
        return $this->uom;
    }

    /**
     * @param string $uom
     */
    public function setUom(string $uom)
    {
        $this->uom = $uom;
    }

    /**
     * @return MasterItem
     */
    public function getMasterItem()
    {
        return $this->masterItem;
    }

    /**
     * @param MasterItem $masterItem
     */
    public function setMasterItem($masterItem)
    {
        $this->masterItem = $masterItem;
    }

    /**
     * @return float
     */
    public function getVatDeductionSource(): ? float
    {
        return $this->vatDeductionSource;
    }

    /**
     * @param float $vatDeductionSource
     */
    public function setVatDeductionSource(float $vatDeductionSource)
    {
        $this->vatDeductionSource = $vatDeductionSource;
    }

    /**
     * @return float
     */
    public function getVatDeductionSourcePercent(): ? float
    {
        return $this->vatDeductionSourcePercent;
    }

    /**
     * @param float $vatDeductionSourcePercent
     */
    public function setVatDeductionSourcePercent(float $vatDeductionSourcePercent)
    {
        $this->vatDeductionSourcePercent = $vatDeductionSourcePercent;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getSalesQuantity()
    {
        return $this->salesQuantity;
    }

    /**
     * @param float $salesQuantity
     */
    public function setSalesQuantity(float $salesQuantity)
    {
        $this->salesQuantity = $salesQuantity;
    }

    /**
     * @return float
     */
    public function getSalesReturnQuantity()
    {
        return $this->salesReturnQuantity;
    }

    /**
     * @param float $salesReturnQuantity
     */
    public function setSalesReturnQuantity(float $salesReturnQuantity)
    {
        $this->salesReturnQuantity = $salesReturnQuantity;
    }

    /**
     * @return float
     */
    public function getSalesReplaceQuantity()
    {
        return $this->salesReplaceQuantity;
    }

    /**
     * @param float $salesReplaceQuantity
     */
    public function setSalesReplaceQuantity(float $salesReplaceQuantity)
    {
        $this->salesReplaceQuantity = $salesReplaceQuantity;
    }

    /**
     * @return float
     */
    public function getPurchaseReturnQuantity()
    {
        return $this->purchaseReturnQuantity;
    }

    /**
     * @param float $purchaseReturnQuantity
     */
    public function setPurchaseReturnQuantity(float $purchaseReturnQuantity)
    {
        $this->purchaseReturnQuantity = $purchaseReturnQuantity;
    }

    /**
     * @return float
     */
    public function getDamageQuantity()
    {
        return $this->damageQuantity;
    }

    /**
     * @param float $damageQuantity
     */
    public function setDamageQuantity(float $damageQuantity)
    {
        $this->damageQuantity = $damageQuantity;
    }

    /**
     * @return float
     */
    public function getProductionStockQuantity()
    {
        return $this->productionStockQuantity;
    }

    /**
     * @param float $productionStockQuantity
     */
    public function setProductionStockQuantity(float $productionStockQuantity)
    {
        $this->productionStockQuantity = $productionStockQuantity;
    }

    /**
     * @return float
     */
    public function getProductionStockReturnQuantity()
    {
        return $this->productionStockReturnQuantity;
    }

    /**
     * @param float $productionStockReturnQuantity
     */
    public function setProductionStockReturnQuantity(float $productionStockReturnQuantity)
    {
        $this->productionStockReturnQuantity = $productionStockReturnQuantity;
    }

    /**
     * @return float
     */
    public function getRemainingQuantity()
    {
        return $this->remainingQuantity;
    }

    /**
     * @param float $remainingQuantity
     */
    public function setRemainingQuantity(float $remainingQuantity)
    {
        $this->remainingQuantity = $remainingQuantity;
    }

    /**
     * @return float
     */
    public function getProductionExpenseQuantity()
    {
        return $this->productionExpenseQuantity;
    }

    /**
     * @param float $productionExpenseQuantity
     */
    public function setProductionExpenseQuantity(float $productionExpenseQuantity)
    {
        $this->productionExpenseQuantity = $productionExpenseQuantity;
    }

    /**
     * @return float
     */
    public function getProductionExpenseReturnQuantity()
    {
        return $this->productionExpenseReturnQuantity;
    }

    /**
     * @param float $productionExpenseReturnQuantity
     */
    public function setProductionExpenseReturnQuantity(float $productionExpenseReturnQuantity)
    {
        $this->productionExpenseReturnQuantity = $productionExpenseReturnQuantity;
    }

    /**
     * @return Setting
     */
    public function getPurchaseInputTax()
    {
        return $this->purchaseInputTax;
    }

    /**
     * @param Setting $purchaseInputTax
     */
    public function setPurchaseInputTax($purchaseInputTax)
    {
        $this->purchaseInputTax = $purchaseInputTax;
    }

    /**
     * @return Vendor
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param Vendor $vendor
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
    }

    /**
     * @return string
     */
    public function getPurchaseMode()
    {
        return $this->purchaseMode;
    }

    /**
     * @param string $purchaseMode
     */
    public function setPurchaseMode(string $purchaseMode)
    {
        $this->purchaseMode = $purchaseMode;
    }

    /**
     * @return bool
     */
    public function isVdsApplicable()
    {
        return $this->vdsApplicable;
    }

    /**
     * @param bool $vdsApplicable
     */
    public function setVdsApplicable($vdsApplicable)
    {
        $this->vdsApplicable = $vdsApplicable;
    }

    /**
     * @return bool
     */
    public function isRebateApplicable()
    {
        return $this->rebateApplicable;
    }

    /**
     * @param bool $rebateApplicable
     */
    public function setRebateApplicable($rebateApplicable)
    {
        $this->rebateApplicable = $rebateApplicable;
    }

    /**
     * @return \Terminalbd\NbrvatBundle\Entity\Setting
     */
    public function getNbrPurchaseInputTax()
    {
        return $this->nbrPurchaseInputTax;
    }

    /**
     * @param \Terminalbd\NbrvatBundle\Entity\Setting $nbrPurchaseInputTax
     */
    public function setNbrPurchaseInputTax($nbrPurchaseInputTax)
    {
        $this->nbrPurchaseInputTax = $nbrPurchaseInputTax;
    }

    /**
     * @return float
     */
    public function getAssesableValue()
    {
        return $this->assesableValue;
    }

    /**
     * @param float $assesableValue
     */
    public function setAssesableValue(float $assesableValue)
    {
        $this->assesableValue = $assesableValue;
    }

    /**
     * @return float
     */
    public function getActualSubTotal()
    {
        return $this->actualSubTotal;
    }

    /**
     * @param float $actualSubTotal
     */
    public function setActualSubTotal(float $actualSubTotal)
    {
        $this->actualSubTotal = $actualSubTotal;
    }

    /**
     * @return float
     */
    public function getAvgPurchasePrice()
    {
        return $this->avgPurchasePrice;
    }

    /**
     * @param float $avgPurchasePrice
     */
    public function setAvgPurchasePrice($avgPurchasePrice)
    {
        $this->avgPurchasePrice = $avgPurchasePrice;
    }



}

