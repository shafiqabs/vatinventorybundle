<?php

namespace Terminalbd\InventoryBundle\Entity;


use App\Entity\Application\Inventory;
use Appstore\Bundle\ProcurementBundle\Entity\PurchaseOrderItem;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\ProductionBundle\Entity\ProductionBatchItem;
use Terminalbd\ProductionBundle\Entity\ProductionElement;

/**
 * SalesItem
 *
 * @ORM\Table(name ="inv_sales_item_export_tax")
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\SalesItemTaxRepository")
 */
class SalesItemTax
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Item")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $item;


     /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\SalesItem")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $salesItem;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionItem")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $productionItem;


     /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionElement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $productionElement;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\PurchaseItem")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $purchaseItem;

    /**
     * @var string
     *
     * @ORM\Column(name="mode", type="string", length=50, nullable=true)
     */
    private $mode = "sales";


    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", nullable = true)
     */
    private $price;


    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $quantity;


    /**
     * @var float
     *
     * @ORM\Column(name="supplementaryDuty", type="float", nullable=true)
     */
    private $supplementaryDuty = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(name="supplementaryDutyPercent", type="float", nullable=true)
     */
    private $supplementaryDutyPercent = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(name="valueAddedTax", type="float", nullable=true)
     */
    private $valueAddedTax = 0.00;


     /**
     * @var float
     *
     * @ORM\Column(name="vatRefundForSales", type="float", nullable=true)
     */
    private $vatRefundForSales = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="valueAddedTaxPercent", type="float", nullable=true)
     */
    private $valueAddedTaxPercent = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="advanceTax", type="float", nullable=true)
     */
    private $advanceTax = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="advanceTaxPercent", type="float", nullable=true)
     */
    private $advanceTaxPercent = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="subTotal", type="float", nullable = true)
     */
    private $subTotal;


    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float", nullable = true)
     */
    private $total;



    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return mixed
     */
    public function getProductionItem()
    {
        return $this->productionItem;
    }

    /**
     * @param mixed $productionItem
     */
    public function setProductionItem($productionItem)
    {
        $this->productionItem = $productionItem;
    }

    /**
     * @return mixed
     */
    public function getPurchaseItem()
    {
        return $this->purchaseItem;
    }

    /**
     * @param mixed $purchaseItem
     */
    public function setPurchaseItem($purchaseItem)
    {
        $this->purchaseItem = $purchaseItem;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode(string $mode)
    {
        $this->mode = $mode;
    }


    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity(float $quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getSupplementaryDuty()
    {
        return $this->supplementaryDuty;
    }

    /**
     * @param float $supplementaryDuty
     */
    public function setSupplementaryDuty(float $supplementaryDuty)
    {
        $this->supplementaryDuty = $supplementaryDuty;
    }

    /**
     * @return float
     */
    public function getSupplementaryDutyPercent()
    {
        return $this->supplementaryDutyPercent;
    }

    /**
     * @param float $supplementaryDutyPercent
     */
    public function setSupplementaryDutyPercent(float $supplementaryDutyPercent)
    {
        $this->supplementaryDutyPercent = $supplementaryDutyPercent;
    }

    /**
     * @return float
     */
    public function getValueAddedTax()
    {
        return $this->valueAddedTax;
    }

    /**
     * @param float $valueAddedTax
     */
    public function setValueAddedTax(float $valueAddedTax)
    {
        $this->valueAddedTax = $valueAddedTax;
    }

    /**
     * @return float
     */
    public function getValueAddedTaxPercent()
    {
        return $this->valueAddedTaxPercent;
    }

    /**
     * @param float $valueAddedTaxPercent
     */
    public function setValueAddedTaxPercent(float $valueAddedTaxPercent)
    {
        $this->valueAddedTaxPercent = $valueAddedTaxPercent;
    }

    /**
     * @return float
     */
    public function getAdvanceTax()
    {
        return $this->advanceTax;
    }

    /**
     * @param float $advanceTax
     */
    public function setAdvanceTax(float $advanceTax)
    {
        $this->advanceTax = $advanceTax;
    }

    /**
     * @return float
     */
    public function getAdvanceTaxPercent()
    {
        return $this->advanceTaxPercent;
    }

    /**
     * @param float $advanceTaxPercent
     */
    public function setAdvanceTaxPercent(float $advanceTaxPercent)
    {
        $this->advanceTaxPercent = $advanceTaxPercent;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal(float $subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal(float $total)
    {
        $this->total = $total;
    }

    /**
     * @return DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param DateTime $created
     */
    public function setCreated(DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param DateTime $updated
     */
    public function setUpdated(DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    /**
     * @return ProductionElement
     */
    public function getProductionElement()
    {
        return $this->productionElement;
    }

    /**
     * @param ProductionElement $productionElement
     */

    public function setProductionElement($productionElement)
    {
        $this->productionElement = $productionElement;
    }

    /**
     * @return SalesItem
     */
    public function getSalesItem()
    {
        return $this->salesItem;
    }

    /**
     * @param SalesItem $salesItem
     */
    public function setSalesItem($salesItem)
    {
        $this->salesItem = $salesItem;
    }

    /**
     * @return float
     */
    public function getVatRefundForSales()
    {
        return $this->vatRefundForSales;
    }

    /**
     * @param float $vatRefundForSales
     */
    public function setVatRefundForSales($vatRefundForSales)
    {
        $this->vatRefundForSales = $vatRefundForSales;
    }




}

