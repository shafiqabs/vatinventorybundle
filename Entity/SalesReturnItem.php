<?php

namespace Terminalbd\InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Terminalbd\ProductionBundle\Entity\ProductionBatchItem;

/**
 * SalesReturnItem
 *
 * @ORM\Table(name="inv_sales_return_item")
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\SalesReturnItemRepository")
 */
class SalesReturnItem
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\SalesReturn", inversedBy="salesReturnItems" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $salesReturn;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\SalesItem", inversedBy="salesReturnItem" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $salesItem;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Item", inversedBy="salesReturnItem" )
     **/
    private  $item;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\StockItem", mappedBy="salesReturnItem" )
     **/
    private  $stockReturnItems;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProductionBundle\Entity\ProductionBatchItem")
     **/
    private  $productionBatchItem;


    /**
     * @var float
     *
     * @ORM\Column(name="customsDuty", type="float", nullable=true)
     */
    private $customsDuty = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="customsDutyPercent", type="float", nullable=true)
     */
    private $customsDutyPercent = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $vatDeductionSource = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $vatDeductionSourcePercent = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="supplementaryDuty", type="float", nullable=true)
     */
    private $supplementaryDuty = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(name="supplementaryDutyPercent", type="float", nullable=true)
     */
    private $supplementaryDutyPercent = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(name="valueAddedTax", type="float", nullable=true)
     */
    private $valueAddedTax = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="valueAddedTaxPercent", type="float", nullable=true)
     */
    private $valueAddedTaxPercent = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="advanceIncomeTax", type="float", nullable=true)
     */
    private $advanceIncomeTax = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="advanceIncomeTaxPercent", type="float", nullable=true)
     */
    private $advanceIncomeTaxPercent = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="advanceTax", type="float", nullable=true)
     */
    private $advanceTax = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="advanceTaxPercent", type="float", nullable=true)
     */
    private $advanceTaxPercent = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="recurringDeposit", type="float", nullable=true)
     */
    private $recurringDeposit = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(name="recurringDepositPercent", type="float", nullable=true)
     */
    private $recurringDepositPercent = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="regulatoryDuty", type="float", nullable=true)
     */
    private $regulatoryDuty = 0.00;

    /**
     * @var float
     *
     * @ORM\Column(name="regulatoryDutyPercent", type="float", nullable=true)
     */
    private $regulatoryDutyPercent = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="advanceTradeVat", type="float", nullable=true)
     */
    private $advanceTradeVat = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="advanceTradeVatPercent", type="float", nullable=true)
     */
    private $advanceTradeVatPercent = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="totalTaxIncidence", type="float", nullable=true)
     */
    private $totalTaxIncidence = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="rebatePercent", type="float", nullable=true)
     */
    private $rebatePercent = 0.00;


    /**
     * @var float
     *
     * @ORM\Column(name="rebate", type="float", nullable=true)
     */
    private $rebate = 0.00;



    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", length=5)
     */
    private $quantity;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="subTotal", type="float")
     */
    private $subTotal;


     /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return SalesReturn
     */
    public function getSalesReturn()
    {
        return $this->salesReturn;
    }

    /**
     * @param SalesReturn $salesReturn
     */
    public function setSalesReturn($salesReturn)
    {
        $this->salesReturn = $salesReturn;
    }

    /**
     * @return SalesItem
     */
    public function getSalesItem()
    {
        return $this->salesItem;
    }

    /**
     * @param SalesItem $salesItem
     */
    public function setSalesItem($salesItem)
    {
        $this->salesItem = $salesItem;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
    }


    /**
     * @return float
     */
    public function getCustomsDuty()
    {
        return $this->customsDuty;
    }

    /**
     * @param float $customsDuty
     */
    public function setCustomsDuty($customsDuty)
    {
        $this->customsDuty = $customsDuty;
    }

    /**
     * @return float
     */
    public function getSupplementaryDuty()
    {
        return $this->supplementaryDuty;
    }

    /**
     * @param float $supplementaryDuty
     */
    public function setSupplementaryDuty($supplementaryDuty)
    {
        $this->supplementaryDuty = $supplementaryDuty;
    }

    /**
     * @return float
     */
    public function getValueAddedTax()
    {
        return $this->valueAddedTax;
    }

    /**
     * @param float $valueAddedTax
     */
    public function setValueAddedTax($valueAddedTax)
    {
        $this->valueAddedTax = $valueAddedTax;
    }

    /**
     * @return float
     */
    public function getAdvanceIncomeTax()
    {
        return $this->advanceIncomeTax;
    }

    /**
     * @param float $advanceIncomeTax
     */
    public function setAdvanceIncomeTax($advanceIncomeTax)
    {
        $this->advanceIncomeTax = $advanceIncomeTax;
    }

    /**
     * @return float
     */
    public function getRecurringDeposit()
    {
        return $this->recurringDeposit;
    }

    /**
     * @param float $recurringDeposit
     */
    public function setRecurringDeposit($recurringDeposit)
    {
        $this->recurringDeposit = $recurringDeposit;
    }

    /**
     * @return float
     */
    public function getAdvanceTradeVat()
    {
        return $this->advanceTradeVat;
    }

    /**
     * @param float $advanceTradeVat
     */
    public function setAdvanceTradeVat($advanceTradeVat)
    {
        $this->advanceTradeVat = $advanceTradeVat;
    }

    /**
     * @return float
     */
    public function getTotalTaxIncidence()
    {
        return $this->totalTaxIncidence;
    }

    /**
     * @param float $totalTaxIncidence
     */
    public function setTotalTaxIncidence($totalTaxIncidence)
    {
        $this->totalTaxIncidence = $totalTaxIncidence;
    }

    /**
     * @return float
     */
    public function getAdvanceTradeVatPercent()
    {
        return $this->advanceTradeVatPercent;
    }

    /**
     * @param float $advanceTradeVatPercent
     */
    public function setAdvanceTradeVatPercent($advanceTradeVatPercent)
    {
        $this->advanceTradeVatPercent = $advanceTradeVatPercent;
    }

    /**
     * @return float
     */
    public function getRecurringDepositPercent()
    {
        return $this->recurringDepositPercent;
    }

    /**
     * @param float $recurringDepositPercent
     */
    public function setRecurringDepositPercent($recurringDepositPercent)
    {
        $this->recurringDepositPercent = $recurringDepositPercent;
    }

    /**
     * @return float
     */
    public function getAdvanceIncomeTaxPercent()
    {
        return $this->advanceIncomeTaxPercent;
    }

    /**
     * @param float $advanceIncomeTaxPercent
     */
    public function setAdvanceIncomeTaxPercent($advanceIncomeTaxPercent)
    {
        $this->advanceIncomeTaxPercent = $advanceIncomeTaxPercent;
    }

    /**
     * @return float
     */
    public function getValueAddedTaxPercent()
    {
        return $this->valueAddedTaxPercent;
    }

    /**
     * @param float $valueAddedTaxPercent
     */
    public function setValueAddedTaxPercent($valueAddedTaxPercent)
    {
        $this->valueAddedTaxPercent = $valueAddedTaxPercent;
    }

    /**
     * @return float
     */
    public function getSupplementaryDutyPercent()
    {
        return $this->supplementaryDutyPercent;
    }

    /**
     * @param float $supplementaryDutyPercent
     */
    public function setSupplementaryDutyPercent($supplementaryDutyPercent)
    {
        $this->supplementaryDutyPercent = $supplementaryDutyPercent;
    }

    /**
     * @return float
     */
    public function getCustomsDutyPercent()
    {
        return $this->customsDutyPercent;
    }

    /**
     * @param float $customsDutyPercent
     */
    public function setCustomsDutyPercent($customsDutyPercent)
    {
        $this->customsDutyPercent = $customsDutyPercent;
    }

    /**
     * @return float
     */
    public function getAdvanceTax()
    {
        return $this->advanceTax;
    }

    /**
     * @param float $advanceTax
     */
    public function setAdvanceTax(float $advanceTax)
    {
        $this->advanceTax = $advanceTax;
    }

    /**
     * @return float
     */
    public function getAdvanceTaxPercent()
    {
        return $this->advanceTaxPercent;
    }

    /**
     * @param float $advanceTaxPercent
     */
    public function setAdvanceTaxPercent(float $advanceTaxPercent)
    {
        $this->advanceTaxPercent = $advanceTaxPercent;
    }

    /**
     * @return float
     */
    public function getRegulatoryDuty()
    {
        return $this->regulatoryDuty;
    }

    /**
     * @param float $regulatoryDuty
     */
    public function setRegulatoryDuty(float $regulatoryDuty)
    {
        $this->regulatoryDuty = $regulatoryDuty;
    }

    /**
     * @return float
     */
    public function getRegulatoryDutyPercent()
    {
        return $this->regulatoryDutyPercent;
    }

    /**
     * @param float $regulatoryDutyPercent
     */
    public function setRegulatoryDutyPercent(float $regulatoryDutyPercent)
    {
        $this->regulatoryDutyPercent = $regulatoryDutyPercent;
    }

    /**
     * @return float
     */
    public function getRebate()
    {
        return $this->rebate;
    }

    /**
     * @param float $rebate
     */
    public function setRebate($rebate)
    {
        $this->rebate = $rebate;
    }

    /**
     * @return float
     */
    public function getRebatePercent()
    {
        return $this->rebatePercent;
    }

    /**
     * @param float $rebatePercent
     */
    public function setRebatePercent($rebatePercent)
    {
        $this->rebatePercent = $rebatePercent;
    }

    /**
     * @return float
     */
    public function getVatDeductionSource()
    {
        return $this->vatDeductionSource;
    }

    /**
     * @param float $vatDeductionSource
     */
    public function setVatDeductionSource(float $vatDeductionSource)
    {
        $this->vatDeductionSource = $vatDeductionSource;
    }

    /**
     * @return float
     */
    public function getVatDeductionSourcePercent()
    {
        return $this->vatDeductionSourcePercent;
    }

    /**
     * @param float $vatDeductionSourcePercent
     */
    public function setVatDeductionSourcePercent(float $vatDeductionSourcePercent)
    {
        $this->vatDeductionSourcePercent = $vatDeductionSourcePercent;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return StockItem
     */
    public function getStockReturnItems()
    {
        return $this->stockReturnItems;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return ProductionBatchItem
     */
    public function getProductionBatchItem()
    {
        return $this->productionBatchItem;
    }

    /**
     * @param ProductionBatchItem $productionBatchItem
     */
    public function setProductionBatchItem($productionBatchItem)
    {
        $this->productionBatchItem = $productionBatchItem;
    }


}

