<?php

namespace Terminalbd\InventoryBundle\Entity;

use App\Entity\Admin\SettingType;
use App\Entity\Application\Inventory;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * BusinessParticular
 *
 * @ORM\Table( name = "inv_setting")
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\SettingRepository")
 */
class Setting
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Inventory")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private $config;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\SettingType")
     **/
    private $settingType;


    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\Item", mappedBy="productGroup" )
     **/
    private $items;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\Item", mappedBy="brand" )
     **/
    private $brandItems;


     /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\Item", mappedBy="productType" )
     **/
    private $typeItems;


     /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\MasterItem", mappedBy="supplyOutputTax" )
     **/
    private $supplyOutputTaxItems;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\ItemKeyValue", mappedBy="setting" )
     **/
    private $itemKeyValues;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="nameBn", type="string", length=255, nullable=true)
     */
    private $nameBn;

    /**
     * @var string
     *
     * @ORM\Column(name="shortCode", type="string", length=50, nullable=true)
     */
    private $shortCode;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $accountCode;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer",nullable=true)
     */
    private $noteNo;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $type;


    /**
     * @Gedmo\Translatable
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=255, unique=true)
     */
    private $slug;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean" )
     */
    private $status= true;

    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }


    /**
     * @return Item
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return string
     */
    public function getShortCode()
    {
        return $this->shortCode;
    }

    /**
     * @param string $shortCode
     */
    public function setShortCode($shortCode)
    {
        $this->shortCode = $shortCode;
    }

    /**
     * @return SettingType
     */
    public function getSettingType()
    {
        return $this->settingType;
    }

    /**
     * @param SettingType $settingType
     */
    public function setSettingType($settingType)
    {
        $this->settingType = $settingType;
    }

    /**
     * @return Inventory
     */
    public function getConfig()
    {
        return $this->config();
    }

    /**
     * @param Inventory $settingType
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getNameBn()
    {
        return $this->nameBn;
    }

    /**
     * @param string $nameBn
     */
    public function setNameBn(string $nameBn)
    {
        $this->nameBn = $nameBn;
    }

    /**
     * @return string
     */
    public function getAccountCode()
    {
        return $this->accountCode;
    }

    /**
     * @param string $accountCode
     */
    public function setAccountCode(string $accountCode)
    {
        $this->accountCode = $accountCode;
    }

    public function treasuryAccountCode(){

        $return =  $this->getNameBn()." - ".$this->getAccountCode();

        return $return;

    }

    /**
     * @return ItemKeyValue
     */
    public function getItemKeyValues()
    {
        return $this->itemKeyValues;
    }

    /**
     * @return mixed
     */
    public function getNoteNo()
    {
        return $this->noteNo;
    }

    /**
     * @param mixed $noteNo
     */
    public function setNoteNo($noteNo)
    {
        $this->noteNo = $noteNo;
    }





}

