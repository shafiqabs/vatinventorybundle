<?php

namespace Terminalbd\InventoryBundle\Entity;


use App\Entity\Application\Inventory;
use Appstore\Bundle\ProcurementBundle\Entity\PurchaseOrderItem;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;



/**
 * SalesItem
 *
 * @ORM\Table(name ="inv_work_order_item")
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\WorkOrderItemRepository")
 */
class WorkOrderItem
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Inventory")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private $config;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Item", inversedBy="salesItems" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $item;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }



}

