<?php
namespace Terminalbd\InventoryBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Terminalbd\InventoryBundle\Entity\MasterItem;

class MasterItemListener
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->createCode($args);
    }

    public function createCode(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof MasterItem) {
            $lastCode = $this->getLastCode($args, $entity);
            $entity->setCode($lastCode + 1);
            $entity->setSku(sprintf("%s", str_pad($entity->getCode(),4, '0', STR_PAD_LEFT)));
        }
    }

    /**
     * @param LifecycleEventArgs $args
     * @param $datetime
     * @param $entity
     * @return int|mixed
     */
    public function getLastCode(LifecycleEventArgs $args, MasterItem $entity)
    {

        $entityManager = $args->getEntityManager();
        $qb = $entityManager->getRepository('TerminalbdInventoryBundle:MasterItem')->createQueryBuilder('s');
        $qb
            ->select('MAX(s.code)')
	        ->where('s.config = :config')->setParameter('config', $entity->getConfig()->getId());
        $lastCode = $qb->getQuery()->getSingleScalarResult();
        if (empty($lastCode)) {
            return 0;
        }
        return $lastCode;
    }
}