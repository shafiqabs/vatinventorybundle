<?php

namespace Terminalbd\InventoryBundle\EventListener;


use Doctrine\ORM\Event\LifecycleEventArgs;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;

class PurchaseItemListener
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->createCode($args);
    }

    public function createCode(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // perhaps you only want to act on some "Purchase" entity
        if ($entity instanceof PurchaseItem) {
            $lastCode = $this->getLastCode($args,$entity);
            $entity->setCode($lastCode+1);
            $barcode = $this->getBarcode($entity);
            $entity->setBarcode($barcode);
        }
    }



    /**
     * @param LifecycleEventArgs $args
     * @param $entity
     * @return int|mixed
     */
    public function getLastCode(LifecycleEventArgs $args , PurchaseItem $entity)
    {

        $entityManager = $args->getEntityManager();
        $qb = $entityManager->getRepository('TerminalbdInventoryBundle:PurchaseItem')->createQueryBuilder('s');
        if($entity->getItem()){
            $qb
                ->select('MAX(s.code)')
                ->where('s.config = :config')->setParameter('config', $entity->getConfig()->getId())
                ->andWhere('s.item = :item')->setParameter('item', $entity->getItem()->getId());
            $lastCode = $qb->getQuery()->getSingleScalarResult();
        }else{
            $qb
                ->select('MAX(s.code)')
                ->where('s.config = :config')->setParameter('config', $entity->getConfig()->getId())
                ->andWhere('s.masterItem = :item')->setParameter('item', $entity->getMasterItem()->getId());
            $lastCode = $qb->getQuery()->getSingleScalarResult();
        }


        if (empty($lastCode)) {
            return 0;
        }
        return $lastCode;
    }

    /**
     * @param @entity
     */

    public function getBarcode(PurchaseItem $entity){

        if($entity->getItem()){
            $masterItemCode = $entity->getItem()->getSku();
        }else{
            $masterItemCode = $entity->getMasterItem()->getSku();
        }
        return $masterItemCode.$this->getStrPad($entity->getCode(),6);

    }

    private  function getStrPad($lastCode,$limit)
    {
        $data = str_pad($lastCode,$limit, '0', STR_PAD_LEFT);
        return $data;
    }
}