<?php
namespace Terminalbd\InventoryBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Terminalbd\InventoryBundle\Entity\StockItem;

class StockItemListener
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->createCode($args);
    }

    public function createCode(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof StockItem) {
            $lastCode = $this->getLastCode($args, $entity);
            $entity->setCode($lastCode + 1);
            $entity->setSku(sprintf("%s%s",$entity->getItem()->getSku()."-", str_pad($entity->getCode(),12, '0', STR_PAD_LEFT)));
        }
    }

    /**
     * @param LifecycleEventArgs $args
     * @param $datetime
     * @param $entity
     * @return int|mixed
     */

    public function getLastCode(LifecycleEventArgs $args, StockItem $entity)
    {

        $entityManager = $args->getEntityManager();
        $qb = $entityManager->getRepository('TerminalbdInventoryBundle:StockItem')->createQueryBuilder('s');
        $qb
            ->select('MAX(s.code)')
	        ->where('s.config = :config')->setParameter('config', $entity->getConfig()->getId())
	        ->andWhere('s.item = :item')->setParameter('item', $entity->getItem()->getId());
        $lastCode = $qb->getQuery()->getSingleScalarResult();
        if (empty($lastCode)) {
            return 0;
        }
        return $lastCode;
    }
}