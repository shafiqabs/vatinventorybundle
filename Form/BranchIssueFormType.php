<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\InventoryBundle\Form;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Accounting;
use App\Entity\Core\Customer;
use App\Entity\Core\Setting;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\AccountingBundle\Entity\AccountBank;
use Terminalbd\InventoryBundle\Entity\BranchIssue;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\Sales;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class BranchIssueFormType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $terminal =  $options['terminal']->getId();
        $builder
            ->add('branch', EntityType::class, array(
                'required'    => true,
                'class' => Setting::class,
                'placeholder' => 'Choose a branch name',
                'choice_label' => 'name',
                'attr'=>array('class'=>'form-control customer select2 branch'),
                'query_builder' => function(EntityRepository $er) use($terminal){
                    return $er->createQueryBuilder('e')
                        ->join("e.settingType",'st')
                        ->where("e.terminal ='{$terminal}'")
                        ->andWhere("st.slug ='branch'")
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('salesBy', EntityType::class, array(
                'required'    => true,
                'class' => User::class,
                'placeholder' => 'Choose a sales by',
                'choice_label' => 'name',
                'attr'=>array('class'=>'form-control customer select2 action'),
                'query_builder' => function(EntityRepository $er) use($terminal){
                    return $er->createQueryBuilder('e')
                        ->where("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('deliveryAddress', TextType::class, [
                'attr' => [
                    'autofocus' => true,
                    'class'=>'action text-left',
                    'placeholder'=>'Enter delivery address',
                ],
                'required' => false
            ])
            ->add('narration', TextType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class' => 'action text-left',
                    'placeholder'=>'Enter narration',
                ],
                'required' => false
            ])
            ->add('issuePerson', TextType::class, [
                'attr' => [
                    'autofocus' => true,
                    'class' => 'action text-left',
                    'placeholder'=>'Enter issue person',
                ],
                'required' => true
            ])
            ->add('issueDesignation', TextType::class, [
                'attr' => [
                    'autofocus' => true,
                    'class' => 'action text-left',
                    'placeholder'=>'Enter designation',
                ],
                'required' => true
            ])

            ->add('process', ChoiceType::class, [
                'choices'  => ['Created' => 'created','In-progress' => 'in-progress','Hold' => 'hold','Done' => 'done'],
                'required'    => true,
                'placeholder' => 'Choose a Process',
                'attr' => ['autofocus' => true,'class'=>'process action'],
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BranchIssue::class,
            'terminal' => Terminal::class,
        ]);
    }



}
