<?php
namespace Terminalbd\InventoryBundle\Form;

use App\Entity\Admin\ProductUnit;
use App\Entity\Application\Inventory;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\Category;
use Terminalbd\InventoryBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\Brand;
use Terminalbd\InventoryBundle\Entity\MasterItem;
use Terminalbd\InventoryBundle\Entity\Setting;
use Terminalbd\InventoryBundle\Entity\TaxTariff;
use Terminalbd\InventoryBundle\Repository\TaxTariffRepository;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ItemFormType extends AbstractType
{



    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $inventory =  $options['inventory']->getId();

        $builder

            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true],
                'label' => 'label.name',
            ])

            ->add('content', TextareaType::class, [
                'attr' => ['autofocus' => false,'rows'=>3,'class'=>'textarea'],
                'label' => 'label.name',
                'required'    => false,

            ])

             ->add('masterItem', EntityType::class, [
                'class' => MasterItem::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er)use($inventory) {
                    return $er->createQueryBuilder('e')
                        ->where("e.status =1")
                        ->andWhere("e.config ='{$inventory}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'span12 select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a  master product',
            ])

            ->add('brand', EntityType::class, array(
                'required'    => false,
                'class' => Setting::class,
                'placeholder' => 'Choose a  product brand/orgin',
                'choice_label' => 'name',
                'attr'=>array('class'=>'span12 m-wrap'),
                'query_builder' => function(EntityRepository $er)use($inventory){
                    return $er->createQueryBuilder('e')
                        ->join("e.settingType","st")
                        ->where("st.slug ='brand'")
                        ->andWhere("e.config ='{$inventory}'")
                        ->orderBy('e.name', 'ASC');
                },
            ))

            ->add('unit', EntityType::class, array(
                'required'    => false,
                'class' => ProductUnit::class,
                'placeholder' => 'Choose a  product unit',
                'choice_label' => 'name',
                'attr'=>array('class'=>'select2'),
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.name', 'ASC');
                },
            ))

           ->add('itemDimension',TextType::class,
                array(
                    'attr'=>array('class'=>'m-wrap span12','placeholder'=>'Enter item size,weight,dimention'),
                    'required'    => false,
                )
            )
            ->add('purchasePrice',TextType::class,
                array(
                    'attr'=>array('class'=>'m-wrap span12 purchasePrice','placeholder'=>'Enter purchase price'),
                    'required'    => false,
                )
            )
            ->add('purchasePrice',TextType::class,
                array(
                    'attr'=>array('class'=>'m-wrap span12 purchasePrice','placeholder'=>'Enter purchase price'),
                    'required'    => false,
                )
            )
            ->add('productionPrice',TextType::class,
                array(
                    'attr'=>array('class'=>'m-wrap span12 productionPrice','placeholder'=>'Enter production price','disabled'=>"disabled"),
                    'required'    => false,
                )
            )
            ->add('maxQuantity',TextType::class,
                array(
                    'attr'=>array('class'=>'m-wrap span12','placeholder'=>'Enter max quantity'),
                    'required'    => false,
                )
            )
            ->add('vatPercent',TextType::class,
                array(
                    'attr'=>array('class'=>'m-wrap span12','placeholder'=>'Enter override VAT percent'),
                    'required'    => false,
                    'help'    => 'If you went to override vat percent',
                )
            )
            ->add('sdPercent',TextType::class,
                array(
                    'attr'=>array('class'=>'m-wrap span12','placeholder'=>'Enter SD percent'),
                    'required'    => false,
                    'help'    => 'If you went to override SD percent',
                )
            )
            ->add('minQuantity',TextType::class,
                array(
                    'attr'=>array('class'=>'m-wrap span12','placeholder'=>'Enter min quantity'),
                    'required'    => false,
                )
            )
            ->add('taxOverride',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "info",
                    'data-on' => "Yes",
                    'data-off'=> "No"
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Item::class,
            'inventory' => Inventory::class,
        ]);
    }
}
