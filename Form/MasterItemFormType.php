<?php
namespace Terminalbd\InventoryBundle\Form;

use App\Entity\Admin\ProductUnit;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\Category;
use Terminalbd\InventoryBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\Brand;
use Terminalbd\InventoryBundle\Entity\MasterItem;
use Terminalbd\InventoryBundle\Entity\TaxTariff;
use Terminalbd\InventoryBundle\Repository\TaxTariffRepository;
use Terminalbd\NbrvatBundle\Entity\Setting;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class MasterItemFormType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder

            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true],
                'label' => 'label.name',
                'required'    => true,
            ])

            ->add('content', TextareaType::class, [
                'attr' => ['autofocus' => false,'rows'=>3,'class'=>'textarea'],
                'label' => 'label.name',
                'required'    => false,

            ])
            ->add('priceMethod', EntityType::class, array(
                'required'    => true,
                'class' => \Terminalbd\InventoryBundle\Entity\Setting::class,
                'choice_label' => 'name',
                'attr'=>array('class'=>'span12 m-wrap'),
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->join("e.settingType","st")
                        ->where("st.slug ='price-method'")
                        ->orderBy('e.name', 'ASC');
                },
            ))

            ->add('category', EntityType::class, [
                'class' => Category::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'span12 select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a  product category',
            ])

            ->add('productGroup', EntityType::class, [
                'class' => \Terminalbd\InventoryBundle\Entity\Setting::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->join("e.settingType","st")
                        ->where("st.slug ='product-group'")
                        ->andWhere("e.status =1")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'span12 select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a  product group',
            ])
           /* ->add('supplyOutputTax', EntityType::class, array(
                'required'    => false,
                'class' => Setting::class,
                'placeholder' => 'Choose a Supply - Output Tax',
                'choice_label' => 'name',
                'attr'=>array('class'=>'span12 m-wrap'),
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->join("e.settingType","st")
                        ->join("st.app","app")
                        ->where("st.slug ='part-3'")
                        ->andWhere("app.id =1")
                        ->andWhere("e.status =1")
                        ->andWhere("e.isInput =1")
                        ->orderBy('e.noteNo', 'ASC');
                },
            ))*/

          /*  ->add('inputTax', EntityType::class, array(
                'required'    => false,
                'class' => Setting::class,
                'placeholder' => 'Choose a Supply - Input Tax',
                'choice_label' => 'name',
                'attr'=>array('class'=>'span12 m-wrap'),
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->join("e.settingType","st")
                        ->join("st.app","app")
                        ->where("st.slug ='part-4'")
                        ->andWhere("app.id =1")
                        ->andWhere("e.status =1")
                        ->andWhere("e.mode ='local'")
                        ->andWhere("e.isInput =1")
                        ->orderBy('e.noteNo', 'ASC');
                },
            ))*/

             /*->add('inputImportTax', EntityType::class, array(
                'required'    => false,
                'class' => Setting::class,
                'placeholder' => 'Choose a Supply - Input Tax',
                'choice_label' => 'name',
                'attr'=>array('class'=>'span12 m-wrap'),
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->join("e.settingType","st")
                        ->join("st.app","app")
                        ->where("st.slug ='part-4'")
                        ->andWhere("app.id =1")
                        ->andWhere("e.status =1")
                        ->andWhere("e.mode ='import'")
                        ->orderBy('e.noteNo', 'ASC');
                },
            ))*/

            ->add('unit', EntityType::class, array(
                'required'    => false,
                'class' => ProductUnit::class,
                'placeholder' => 'Choose a  product unit',
                'choice_label' => 'name',
                'attr'=>array('class'=>'select2'),
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.status', 'ASC');
                },
            ))

            ->add('customsDuty', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder'=>"CD"],
                'required' => false,
                'label' => 'label.customDuty',

            ])

            ->add('supplementaryDuty', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder'=>"SD"],
                'required' => false,
            ])
            ->add('valueAddedTax', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder'=>"VAT"],
                'required' => false,
            ])
            ->add('minValueAddedTax', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder'=>"Min VAT"],
                'required' => false,
            ])
            ->add('advanceIncomeTax', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder'=>"AIT"],
                'required' => false,
            ])
            ->add('advanceTax', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder'=>"AT"],
                'required' => false,
            ])
            ->add('regulatoryDuty', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder'=>"RD"],
                'required' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MasterItem::class,
        ]);
    }
}
