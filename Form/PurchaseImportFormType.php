<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\InventoryBundle\Form;

use App\Entity\Admin\Country;
use App\Entity\Admin\Terminal;
use App\Entity\Application\Accounting;
use App\Entity\Application\Inventory;
use App\Entity\Core\Vendor;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\AccountingBundle\Entity\AccountBank;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\Setting;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class PurchaseImportFormType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $terminal   =  $options['terminal']->getId();
        $account    =  $options['account']->getId();
        $inventory  =  $options['inventory']->getId();

        $builder
            ->add('vendor', EntityType::class, array(
                'required'    => true,
                'class' => Vendor::class,
                'placeholder' => 'Choose a vendor',
                'choice_label' => 'companyName',
                'attr'=>array('class'=>'form-control vendor select2 purchase'),
                'query_builder' => function(EntityRepository $er) use($terminal){
                    return $er->createQueryBuilder('e')
                        ->where("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('receiveAddress', TextType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'purchase text-left',
                    'placeholder'=>'Enter received address',
                ],
                'required' => false
            ])
            ->add('narration', TextType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class' => 'purchase text-left',
                    'placeholder'=>'Enter narration',
                ],
                'required' => false
            ])
            ->add('assignPerson', TextType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class' => 'purchase text-left',
                    'placeholder'=>'Enter assign person',
                ],
                'required' => false
            ])
            ->add('assignPersonDesignation', TextType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class' => 'purchase text-left',
                    'placeholder'=>'Enter designation',
                ],
                'required' => false
            ])
            ->add('discountMode', ChoiceType::class, [
                'choices'  => ['Flat' => 'flat','Percent' => 'percent'],
                'required'    => false,
                'placeholder' => 'Mode',
                'attr' => ['autofocus' => true,'class'=>'purchase discountMode'],
            ])
            ->add('discountCalculation', NumberType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Taka','class'=>'amount purchase discountCalculation'],
                'required' => false
            ])
            ->add('transactionMethod', ChoiceType::class, [
                'choices'  => ['Cash' => 'cash','Bank' => 'bank','Mobile' => 'mobile'],
                'required'    => false,
                'placeholder' => 'Transaction',
                'attr' => ['autofocus' => true,'class'=>'transaction-method purchase'],
            ])

            ->add('process', ChoiceType::class, [
                'choices'  => ['Created' => 'created','In-progress' => 'in-progress','Hold' => 'hold','Done' => 'done'],
                'required'    => false,
                'placeholder' => 'Choose a Process',
                'attr' => ['autofocus' => true,'class'=>'process purchase'],
            ])

            ->add('accountBank', EntityType::class, array(
                'required'    => false,
                'class' => AccountBank::class,
                'placeholder' => 'Choose a bank account',
                'choice_label' => 'name',
                'attr'=>array('class'=>'form-control purchase'),
                'query_builder' => function(EntityRepository $er) use($account){
                    return $er->createQueryBuilder('e')
                        ->where("e.config ='{$account}'")
                        ->andWhere("e.mode ='bank'")
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('accountMobile', EntityType::class, array(
                'required'    => false,
                'class' => AccountBank::class,
                'placeholder' => 'Choose a bank account',
                'choice_label' => 'name',
                'attr'=>array('class'=>'form-control purchase'),
                'query_builder' => function(EntityRepository $er) use($account){
                    return $er->createQueryBuilder('e')
                        ->where("e.config ='{$account}'")
                        ->andWhere("e.mode ='mobile'")
                        ->orderBy('e.name', 'ASC');
                },
            ))

            ->add('payment', NumberType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Taka','class'=>'purchase'],
                'required' => false
            ])
            ->add('lcNo', NumberType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'LC no','class'=>'purchase'],
                'required'    => true,
            ])
            ->add('challanNo', TextType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'purchase',
                    'placeholder'=>'Enter bill of entry',
                ],
                'required' => true
            ])
            ->add('clearForwardingFirm', NumberType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'C&F firm','class'=>'purchase'],
                'required' => false
            ])
            ->add('customsHouse', EntityType::class, array(
                'required'    => true,
                'class' => Setting::class,
                'placeholder' => 'Choose a customs house',
                'choice_label' => 'name',
                'attr'=>array('class'=>'form-control select2 purchase'),
                'query_builder' => function(EntityRepository $er) use($inventory){
                    return $er->createQueryBuilder('e')
                        ->join("e.settingType","st")
                        ->where("e.config ='{$inventory}'")
                        ->andWhere("st.slug ='customs-house'")
                        ->orderBy('e.name', 'ASC');
                },
            ))

             ->add('country', EntityType::class, array(
                'required'    => true,
                'class' => Country::class,
                'placeholder' => 'Choose a orgin country',
                'choice_label' => 'name',
                'attr'=>array('class'=>'form-control select2 purchase'),
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('billOfEntryDate', DateType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'purchase',
                ],
                'widget' => 'single_text',
                'html5' => true,
            ])
            ->add('lcDate', DateType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'purchase',
                ],
                'widget' => 'single_text',
                'html5' => true,
            ])
            ->add('chequeNo', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Cheque no','class'=>'text-left purchase'],
                'required' => false
            ])
            ->add('chequeDate', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Cheque date','class'=>'text-left datePicker col-md-8 purchase'],
                'required' => false
            ])
            ->add('trxId', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Teansaction id','class'=>'purchase text-left'],
                'required' => false
            ])
            ->add('trxDate', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Teansaction date','class'=>'datePicker col-md-8  purchase text-left'],
                'required' => false
            ])
            /*->add('file', null, [
                'required' => false,
                'attr' => [
                    'class'=>'form-file-input purchase'
                ]
            ])*/;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Purchase::class,
            'terminal' => Terminal::class,
            'account' => Accounting::class,
            'inventory' => Inventory::class,
        ]);
    }



}
