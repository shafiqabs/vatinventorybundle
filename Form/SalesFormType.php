<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\InventoryBundle\Form;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Accounting;
use App\Entity\Core\Customer;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\AccountingBundle\Entity\AccountBank;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\Sales;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class SalesFormType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $terminal =  $options['terminal']->getId();
        $account =  $options['account']->getId();

        $builder
            ->add('customer', EntityType::class, array(
                'required'    => true,
                'class' => Customer::class,
                'placeholder' => 'Choose a customer',
                'choice_label' => 'companyName',
                'attr'=>array('class'=>'form-control customer select2 action'),
                'query_builder' => function(EntityRepository $er) use($terminal){
                    return $er->createQueryBuilder('e')
                        ->where("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('salesBy', EntityType::class, array(
                'required'    => true,
                'class' => User::class,
                'placeholder' => 'Choose a sales by',
                'choice_label' => 'name',
                'attr'=>array('class'=>'form-control customer select2 action'),
                'query_builder' => function(EntityRepository $er) use($terminal){
                    return $er->createQueryBuilder('e')
                        ->where("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('deliveryAddress', TextType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'action text-left',
                    'placeholder'=>'Enter delivery address',
                ],
                'required' => false
            ])
            ->add('narration', TextType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class' => 'action text-left',
                    'placeholder'=>'Enter narration',
                ],
                'required' => false
            ])
            ->add('issuePerson', TextType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class' => 'action text-left',
                    'placeholder'=>'Enter issue person',
                ],
                'required' => true
            ])
            ->add('issueDesignation', TextType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class' => 'action text-left',
                    'placeholder'=>'Enter designation',
                ],
                'required' => true
            ])
            ->add('discountMode', ChoiceType::class, [
                'choices'  => ['Flat' => 'flat','Percent' => 'percent'],
                'required'    => false,
                'placeholder' => 'Mode',
                'attr' => ['autofocus' => true,'class'=>'action discountMode'],
            ])
            ->add('discountCalculation', NumberType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Taka','class'=>'amount action discountCalculation'],
                'required' => false
            ])
            ->add('transactionMethod', ChoiceType::class, [
                'choices'  => ['Cash' => 'cash','Bank' => 'bank','Mobile' => 'mobile'],
                'required'    => false,
                'placeholder' => 'Transaction',
                'attr' => ['autofocus' => true,'class'=>'transaction-method action'],
            ])

            ->add('process', ChoiceType::class, [
                'choices'  => ['Created' => 'created','In-progress' => 'in-progress','Hold' => 'hold','Done' => 'done'],
                'required'    => false,
                'placeholder' => 'Choose a Process',
                'attr' => ['autofocus' => true,'class'=>'process action'],
            ])

            ->add('chequeNo', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Cheque no','class'=>'text-left purchase'],
                'required' => false
            ])
            ->add('chequeDate', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Cheque date','class'=>'text-left datePicker col-md-8 purchase'],
                'required' => false
            ])
            ->add('trxId', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Teansaction id','class'=>'purchase text-left'],
                'required' => false
            ])
            ->add('trxDate', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Teansaction date','class'=>'datePicker col-md-8  purchase text-left'],
                'required' => false
            ])

            ->add('accountBank', EntityType::class, array(
                'required'    => false,
                'class' => AccountBank::class,
                'placeholder' => 'Choose a bank account',
                'choice_label' => 'name',
                'attr'=>array('class'=>'form-control action'),
                'query_builder' => function(EntityRepository $er) use($account){
                    return $er->createQueryBuilder('e')
                        ->where("e.config ='{$account}'")
                        ->andWhere("e.mode ='bank'")
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('accountMobile', EntityType::class, array(
                'required'    => false,
                'class' => AccountBank::class,
                'placeholder' => 'Choose a bank account',
                'choice_label' => 'name',
                'attr'=>array('class'=>'form-control action'),
                'query_builder' => function(EntityRepository $er) use($account){
                    return $er->createQueryBuilder('e')
                        ->where("e.config ='{$account}'")
                        ->andWhere("e.mode ='mobile'")
                        ->orderBy('e.name', 'ASC');
                },
            ))

             ->add('amount', NumberType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Taka','class'=>'Receive payment action'],
                'required' => false
            ])
            ->add('issueDate', DateType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'action',
                ],
                'widget' => 'single_text',
                'html5' => true,
            ])
            ->add('issueTime', TextType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class'=>'timePicker'
                ],
            ])

        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sales::class,
            'terminal' => Terminal::class,
            'account' => Accounting::class,
        ]);
    }



}
