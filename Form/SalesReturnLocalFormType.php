<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\InventoryBundle\Form;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Accounting;
use App\Entity\Application\Inventory;
use App\Entity\Core\Customer;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\AccountingBundle\Entity\AccountBank;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\Sales;
use Terminalbd\InventoryBundle\Entity\SalesReturn;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class SalesReturnLocalFormType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $inventory =  $options['inventory']->getId();
        $builder
            ->add('sales', EntityType::class, array(
                'required'    => false,
                'class' => Sales::class,
                'placeholder' => 'Choose a sales invoice',
                'choice_label' => 'invoice',
                'attr'=>array('class'=>'form-control invoice select2 salesId'),
                'query_builder' => function(EntityRepository $er) use($inventory){
                    return $er->createQueryBuilder('e')
                        /*  ->where("e.config ='{$inventory}'")*/
                        ->where("e.mode ='local'")
                        ->andWhere("e.process ='approved'")
                        ->orderBy('e.invoice', 'DESC');
                },
            ))
            ->add('narration', TextType::class, [
                'attr' => [
                    'autofocus' => true,
                    'class' => 'purchase text-left',
                    'placeholder'=>'Enter narration',
                ],
                'required' => true
            ])
            ->add('issuePerson', TextType::class, [
                'attr' => [
                    'autofocus' => true,
                    'class' => 'purchase text-left',
                    'placeholder'=>'Enter assign person',
                ],
                'required' => true
            ])
            ->add('issueDesignation', TextType::class, [
                'attr' => [
                    'autofocus' => false,
                    'class' => 'purchase text-left',
                    'placeholder'=>'Enter designation',
                ],
                'required' => true
            ])
            ->add('process', ChoiceType::class, [
                'choices'  => ['Created' => 'created','In-progress' => 'in-progress','Hold' => 'hold','Done' => 'done'],
                'required'    => false,
                'placeholder' => 'Choose a Process',
                'attr' => ['autofocus' => true,'class'=>'process purchase'],
            ])

            ->add('file', null, [
                'required' => false,
                'attr' => [
                    'class'=>'form-file-input'
                ]
            ])

        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SalesReturn::class,
            'inventory' => Inventory::class,
        ]);
    }



}
