<?php

namespace Terminalbd\InventoryBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * BusinessConfigRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class InventoryConfigRepository extends EntityRepository
{


    public function inventoryReset()
    {

	    set_time_limit(0);
	    ignore_user_abort(true);
	    $em = $this->_em;

    }
}
