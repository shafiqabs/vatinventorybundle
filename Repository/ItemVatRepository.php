<?php

namespace Terminalbd\InventoryBundle\Repository;
use Appstore\Bundle\InventoryBundle\Entity\InventoryConfig;
use Doctrine\ORM\EntityRepository;
use Terminalbd\InventoryBundle\Entity\Brand;
use Terminalbd\InventoryBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\ItemVat;
use Terminalbd\InventoryBundle\Entity\MasterItem;

/**
 * ItemBrandRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ItemVatRepository extends EntityRepository
{


    public function masterInsertSurcharge(MasterItem $item,$data)
    {

        $em = $this->_em;
        $i=0;
        foreach ($data['surcharge'] as $value) {

            $vatId = isset($data['vatId'][$i]) ? $data['vatId'][$i] : 0;
            $amount = $data['amount'][$i] ? $data['amount'][$i] : 0;
            $mode = $data['mode'][$i] ? $data['mode'][$i] : 'percent';

            if(!empty($vatId)){
                $this->updatedItemSurcharge($vatId,$data['amount'][$i],$data['mode'][$i]);
            }else{
                $surcharge = $data['surcharge'][$i] ? $data['surcharge'][$i] : 0;
                $surchargeObj = $em->getRepository('TerminalbdInventoryBundle:Setting')->find($surcharge);
                $entity = new ItemVat();
                $entity->setMasterItem($item);
                $entity->setSurcharge($surchargeObj);
                $entity->setAmount($amount);
                $entity->setMode($mode);
                $em->persist($entity);
                $em->flush($entity);

            }
            $i++;
        }

    }


    private function updatedItemSurcharge($itemVat,$amount,$mode){

        $em = $this->_em;
        $itemVat = $this->find($itemVat);
        if ($itemVat) {
            $itemVat->setAmount($amount);
            $itemVat->setMode($mode);
            $em->flush();
        }
    }


}
