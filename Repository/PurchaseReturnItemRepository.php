<?php

namespace Terminalbd\InventoryBundle\Repository;

use App\Entity\Application\Inventory;
use App\Service\ConfigureManager;
use Core\UserBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Setting\Bundle\ToolBundle\Entity\GlobalOption;
use Terminalbd\InventoryBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\MasterItem;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Entity\PurchaseReturn;
use Terminalbd\InventoryBundle\Entity\PurchaseReturnItem;


/**
 * ExpenditureItemRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PurchaseReturnItemRepository extends EntityRepository
{

    public function getPurchaseReturnList(PurchaseReturn $return)
    {
        /* @var $item PurchaseReturnItem */
        $items = array();
        foreach ($return->getReturnItems() as $item){
            $items[$item->getPurchaseItem()->getId()] = $item;
        }
        return $items;
    }

    public function insertPurchaseKeyItem(PurchaseReturn $reEntity,$data)
    {

        $em = $this->_em;
        $i = 0;

        if(isset($data['quantity']) OR isset($data['returnItemId']) ){
            foreach ($data['purchaseItemId'] as $value) {
                $purchaseItemId = isset($data['purchaseItemId'][$i]) ? $data['purchaseItemId'][$i] : 0 ;
                $metaId = isset($data['returnItemId'][$i]) ? $data['returnItemId'][$i] : 0 ;
                $itemKeyValue = $em->getRepository('TerminalbdInventoryBundle:PurchaseReturnItem')->findOneBy(array('purchaseReturn' => $reEntity,'purchaseItem' => $purchaseItemId,'process' =>'created'));
                if(!empty($itemKeyValue)){
                    $this->updateMetaAttribute($itemKeyValue,$data['quantity'][$i]);
                }else{
                    if(!empty($data['quantity'][$i]))
                    {
                        $purchaseItem = $em->getRepository('TerminalbdInventoryBundle:PurchaseItem')->find($data['purchaseItemId'][$i]);
                        $entity = new PurchaseReturnItem();
                        $entity->setPurchaseReturn($reEntity);
                        $entity->setQuantity($data['quantity'][$i]);
                        $entity->setItem($purchaseItem->getItem());
                        $entity->setPurchaseItem($purchaseItem);
                        $entity->setPrice($purchaseItem->getPurchasePrice());
                        $entity->setSubTotal($entity->getQuantity() * $purchaseItem->getPurchasePrice());
                        $em->persist($entity);
                        $em->flush($entity);
                        $this->updateItemTax($entity);
                    }

                }
                $i++;
            }

        }
    }

    public function updateMetaAttribute(PurchaseReturnItem $item,$key)
    {
        $em = $this->_em;
        $item->setQuantity(floatval($key));
        $item->setSubTotal($item->getQuantity() * $item->getPrice());
        $em->flush();
        $this->updateItemTax($item);
    }

    public function updateItemTax(PurchaseReturnItem $entity)
    {
        $em = $this->_em;
        $subTotal = $entity->getSubTotal();
        $vat = $entity->getPurchaseItem();
        if($vat){

            /* @var $vat PurchaseItem */

            if($entity->getPurchaseReturn()->getMode() == "foreign" and $vat->getCustomsDuty() > 0){
                $entity->setCustomsDutyPercent($vat->getCustomsDutyPercent());
                $amount = $this->getTaxTariffCalculation($subTotal,$vat->getCustomsDutyPercent());
                $entity->setCustomsDuty($amount);
            }

            $rdModes = array("foreign");
            if(in_array($entity->getPurchaseReturn()->getMode(),$rdModes) and $vat->getRegulatoryDuty() > 0){
                $entity->setRegulatoryDutyPercent($vat->getRegulatoryDutyPercent());
                $rd = $this->getTaxTariffCalculation($subTotal,$vat->getRegulatoryDutyPercent());
                $entity->setRegulatoryDuty($rd);
            }

            $sdModes = array("foreign","local");
            if(in_array($entity->getPurchaseReturn()->getMode(),$sdModes)){
                $total = $subTotal +  $entity->getCustomsDuty() + $entity->getRegulatoryDuty();
                if($entity->getPurchaseItem()->getSupplementaryDuty() > 0 and $entity->getPurchaseReturn()->getMode() == "foreign"){
                    $total = ($subTotal +  $entity->getCustomsDuty() + $entity->getRegulatoryDuty());
                    $entity->setSupplementaryDutyPercent($vat->getSupplementaryDutyPercent());
                    $amount = $this->getTaxTariffCalculation($total,$vat->getSupplementaryDutyPercent());
                    $entity->setSupplementaryDuty($amount);
                }elseif($entity->getPurchaseItem()->getSupplementaryDuty() > 0 and $entity->getPurchaseReturn()->getMode() == "local"){
                    $total = $subTotal;
                    $amount = (($entity->getPurchaseItem()->getSupplementaryDuty() / $entity->getPurchaseItem()->getTotalQuantity()) * $entity->getQuantity());
                    $entity->setSupplementaryDuty($amount);
                }

            }

            $vatModes = array("foreign","registred","local");
            if(in_array($entity->getPurchaseReturn()->getMode(),$vatModes) and $vat->getValueAddedTax() > 0){
                if($entity->getSupplementaryDuty() > 0){
                    $total = ($subTotal +  $entity->getCustomsDuty() + $entity->getRegulatoryDuty() +  $entity->getSupplementaryDuty());
                }elseif(in_array($entity->getPurchaseReturn()->getMode(),$vatModes)){
                    $total = $subTotal +  $entity->getCustomsDuty() + $entity->getRegulatoryDuty();
                }
                $entity->setValueAddedTaxPercent($vat->getValueAddedTaxPercent());
                $amount = $this->getTaxTariffCalculation($total,$vat->getValueAddedTaxPercent());
                $entity->setValueAddedTax($amount);
            }

            $aitModes = array("foreign");
            if(in_array($entity->getPurchaseReturn()->getMode(),$aitModes) and $vat->getAdvanceIncomeTax() > 0){
                if($entity->getSupplementaryDuty() > 0){
                    $total = ($subTotal +  $entity->getCustomsDuty() + $entity->getRegulatoryDuty() +  $entity->getSupplementaryDuty());
                }elseif(in_array($entity->getPurchaseReturn()->getMode(),$aitModes)){
                    $total = $subTotal +  $entity->getCustomsDuty() + $entity->getRegulatoryDuty();
                }
                $entity->setAdvanceIncomeTaxPercent($vat->getAdvanceIncomeTaxPercent());
                $ait = $this->getTaxTariffCalculation($total,$vat->getAdvanceIncomeTaxPercent());
                $entity->setAdvanceIncomeTax($ait);
            }

            $atvModes = array("foreign");
            if(in_array($entity->getPurchaseReturn()->getMode(),$atvModes) and $vat->getAdvanceTradeVat() > 0){
                $entity->setAdvanceTradeVatPercent($vat->getAdvanceTradeVatPercent());
                $atv = $this->getTaxTariffCalculation($subTotal,$vat->getAdvanceTradeVatPercent());
                $entity->setAdvanceTradeVat($atv);
            }
            $vdsModes = array("registred","non-registred");
            if(in_array($entity->getPurchaseReturn()->getMode(),$vdsModes) and $vat->getVatDeductionSource() > 0){
                $entity->setVatDeductionSourcePercent($vat->getVatDeductionSourcePercent());
                $amount = $this->getTaxTariffCalculation($subTotal,$vat->getVatDeductionSourcePercent());
                $entity->setVatDeductionSource($amount);
            }

            $TTI = ($entity->getCustomsDuty() + $entity->getSupplementaryDuty() + $entity->getValueAddedTax() + $entity->getAdvanceIncomeTax() + $entity->getRegulatoryDuty() + $entity->getAdvanceTradeVat() + $entity->getVatDeductionSource());

            $reateModes = array("registred");
            if(in_array($entity->getPurchaseReturn()->getMode(),$reateModes) and $vat->getRebate() > 0){
                $entity->setRebatePercent($vat->getRebate());
                $amount = $this->getTaxTariffCalculation($TTI,$vat->getRebate());
                $entity->setRebate($amount);
            }
            $entity->setTotalTaxIncidence($TTI  - $entity->getRebate());
        }
        $entity->setTotal($entity->getSubTotal() + $entity->getTotalTaxIncidence());
        $em->persist($entity);
        $em->flush($entity);

    }

    private function getTaxTariffCalculation($subTotal,$tariff)
    {
        $value = 0;
        $value = (($subTotal * $tariff)/100);
        return $value;
    }


    public function mushukPurchaseReturnItem(Inventory $config , $data)
    {
        $qb = $this->createQueryBuilder('item');
        $qb->leftJoin('item.purchaseReturn','purchaseReturn');
        $qb->leftJoin('item.item','product');
        $qb->leftJoin('item.purchaseItem','purchaseItem');
        $qb->leftJoin('purchaseItem.purchase','purchase');
        $qb->leftJoin('purchase.vendor','vendor');

        $qb->select('item.id as id','item.quantity as quantity','item.subTotal as subTotal','item.supplementaryDuty as sd','item.valueAddedTax as vat');
        $qb->addSelect('purchaseItem.quantity as purchaseQuantity','purchaseItem.subTotal as purchasePrice','purchaseItem.supplementaryDuty as purchaseSd','purchaseItem.valueAddedTax as purchaseVat');
        $qb->addSelect('purchaseReturn.invoice as returnInvoice','purchaseReturn.updated as issueDate');
        $qb->addSelect('product.name as itemName');
        $qb->addSelect('purchase.challanNo as challanNo','purchaseReturn.receiveDate as challandate');
        $qb->addSelect("vendor.companyName as companyName","vendor.address as companyAddress","vendor.binNo as binNo","vendor.nid as nid");
        $qb->where("purchaseReturn.config = :config")->setParameter('config', $config);
        if (!empty($startDate)) {
            $datetime = new \DateTime($startDate);
            $start = $datetime->format('Y-m-d 00:00:00');
            $qb->andWhere("item.updated >= :startDate")->setParameter('startDate',$start);
        }
        if (!empty($endDate)) {
            $datetime = new \DateTime($endDate);
            $end = $datetime->format('Y-m-d 23:59:59');
            $qb->andWhere("item.updated <= :endDate")->setParameter('endDate',$end);
        }
        $qb->orderBy('item.updated', 'ASC');
        $result = $qb->getQuery()->getArrayResult();
        return  $result;
    }


}
