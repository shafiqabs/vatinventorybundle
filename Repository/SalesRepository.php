<?php

namespace Terminalbd\InventoryBundle\Repository;
use App\Entity\Application\Inventory;
use App\Service\ConfigureManager;
use Core\UserBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Terminalbd\InventoryBundle\Entity\BranchIssue;
use Terminalbd\InventoryBundle\Entity\Sales;
use Terminalbd\InventoryBundle\Entity\SalesItem;
use Terminalbd\InventoryBundle\Entity\SalesItemExportTax;
use Terminalbd\InventoryBundle\Entity\SalesItemTax;
use Terminalbd\ProductionBundle\Entity\ProductionElement;

/**
 * SalesRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SalesRepository extends EntityRepository
{

    public function handleWidthSearch($qb,$data)
   {

       $invoice           = isset($data['invoice'])? $data['invoice'] :'';
       $customer          = isset($data['customer'])? $data['customer'] :'';
       $mode              = isset($data['mode'])? $data['mode'] :'';
       $process           = isset($data['process'])? $data['process'] :'';
       $method             = isset($data['method'])? $data['method'] :'';
       $startDate           = isset($data['startDate'])? $data['startDate'] :'';
       $endDate           = isset($data['endDate'])? $data['endDate'] :'';
       $receivedDate           = isset($data['receivedDate'])? $data['receivedDate'] :'';

       if (!empty($item)) {
           $qb->andWhere("item.invoice LIKE :name")->setParameter('name', "%{$invoice}%");
       }
       if (!empty($customer)) {
           $qb->andWhere("customer.id = :customerId")->setParameter('customerId', $customer);
       }
       if (!empty($mode)) {
           $qb->andWhere("item.mode = :mode")->setParameter('mode', $mode);
       }
       if (!empty($method)) {
           $qb->andWhere("item.transactionMethod = :method")->setParameter('method', $method);
       }
       if (!empty($process)) {
           $qb->andWhere("item.process = :process")->setParameter('process', $process);
       }

       if (!empty($startDate)) {
           $datetime = new \DateTime($startDate);
           $start = $datetime->format('Y-m-d 00:00:00');
           $qb->andWhere("item.created >= :startDate")->setParameter('startDate',$start);
       }
       if (!empty($endDate)) {
           $datetime = new \DateTime($endDate);
           $end = $datetime->format('Y-m-d 23:59:59');
           $qb->andWhere("item.created <= :endDate")->setParameter('endDate',$end);
       }


   }

    public function findWithSearch($config,$parameter,$query)
    {
        if (!empty($parameter['orderBy'])) {
            $sortBy = $parameter['orderBy'];
            $order = $parameter['order'];
        }

        $invoice           = isset($data['invoice'])? $data['invoice'] :'';
        $customer          = isset($data['customer'])? $data['customer'] :'';
        $mode              = isset($data['mode'])? $data['mode'] :'';
        $process           = isset($data['process'])? $data['process'] :'';
        $method             = isset($data['method'])? $data['method'] :'';
        $startDate           = isset($data['startDate'])? $data['startDate'] :'';
        $endDate           = isset($data['endDate'])? $data['endDate'] :'';
        $receivedDate           = isset($data['receivedDate'])? $data['receivedDate'] :'';

        $qb = $this->createQueryBuilder('item');
        $qb->leftJoin('item.branch','b');
        $qb->join('item.customer','c');
        $qb->select('item.id as id','item.invoice as invoice','item.netTotal as netTotal','item.amount as amount','item.subTotal as subTotal','item.discount as discount','item.due as due','item.totalTaxIncidence as taxTotal',"item.process as process","item.created as created","item.issueDate as issueDate","item.transactionMethod as method","item.discountMode as discountMode","item.mode as mode","item.discountCalculation as calculation");
        $qb->addSelect("b.name as branch");
        $qb->addSelect("c.companyName as company","c.address as address","c.binNo as binNo","c.nid as nidNo");
        $qb->where("item.config = :config")->setParameter('config', $config);
        if (!empty($item)) {
            $qb->andWhere("item.invoice LIKE :name")->setParameter('name', "%{$invoice}%");
        }
        if (!empty($customer)) {
            $qb->andWhere("customer.id = :customerId")->setParameter('customerId', $customer);
        }
        if (!empty($mode)) {
            $qb->andWhere("item.mode = :mode")->setParameter('mode', $mode);
        }
        if (!empty($method)) {
            $qb->andWhere("item.transactionMethod = :method")->setParameter('method', $method);
        }
        if (!empty($process)) {
            $qb->andWhere("item.process = :process")->setParameter('process', $process);
        }

        if (!empty($startDate)) {
            $datetime = new \DateTime($startDate);
            $start = $datetime->format('Y-m-d 00:00:00');
            $qb->andWhere("item.created >= :startDate")->setParameter('startDate',$start);
        }
        if (!empty($endDate)) {
            $datetime = new \DateTime($endDate);
            $end = $datetime->format('Y-m-d 23:59:59');
            $qb->andWhere("item.created <= :endDate")->setParameter('endDate',$end);
        }

        if (!empty($startDate)) {
            $datetime = new \DateTime($startDate);
            $start = $datetime->format('Y-m-d 00:00:00');
            $qb->andWhere("item.receiveDate >= :startDate")->setParameter('startDate',$start);
            $end = $datetime->format('Y-m-d 23:59:59');
            $qb->andWhere("item.receiveDate <= :endDate")->setParameter('endDate',$end);
        }
        $qb->setFirstResult($parameter['offset']);
        $qb->setMaxResults($parameter['limit']);
        if ($parameter['orderBy']){
            $qb->orderBy($sortBy, $order);
        }else{
            $qb->orderBy('item.created', 'ASC');
        }
        $result = $qb->getQuery()->getArrayResult();
        return  $result;

    }

    public function findReportSearch($config,$data)
    {

        $qb = $this->createQueryBuilder('item');
        $qb->leftJoin('item.branch','b');
        $qb->join('item.customer','c');
        $qb->select('item.id as id','item.invoice as invoice','item.total as total','item.netTotal as netTotal','item.amount as amount','item.subTotal as subTotal','item.totalTaxIncidence as taxTotal',"item.process as process","item.created as created","item.issueDate as issueDate","item.transactionMethod as method","item.discountMode as discountMode");
        $qb->addSelect("b.name as branch");
        $qb->addSelect("c.companyName as company","c.address as address","c.binNo as binNo","c.nid as nidNo");
        $qb->where("item.config = :config")->setParameter('config', $config);
        $qb->andWhere("item.process = :process")->setParameter('process', "approved");
        $qb->andWhere("item.total >= :value")->setParameter('value', 200000);
        $this->handleWidthSearch($qb,$data);
        $qb->orderBy('item.created', 'ASC');
        $result = $qb->getQuery()->getArrayResult();
        return  $result;

    }

    public function searchAutoComplete($q, GlobalOption $global)
	{
		$qb = $this->createQueryBuilder('e');
		$qb->select('e.companyName as id');
		$qb->addSelect('e.companyName as text');
		$qb->where("e.globalOption = :global")->setParameter('global', $global->getId());
		$qb->andWhere($qb->expr()->like("e.companyName", "'%$q%'" ));
		$qb->groupBy('e.companyName');
		$qb->orderBy('e.companyName', 'ASC');
		$qb->setMaxResults( '30' );
		return $qb->getQuery()->getResult();

	}

	public function salesSummary(Inventory $config)
    {
            $qb = $this->createQueryBuilder('e');
            $qb->select(
                'sum(e.subTotal) as subTotal',
                'sum(e.supplementaryDuty) as supplementaryDuty',
                'sum(e.valueAddedTax) as valueAddedTax'
            );
            $qb->where('e.config = :entity')->setParameter('entity', $config ->getId());
            $datetime = new \DateTime("now");
            $start = $datetime->format('Y-m-01 00:00:00');
          //  $qb->andWhere("e.updated >= :startDate")->setParameter('startDate',$start);
            $end = $datetime->format('Y-m-t 23:59:59');
          //  $qb->andWhere("e.updated <= :endDate")->setParameter('endDate',$end);
            $result = $qb->getQuery()->getSingleResult();
            return $result;

    }

    public function updateSalesTotalPrice(Sales $entity)
    {
        $em = $this->_em;

        $confManager = new ConfigureManager();
        
        $total = $em->createQueryBuilder()
            ->from('TerminalbdInventoryBundle:SalesItem','si')
            ->select(
                'sum(si.subTotal) as subTotal',
                'sum(si.supplementaryDuty) as supplementaryDuty',
                'sum(si.valueAddedTax) as valueAddedTax',
                'sum(si.totalTaxIncidence) as totalTaxIncidence',
                'sum(si.totalQuantity) as quantity',
                'count(si.id) as totalItem'
            )
            ->where('si.sales = :entity')
            ->setParameter('entity', $entity ->getId())
            ->getQuery()->getSingleResult();

        $subTotal = $total['subTotal'];
        if($subTotal > 0){
            $entity->setQuantity($total['quantity']);
            $entity->setTotalItem($total['totalItem']);
            $entity->setSubTotal($confManager->numberFormat($subTotal));
            $entity->setValueAddedTax($confManager->numberFormat($total['supplementaryDuty']));
            $entity->setValueAddedTax($confManager->numberFormat($total['valueAddedTax']));
            $entity->setTotalTaxIncidence($confManager->numberFormat($total['totalTaxIncidence'],2));
            if($entity->getDiscountMode() and floatval($entity->getDiscountCalculation()) > 0){
                $discount = $this->discountCalculation($entity);
            }else{
                $netTotal = ($entity->getSubTotal() + $entity->getTotalTaxIncidence());
                $entity->setTotal($confManager->numberFormat($netTotal));
                $entity->setNetTotal($confManager->numberFormat($netTotal));
                $entity->setDue($confManager->numberFormat($entity->getNetTotal() - $entity->getAmount()));
            }

        }else{

            $entity->setSubTotal(0);
            $entity->setTotal(0);
            $entity->setNetTotal(0);
            $entity->setDue(0);
            $entity->setDiscount(0);
            $entity->setDiscountCalculation(0);
            $entity->setValueAddedTax(0);
            $entity->setTotalTaxIncidence(0);
        }

        $em->persist($entity);
        $em->flush();
        return $entity;
    }

    public function discountCalculation(Sales $entity){

        $confManager = new ConfigureManager();
        $em = $this->_em;
        $discount = 0;
        $total = 0;
        $subTotal = $entity->getSubTotal();
        $discountMode = $entity->getDiscountMode();
        if($discountMode and $discountMode == 'flat' and $entity->getDiscountCalculation() > 0){
            $total = ($subTotal  - $entity->getDiscountCalculation());
            $discount = $entity->getDiscountCalculation();
        }elseif($discountMode and $entity->getDiscountCalculation() > 0  ){
            $discount = ($subTotal * $entity->getDiscountCalculation())/100;
            $total = ($subTotal  - $discount);
        }
        if($total > $discount ){
            $entity->setDiscount($confManager->numberFormat($discount));
            $entity->setTotal($confManager->numberFormat($subTotal + $entity->getTotalTaxIncidence()));
            $entity->setNetTotal($confManager->numberFormat($entity->getTotal() - $entity->getDiscount() ));
            $entity->setDue($confManager->numberFormat($entity->getNetTotal() - $entity->getAmount()));
        }else{

            $entity->setTotal($confManager->numberFormat($subTotal + $entity->getTotalTaxIncidence()));
            $entity->setNetTotal($confManager->numberFormat($entity->getTotal()));
            $entity->setDue($confManager->numberFormat($entity->getNetTotal() - $entity->getAmount()));
            $entity->setDiscountCalculation(0);
            $entity->setDiscount(0);

        }
        $em->persist($entity);
        $em->flush();
        return $entity;
    }

    public function getSalesReturnInsertQnt(SalesReturn $entity){

        $em = $this->_em;

        /** @var $item SalesReturnItem  */

        if($entity->getSalesReturnItems()){
            foreach($entity->getSalesReturnItems() as $item ){
                $this->processStockQuantity($item,'sales-return');
            }
        }
    }

    public function getUpdateRebate(Sales $sales)
    {

        $em = $this->_em;

        /* @var $item SalesItem */

        foreach ($sales->getsalesItems() as $item ){

            if($item->getProductionBatchItem()){

                $salesItemId = $item->getId();
                $em->createQuery("DELETE TerminalbdInventoryBundle:SalesItemTax e WHERE e.salesItem = '{$salesItemId}'")->execute();

                $productionItem = $item->getProductionBatchItem()->getProductionItem();

                /* @var ProductionElement $element */

                foreach ($productionItem->getElements() as $element ):

                    $entity = new SalesItemTax();
                    $entity->setSalesItem($item);
                    $entity->setItem($element->getMaterial());
                    $entity->setProductionItem($productionItem);
                    $entity->setProductionElement($element);
                    $entity->setPrice($element->getMaterial()->getPurchasePrice());
                    $qnt = ($element->getQuantity() * $item->getTotalQuantity());
                    $entity->setQuantity($qnt);

                    if($element->getMaterial()->getSdPercent()){
                        $entity->setSupplementaryDutyPercent($element->getMaterial()->getSdPercent());
                        $sd = $this->stockPurchaseItemPrice($qnt,$element->getMaterial()->getPurchasePrice(),$element->getMaterial()->getSdPercent());
                        $entity->setSupplementaryDuty($sd);
                    }

                    if($element->getMaterial()->getVatPercent()){
                        $entity->setValueAddedTaxPercent($element->getMaterial()->getVatPercent());
                        $vat = $this->stockPurchaseItemPrice($qnt,$element->getMaterial()->getPurchasePrice(),$element->getMaterial()->getVatPercent());
                        $entity->setValueAddedTax($vat);
                    }

                    if($element->getMaterial()->getMasterItem()->getAdvanceTax()){
                        $entity->setAdvanceTaxPercent($element->getMaterial()->getMasterItem()->getAdvanceTax());
                        $at = $this->stockPurchaseItemPrice($qnt,$element->getMaterial()->getPurchasePrice(),$element->getMaterial()->getMasterItem()->getAdvanceTax());
                        $entity->setAdvanceTax($at);
                    }
                    $em->persist($entity);
                    $em->flush();

                endforeach;
            }
            $itemTax = $em->getRepository('TerminalbdInventoryBundle:SalesItemTax')->getExportSalesItemTax($item);
            $item->setRebateSd($itemTax['supplementaryDuty']);
            $item->setRebateAt($itemTax['advanceTax']);
            $item->setRebateVat($itemTax['valueAddedTax']);
            $em->persist($item);
            $em->flush();

        }
    }

    public function getRefundIncreasingAdjustment(Sales $sales)
    {

        $em = $this->_em;

        /* @var $item SalesItem */

        foreach ($sales->getsalesItems() as $item ){

            if($item->getNbrSupplyOutputTax()->getSlug() != "part-3-standard-rated-goods-service"){
                $em->createQuery("DELETE TerminalbdInventoryBundle:SalesItemTax e WHERE e.salesItem = '{$item->getId()}'")->execute();
                if($item->getProductionBatchItem()){

                    $productionItem = $item->getProductionBatchItem()->getProductionItem();

                    /* @var ProductionElement $element */

                    foreach ($productionItem->getElements() as $element ):

                        $entity = new SalesItemTax();
                        $entity->setSalesItem($item);
                        $entity->setItem($element->getMaterial());
                        $entity->setProductionItem($productionItem);
                        $entity->setProductionElement($element);
                       // $entity->setPrice($element->getMaterial()->getProductionPrice());
                        $entity->setPrice($entity->getItem()->getProductionPrice());
                        $qnt = ($element->getQuantity() * $item->getTotalQuantity());
                        $entity->setQuantity($qnt);
                        if($element->getMaterial()->getVatPercent()){
                            $entity->setValueAddedTaxPercent($element->getMaterial()->getVatPercent());
                            $vat = $this->stockPurchaseItemPrice($qnt,$entity->getPrice(),$entity->getValueAddedTaxPercent());
                            $entity->setVatRefundForSales($vat);
                        }
                        $em->persist($entity);
                        $em->flush();

                    endforeach;

                }elseif($item->getItem()->getMasterItem()->getProductGroup()->getSlug() == "finish-goods" and !empty($item->getItem()->getProductionItem())){

                    $productionItem = $item->getItem()->getProductionItem();
                    $entity = new SalesItemTax();
                    $entity->setSalesItem($item);
                    $entity->setItem($item->getItem());
                    $entity->setProductionItem($productionItem);
                   // $entity->setPrice($item->getPurchasePrice());
                    $entity->setPrice($entity->getItem()->getProductionPrice());
                    $entity->setQuantity($item->getTotalQuantity());
                    if($item->getPurchaseItem()->getValueAddedTaxPercent()){
                        $entity->setValueAddedTaxPercent($item->getPurchaseItem()->getValueAddedTaxPercent());
                        $vat = $this->stockPurchaseItemPrice($entity->getQuantity(),$entity->getPrice(),$entity->getValueAddedTaxPercent());
                        $entity->setVatRefundForSales($vat);
                    }
                    $em->persist($entity);
                    $em->flush();
                }
                $itemTax = $em->getRepository('TerminalbdInventoryBundle:SalesItemTax')->getExportSalesItemTax($item);
                if($itemTax['vatRefundForSales']){
                    $item->setVatRefundForSales($itemTax['vatRefundForSales']);
                }
                $em->persist($item);
                $em->flush();
            }
        }
    }

    public function localNbrSupplyOutputTax(Sales $sales)
    {
        $em = $this->_em;
        /* @var $item SalesItem */
        foreach ($sales->getsalesItems() as $item ) {
            $itemNote = $item->getItem()->getMasterItem()->getSupplyOutputTax();
            $item->setNbrSupplyOutputTax($itemNote);
            $em->persist($item);
            $em->flush();
        }
    }

    public function stockPurchaseItemPrice($qnt,$price,$percentage)
    {
        $amount = ((($qnt * $price) * $percentage )/100);
        return $amount;

    }

    public function updateVAT($user , Sales $entity)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $qb->from('TerminalbdInventoryBundle:SalesItem','si');
        $qb->select('sum(si.valueAddedTax) as valueAddedTax');
        $qb->where('si.sales = :entity')->setParameter('entity', $entity ->getId());
        $qb->andWhere('si.vdsApplicable = :vds')->setParameter('vds',1);
        $vds = $qb->getQuery()->getOneOrNullResult();
        $vdsvat = $vds['valueAddedTax'];
        $entity->setTaxableVat($vdsvat);
        $em->persist($entity);
        $em->flush();
        if($vdsvat > 0){
            $em->getRepository('TerminalbdNbrvatBundle:VdsCertificate')->insertSalesVds($user,$entity);
        }
    }

}
