<?php

namespace Terminalbd\InventoryBundle\Repository;

use App\Entity\Application\Inventory;
use App\Service\ConfigureManager;
use Core\UserBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Terminalbd\InventoryBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\SalesReturn;
use Terminalbd\InventoryBundle\Entity\SalesReturnItem;


/**
 * ExpenditureItemRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SalesReturnItemRepository extends EntityRepository
{

    public function getSalesReturnList(SalesReturn $return)
    {
        $items = array();
        foreach ($return->getSalesReturnItems() as $item){
            $items[$item->getSalesItem()->getId()] = $item;
        }
        return $items;
    }

    public function insertSalesKeyItem(SalesReturn $reEntity,$data)
    {

        $em = $this->_em;
        $i = 0;

        if(isset($data['quantity']) OR isset($data['returnItemId']) ){
            foreach ($data['salesItemId'] as $value) {
                $salesItemId = isset($data['salesItemId'][$i]) ? $data['salesItemId'][$i] : 0 ;
                $metaId = isset($data['returnItemId'][$i]) ? $data['returnItemId'][$i] : 0 ;
                $itemKeyValue = $em->getRepository('TerminalbdInventoryBundle:SalesReturnItem')->findOneBy(array('salesReturn' => $reEntity,'salesItem' => $salesItemId));
                if(!empty($itemKeyValue)){
                    $this->updateMetaAttribute($itemKeyValue,$data['quantity'][$i]);
                }else{
                    if(!empty($data['quantity'][$i]))
                    {
                        $salesItemId = $em->getRepository('TerminalbdInventoryBundle:SalesItem')->find($data['salesItemId'][$i]);
                        $entity = new SalesReturnItem();
                        $entity->setSalesReturn($reEntity);
                        $entity->setQuantity($data['quantity'][$i]);
                        $entity->setSalesItem($salesItemId);
                        $entity->setProductionBatchItem($salesItemId->getProductionBatchItem());
                        $entity->setItem($salesItemId->getItem());
                        $entity->setPrice($salesItemId->getSalesPrice());
                        $entity->setSubTotal($entity->getQuantity() * $salesItemId->getSalesPrice());
                        $entity->setTotal($entity->getSubTotal());
                        $em->persist($entity);
                        $em->flush($entity);
                        $this->updateItemTax($entity);
                    }

                }
                $i++;
            }

        }
    }

    public function updateMetaAttribute(SalesReturnItem $item,$key)
    {
        $em = $this->_em;
        $item->setQuantity(floatval($key));
        $item->setSubTotal($item->getQuantity() * $item->getPrice());
        $em->flush();
        $this->updateItemTax($item);
    }

    public function updateItemTax(SalesReturnItem $entity)
    {
        $em = $this->_em;
        $subTotal = $entity->getSubTotal();
        $vat = $entity->getSalesItem();
        if($vat){

            /* @var $vat salesItemId */


            $sdModes = array("local");
            if(in_array($entity->getSalesReturn()->getMode(),$sdModes) and $vat->getSupplementaryDuty() > 0){
                $entity->setSupplementaryDutyPercent($vat->getSupplementaryDutyPercent());
                $amount = $this->getTaxTariffCalculation($subTotal,$vat->getSupplementaryDutyPercent());
                $entity->setSupplementaryDuty($amount);
            }

            $vatModes = array("registred","local");
            if(in_array($entity->getSalesReturn()->getMode(),$vatModes) and $vat->getValueAddedTax() > 0){
                $total = $subTotal +  $entity->getSupplementaryDuty();
                $entity->setValueAddedTaxPercent($vat->getValueAddedTaxPercent());
                $amount = $this->getTaxTariffCalculation($total,$vat->getValueAddedTaxPercent());
                $entity->setValueAddedTax($amount);
            }

            $TTI = ($entity->getSupplementaryDuty() + $entity->getValueAddedTax());
            $reateModes = array("registred");
            if(in_array($entity->getSalesReturn()->getMode(),$reateModes) and $vat->getRebate() > 0){
                $entity->setRebatePercent($vat->getRebate());
                $amount = $this->getTaxTariffCalculation($TTI,$vat->getRebate());
                $entity->setRebate($amount);
            }
            $entity->setTotalTaxIncidence($TTI  - $entity->getRebate());
        }
        $entity->getSubTotal() + $entity->getTotalTaxIncidence();
        $entity->setTotal($entity->getSubTotal() + $entity->getTotalTaxIncidence());
        $em->persist($entity);
        $em->flush($entity);

    }

    private function getTaxTariffCalculation($subTotal,$tariff)
    {
        $value = 0;
        $value = (($subTotal * $tariff)/100);
        return $value;
    }

    public function getSalesReturnInsertQnt(SalesReturn $entity){

        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');

        /* @var $salesReturnItem SalesReturnItem */

        foreach ($entity->getSalesReturnItems() as $salesReturnItem ){

            /* @var $stockItem StockItem */

            $em->getRepository('TerminalbdInventoryBundle:StockItem')->getSalesReturnInsertQnt($salesReturnItem, $salesReturnItem->getQuantity());

        }

    }


}
